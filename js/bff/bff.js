/**
 * Bff.Utilites.js
 * @author Tamaranga | tamaranga.com
 * @version 0.52
 * @owr dab8fceb3f2dc9809a0c070199ff2721
 * @modified 18.jul.2015
 */

$.fn.btnBootstrap = $.fn.button.noConflict();
var bff = {
    h: !!(window.history && history.pushState),

    extend: function (destination, source) {
        if (destination.prototype) {
            for (var property in source)
                destination.prototype[property] = source[property];
        } else {
            for (var property in source)
                destination[property] = source[property];
        }
        return destination;
    },

    redirect: function (url, ask, timeout) {
        if (!ask || (ask && confirm(ask))) {
            window.setTimeout(function () {
                window.location.href = url;
            }, (timeout || 0) * 1000);
        }
        return false;
    },

    isEmail: function (str) {
        if (str.length <= 6) return false;
        var re = /^\s*[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\s*$/i;
        return re.test(str);
    },

    ajax_utils: (function () {
        return {
            errors: function (errors, system) {
                if (!app.hasOwnProperty('adm') || !app.adm) return;
                if (system) {
                    bff.error('Ошибка передачи данных' + (errors ? '(' + errors + ')' : ''));
                } else {
                    bff.errors.show(errors);
                }
            },
            progress: function (progressSelector) {
                var timer_reached = false, response_received = false, progress = null,
                    progressIsFunc = false, progressIsButton = false;
                if (progressSelector !== undefined && progressSelector != false) {
                    if ((progressIsFunc = (Object.prototype.toString.call(progressSelector) === "[object Function]"))) {
                        progress = progressSelector;
                    } else {
                        progress = $(progressSelector);
                        if (!progress.length) progress = false;
                        else if (progress.is('input[type="button"]') || progress.is('button')) {
                            progressIsButton = true;
                        }
                    }
                }
                if (progress) {
                    if (progressIsFunc) {
                        progress.call(this, true);
                    }
                    else if (progressIsButton) {
                        progress.prop('disabled', true);
                    }
                    else {
                        progress.show();
                    }
                    setTimeout(function () {
                        timer_reached = true;
                        that.finish();
                    }, 400);
                }
                var that = {
                    finish: function (response_received_set) {
                        if (response_received_set) {
                            response_received = true;
                        }
                        if (timer_reached && response_received && progress) {
                            if (progressIsFunc) {
                                progress.call(this, false);
                            }
                            else if (progressIsButton) {
                                progress.removeProp('disabled');
                            }
                            else {
                                progress.hide();
                            }
                        }
                    }
                };
                return that;
            }
        }
    }()),

    ajax: function (url, params, callback, progressSelector, o) {
        o = o || {async: true, crossDomain: false, isPost: true};
        var progress = bff.ajax_utils.progress(progressSelector);
        params.xmlhttprequest = true;
        return $.ajax({
            url: url,
            data: params,
            dataType: 'json',
            type: (o.isPost ? 'POST' : 'GET'),
            crossDomain: (o.crossDomain || false),
            async: (o.async && true),
            success: function (resp, status, xhr) {
                console.log('ajax' , resp);
                progress.finish(true);
                if (resp === undefined) {
                    if (status !== 'success')
                        bff.ajax_utils.errors(0, true);
                    if (callback) callback(false);
                    return;
                }

                if (resp['data'] && resp.data['DEBUG']) {
                    $('.logger').append('<hr>' + url + '<br>' + resp.data.DEBUG);
                }

                if (resp.errors && (($.isArray(resp.errors) && resp.errors.length) || $.isPlainObject(resp.errors)))
                    bff.ajax_utils.errors(resp.errors, false);



                if (callback) {
                    if (!resp.data || !resp.data.script) {
                        callback(resp.data, resp.errors);
                    } else {
                        if (!resp.data.script[2]) {
                            resp.data.script[2] = [];
                        }
                        resp.data.script[2].unshift(function () {
                            callback(resp.data, resp.errors);
                        });
                    }
                }

                bff.includeSrc(resp.data);
            },
            error: function (xhr, status, e) {
                progress.finish(true);
                bff.ajax_utils.errors(xhr.status, true);
                if (o.onError) o.onError(xhr, status, e);
                if (callback) callback(false);
            }
        })  ;
    },

    iframeSubmit: function (form, callback, o) {
        var $form = $(form);
        if (!$form.length) {
            console.info('Form not found for iframeSubmit', form);
            return;
        }
        o = $.extend({
            json: true,
            button: '',
            prefix: 'bff_ajax_iframe',
            url: $form.attr('action'),
            progress: false,
            beforeSubmit: $.noop
        }, o || {});
        //if (!o.url) o.url = location.href;

        if ($form.hasClass(o.prefix + '_inited')) return;
        var iframeID = parseInt((Math.random() * (999 - 1) + 1));
        $form.before('<iframe name="' + o.prefix + iframeID + '" class="' + o.prefix + '" id="' + o.prefix + iframeID + '" style="display:none;"></iframe>');
        var $iframe = $('#' + o.prefix + iframeID);
        if (!$iframe.length) return;

        $form.attr({target: $iframe.attr('name'), action: o.url, method: 'POST', enctype: 'multipart/form-data'});
        $form.addClass(o.prefix + '_inited');
        $form.append('<input type="hidden" name="xmlhttprequest" value="true" />');

        var _process = false, $button = $(o.button);
        if (!$button.length) $button = false;

        //console.log('iframeSubmit: Init load', $form);
        $iframe.on('load', function () {
            if (o.progress) o.progress.finish(true);
            _process = false;
            var responseTxt = $iframe.contents().text();
            if (!responseTxt) return;
            if (o.json) {
                var data = {};
                try{
                    data = $.extend({data: '', errors: []}, $.parseJSON(responseTxt) || {});
                    if (data.errors.length) {
                        bff.ajax_utils.errors(data.errors, false);
                    }
                } catch(e) {
                    data = {data: '', errors:[]};
                    bff.ajax_utils.errors('Ошибка при выполнении запроса.', true);
                }
                callback.apply(this, [data.data, data.errors]);
            } else {
                callback.apply(this, [responseTxt]);
            }
            if ($button) $button.btnBootstrap('reset');
            // setTimeout(function () {
            //     response.html('');
            // }, 1);
        });

        //console.log('iframeSubmit: Init submit', $form);
        $form.submit(function () {
            //console.log('iframeSubmit: Is Submit', $form);
            if (_process) {
                console.error('Form not yet complete response');
                return false;
            }
            if ($.isFunction(o.beforeSubmit)) {
                if (o.beforeSubmit.apply(this, [$form]) === false) {
                    return false;
                }
            }
            _process = true;
            if ($button) {
                $button.btnBootstrap('loading');
            }
            bff.ajax_utils.progress(o.progress);
            return true;
        });
    },

    ajaxURL: function (sModule, sActionQuery) {
        return '/index.php?bff=ajax&s=' + sModule + '&lng=' + app.lng + '&act=' + sActionQuery;
    },

    ymaps: function (container, center, centerZoom, callback, o) {
        YMaps.load(function () {
            var map = new YMaps.Map($(container).get(0));
            if ($.type(center) === 'string') {
                center = YMaps.GeoPoint.fromString(center);
            }
            map.setCenter(center, centerZoom || 12);
            callback = callback || $.noop;
            callback(map);
        });
    },

    map: (function () {
        var YANDEX = 'yandex', GOOGLE = 'google';
        var currentType = YANDEX;

        function isYandex() {
            return (currentType === YANDEX);
        }

        function isGoogle() {
            return (currentType === GOOGLE);
        }

        return {
            setType: function (type) {
                currentType = type;
            },
            isYandex: isYandex,
            isGoogle: isGoogle,
            init: function (container, center, callback, o) {
                o = $.extend({controls: 'view', zoom: 16, marker: false}, o || {});
                if (!$.isArray(center)) {
                    center = center.split(',', 2);
                    center[0] = parseFloat(center[0]);
                    center[1] = parseFloat(center[1]);
                }
                callback = callback || $.noop;

                var map, type = currentType;
                var self = {
                    getMap: function () {
                        return map;
                    },
                    panTo: function (point, opts) {
                        if (this.isYandex()) {
                            map.panTo(point, opts);
                        } else if (this.isGoogle()) {
                            map.panTo(($.isArray(point) ? new google.maps.LatLng(point[0], point[1]) : point));
                            if (opts.hasOwnProperty('callback') && $.isFunction(opts['callback'])) {
                                opts.callback.call(self);
                            }
                        }
                    },
                    refresh: function () {
                        if (this.isYandex()) {
                            map.container.fitToViewport();
                        } else if (this.isGoogle()) {
                            var lastCenter = map.getCenter();
                            google.maps.event.trigger(map, 'resize');
                            map.setCenter(lastCenter);
                        }
                    },
                    isYandex: function () {
                        return (type === YANDEX);
                    },
                    isGoogle: function () {
                        return (type === GOOGLE);
                    }
                };

                switch (type) {
                    case YANDEX:
                        ymaps.ready(function () {
                            var controls = {view: 'smallMapDefaultSet', search: 'mediumMapDefaultSet'};
                            map = new ymaps.Map(container, {
                                zoom: o.zoom,
                                center: center,
                                controls: ( controls.hasOwnProperty(o.controls) ? [controls[o.controls]] : o.controls )
                            }, {autoFitToViewport: 'always'});
                            map.behaviors.disable('scrollZoom');
                            map.container.fitToViewport();

                            callback.call(self, map, type);
                            if (o.marker) {
                                map.geoObjects.add(new ymaps.Placemark(map.getCenter(), {draggable: false}));
                            }
                        });
                        break;
                    case GOOGLE:
                        $(function () {
                            if (typeof container === 'string' || container instanceof String) {
                                container = document.getElementById(container);
                            }
                            map = new google.maps.Map(container, {
                                zoom: o.zoom,
                                center: new google.maps.LatLng(center[0], center[1]),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                scrollwheel: false
                            });

                            callback.call(self, map, type);
                            if (o.marker) {
                                new google.maps.Marker({position: map.getCenter(), map: map, draggable: false});
                            }
                        });
                        break;
                }

                return self;
            },
            editor: function () {
                if (isGoogle()) {
                    return bffmapEditorGoogle();
                }
                return bffmapEditorYandex();
            },
            coordsFromString: function (v) {
                if (!$.isArray(v)) {
                    v = v.split(',', 2);
                }
                v = [parseFloat(v[0]), parseFloat(v[1])];
                if (isGoogle()) {
                    return new google.maps.LatLng(v[0], v[1]);
                }
                return v;
            }
        };
    }()),

    maxlength: function (element, options) {
        var o = {}, $e, $m, maxLength = 0, lastLength;

        $(function () {
            o = $.extend(true, {
                limit: 0, // max. text length
                message: false, // message block
                cut: true, // cut
                escape: false, //escape chars
                lang: {
                    left: '[symbols] осталось',
                    symbols: ['знак', 'знака', 'знаков']
                },
                nobr: false, // true: convert \n => <br />, false \n => \t
                onError: $.noop, // error callback
                onChange: $.noop // change callback
            }, options);

            maxLength = intval(o.limit);
            $e = $(element);
            if (!$e.length || o.limit <= 0) return;
            $e.on('keyup input paste', function () {
                _check($e.val());
                o.onChange($e);
            });
            $m = $(o.message);
            o.message = ( $m.length > 0 );

            _check($e.val());
            if (!o.cut) $e.attr({maxlength: maxLength});
        });

        function _check(value) {
            if (lastLength === value.length) return;
            lastLength = value.length;
            var currentLength = ( o.escape ? _escape(value).length : value.length );
            if (!o.message) return;
            if (currentLength > maxLength) {
                if (o.cut) {
                    $e.val(value.substr(0, maxLength));
                } else {
                    if (o.onError && $.isFunction(o.onError)) {
                        o.onError();
                    }
                }
            } else {
                $m.text(o.lang.left.toString().replace(new RegExp('\\[symbols\\]'), bff.declension(maxLength - currentLength, o.lang.symbols)));
            }
        }

        function _escape(text) {
            var res = '';
            for (var i = 0; i < text.length; i++) {
                var c = text.charCodeAt(i);
                switch (c) {
                    case 0x26:
                        res += '&amp;';
                        break;
                    case 0x3C:
                        res += '&lt;';
                        break;
                    case 0x3E:
                        res += '&gt;';
                        break;
                    case 0x22:
                        res += '&quot;';
                        break;
                    case 0x0D:
                        res += '';
                        break;
                    case 0x0A:
                        res += ( o.nobr ? "\t" : '<br />' );
                        break;
                    case 0x21:
                        res += '&#33;';
                        break;
                    case 0x27:
                        res += '&#39;';
                        break;
                    default:
                        res += ((c > 0x80 && c < 0xC0) || c > 0x500) ? '&#' + c + ';' : text.charAt(i);
                        break;
                }
            }
            return res;
        }
    },

// author: Sergey Chikuyonok
    placeholder: (function () {
        var data_key = 'plc-label',
            fields_key = 'bindedFiles';
        // is placeholder supported by browser (Webkit, Firefox 3.7)
        var nativePlaceholder = ('placeholder' in document.createElement('input'));

        /**
         * Функция, отвечающая за переключение отображения заполнителя.
         * Срабатывает автоматически при фокусе/блюре с элемента ввода.
         * @param {Event} evt
         */
        function placeholderSwitcher(evt) {
            var input = $(this),
                label = input.data(data_key);

            if (!$.trim(input.val()) && evt.type == 'blur')
                label.show();
            else
                label.hide();
        }

        function focusOnField(evt) {
            var binded_fields = $(this).data(fields_key);

            if (binded_fields) {
                $(binded_fields).filter(':visible:first').focus();
                evt.preventDefault();
            }
        }

        function linkPlaceholderWithField(label, field) {
            label = $(label);
            field = $(field);

            if (!label.length || !field.length)
                return;

            /** @type Array */
            var binded_fields = label.data(fields_key);

            if (!binded_fields) {
                binded_fields = [];
                label
                    .data(fields_key, binded_fields)
                    .click(focusOnField);
            }

            binded_fields.push(field[0]);
            field.data(data_key, label)
                .bind('focus blur', placeholderSwitcher)
                .blur();
        }

        return {
            init: function (context) {
                $(context || document).find('label.placeholder').each(function () {
                    if (nativePlaceholder) return;
                    linkPlaceholderWithField(this, '#' + $(this).attr('for'));
                    $($(this).data(fields_key)).blur();
                });
            },

            linkWithField: linkPlaceholderWithField
        };
    })(),
    st: {
        versioninig: false, _in: {},

        _url: function (url) {
            if (url.indexOf('http') == 0) return url;
            return ( url.indexOf('core') == 0 ? '/js/bff' + url.substr(4) : '/js/' + url );
        },
        includeJS: function (url, callback, ver) {
            ver = ver || 1.0;
            if (this.isDone(url)) {
                if (callback) callback();
                return false;
            }
            this.done(url, ver);
            $.getScript((this._url(url) + (this.versioninig ? '?v=' + ver : '')), function () {
                if (callback) callback();
            });
        },
        includeCSS: function (url, ver) {
            ver = ver || 1.0;
            if (this.isDone(url)) {
                if (callback) callback();
                return false;
            }
            this.done(url, ver);
            var el = document.createElement('link');
            el.href = (this._url(url) + (this.versioninig ? '?v=' + ver : ''));
            el.rel = 'stylesheet';
            el.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(el);
        },
        isDone: function (url) {
            return this._in.hasOwnProperty(url);
        },
        done: function (url, ver) {
            ver = ver || 1.0;
            this._in[url] = ver;
        }
    },

    includeSrc: function (resp) {
        document.body.style.cursor='wait';
        if (typeof resp.css !== "undefined") {
            bff.includer.cssLoad(resp.css);
        }
        if (typeof resp.js !== "undefined") {
            bff.includer.scriptLoad(resp.js);

        }
        if (typeof resp.script !== "undefined") {
            bff.includer.onLoad(resp.script);
        }
    },
    includer: {
            onLoad: function (src) {
            if (bff.includer._loadCount !== 0) {
                console.log('--in process--' + bff.includer._loadCount);
                setTimeout(function () {
                    bff.includer.onLoad(src);
                }, 500);
            }
            else {
                console.log('--READY--' + bff.includer._loadCount);
                bff.includer.scriptLoad(src);
            }
        },

        cssLoad: function (css) {
            for (var i in css) {
                if (typeof i === 'string' && i.indexOf('.css')>0 && bff.includer.isUrl(i)) {
                    bff.includer.includeCSS(i);
                }
                else if(typeof css[i] === 'string' && css[i]) {
                    if (css[i].indexOf('.css')>0 && bff.includer.isUrl(css[i])) {
                        bff.includer.includeCSS(css[i]);
                    }
                    else {
                        console.warn('CSS - ', css[i]);
                        var style = document.createElement('style');
                        style.innerHTML = css[i];
                        document.getElementsByTagName('head')[0].appendChild(style);
                    }
                }
                else if (typeof css[i] === 'object') {
                    bff.includer.cssLoad(css[i]);
                }

            }
        },
        includeCSSlist: {},
        includeCSS: function (css, onComplete) {
            if (typeof(css) === 'string')
                css = [css];
            var i = 1;
            var ii = css.length;
            var onCssLoaded = function () {
                if (i++ === ii && onComplete)
                    onComplete.call();
            };
            for (var s in css) {
                var validSrc = bff.includer.checkCSSInclude(css[s]);

                if (validSrc) {
                    var styleCss = document.createElement('link');
                    //styleCss.type = 'text/css';
                    styleCss.rel = 'stylesheet';
                    styleCss.href = css[s];
                    $(styleCss).ready(onCssLoaded);
                    document.getElementsByTagName('head')[0].appendChild(styleCss);
                    console.log('Create stylesheet el : ' + css[s]);
                }
                else if (onComplete)
                    onComplete.call(this);
            }
            ;
            return true;
        },
        checkCSSInclude: function (url) {
            url = bff.includer.absPath(url);
            if (jQuery.isEmptyObject(bff.includer.includeCSSlist)) {// проверка на уникальность подключаемого стиля
                bff.includer.includeCSSlist[url] = 1;
                var flag = 0;
                jQuery('link[href!=""]').each(function () {
                    var href = bff.includer.absPath(this.href);
                    bff.includer.includeCSSlist[href] = 1;
                    if (href == url) flag = 1;
                });
                if (flag == 1) {
                    bff.includer.includeCSSlist[url] = 2;
                    return false;
                }
            }
            else {
                if (bff.includer.includeCSSlist[url]) {
                    bff.includer.includeCSSlist[url]++;
                    return false;
                }
                else bff.includer.includeCSSlist[url] = 1;
            }
            return url;
        },

        _onLoad: false,
        _loadCount: 0,
        scriptLoad: function (script) {
            var thisObj = this;
            --bff.includer._loadCount;

            if (typeof script === 'object') {
                for (var i in script) {
                    if (typeof script[i] === 'function') {
                        globalEval(script[i]);
                    }
                    else if (typeof script[i] === 'string') {
                        if (bff.includer.isUrl(script[i]) && script[i].indexOf('.js')>0) {
                            bff.includer.includeHelper(script[i]);
                        }
                        else {
                            globalEval(script[i]);
                        }
                    }
                    else if (typeof script[i] === 'object') {
                        bff.includer.scriptLoad(script[i]);
                    }
                }
            }
            else {
                globalEval(script);
            }
            document.body.style.cursor='default';
            ++bff.includer._loadCount;
        },

        // Обертка для запуска функции include
        includeHelper: function (src, subsrc) {
            var thisObj = this;
            --bff.includer._loadCount;
            bff.includer.include(src, function () {
                if (subsrc)
                    thisObj.scriptLoad(subsrc);
                ++thisObj._loadCount;
            });
        },

        isUrl: function (str) {
            return (
                strpos(str, '.') === 0 ||
                strpos(str, '/') === 0 ||
                strpos(str, 'https://') === 0 ||
                strpos(str, 'http://') === 0
            );
        },

        includejslist: null,

        // загружаем скрипт
        include: function (scripts, onComplete) {
            if (typeof(scripts) == 'string')
                scripts = [scripts];
            var i = 1;
            var ii = scripts.length;
            var onScriptLoaded = function () {
                if (i++ == ii && onComplete) {
                    onComplete.call();
                    i++;
                }
            };
            for (var s in scripts) {
                var validSrc = bff.includer.checkJsInclude(scripts[s]);
                if (validSrc) {
                    var scriptElement = document.createElement('script');
                    //styleCss.type = 'text/javascript';
                    scriptElement.src = scripts[s];
                    // SET READY
                    scriptElement.onload = function () {
                        onScriptLoaded.call();
                    };
                    scriptElement.onreadystatechange = function () {
                        if (this.readyState != "complete" && this.readyState != "loaded") return;
                        onScriptLoaded.call();
                    };
                    //$(scriptElement).ready(onScriptLoaded);
                    document.getElementsByTagName('head')[0].appendChild(scriptElement);
                    console.log('Create script el : ' + validSrc);
                }
                else {
                    onScriptLoaded.call();
                }
            }
            return;
        },

        checkJsInclude: function (urlDirty) {
            var url = bff.includer.absPath(urlDirty);
            if (!bff.includer.includejslist) {// проверка на уникальность подключаемого
                bff.includer.includejslist = {};
                bff.includer.includejslist[url] = 1;
                var flag = 0;

                jQuery('script[src!=""]').each(function () {
                    var href = bff.includer.absPath(this.src);
                    bff.includer.includejslist[href] = 1;
                    if (href == url) flag = 1;
                });

                if (flag == 1) {
                    bff.includer.includejslist[url] = 2;
                    return false;
                }
            }
            else {
                if (bff.includer.includejslist[url]) {
                    bff.includer.includejslist[url]++;
                    return false;
                }
                else bff.includer.includejslist[url] = 1;
            }
            return url;
        },

        baseHref: '',
        absPath: function (url) {
            var pos = strpos(url, '?v=');
            if (pos) {
                url = url.substr(0, pos);
            }
            if (url.substr(0, 4) != 'http' && url.substr(0, 5) != 'https' && url.substr(0, 1) != '/') {
                if (bff.includer.baseHref == '') {
                    bff.includer.baseHref = jQuery('base').attr('href');
                    if (!bff.includer.baseHref)
                        bff.includer.baseHref = '//' + window.location.host;
                    else {
                        bff.includer.baseHref = $.trim(bff.includer.baseHref, '/');
                    }
                }
                url = bff.includer.baseHref + '/' + $.trim(url, '/');
            } else {
                var i = url.indexOf('../');
                while (i > -1) {
                    url = url.replace(RegExp("[^\/]+\/\.\.\/", "g"), '');
                    i = url.indexOf('../');
                }
            }
            url = url.replace(/\?.+/, '');
            url = url.replace(/^http:/, '');
            url = url.replace(/^https:/, '');
            return url;
        }
    },

    input: {
        file: function (self, id) {
            var file = self.value.split("\\");
            file = file[file.length - 1];
            if (file.length > 30) file = file.substring(0, 30) + '...';
            var html = '<a href="#delete" onclick="bff.input.reset(\'' + self.id + '\'); $(\'#' + id + '\').html(\'\'); $(this).blur(); return false;"></a>' + file;
            $('#' + id).html(html);
        },
        reset: function (id) {
            var o = document.getElementById(id);
            if (o.type == 'file') {
                try {
                    o.parentNode.innerHTML = o.parentNode.innerHTML;
                } catch (e) {
                }
            } else {
                o.value = '';
            }
        }
    },

    declension: function (count, forms, add) {
        var prefix = (add !== false ? count + ' ' : '');
        var n = Math.abs(count) % 100;
        var n1 = n % 10;
        if (n > 10 && n < 20) {
            return prefix + forms[2];
        }
        if (n1 > 1 && n1 < 5) {
            return prefix + forms[1];
        }
        if (n1 == 1) {
            return prefix + forms[0];
        }
        return prefix + forms[2];
    },

    /**
     * Создает куки или возвращает значение.
     * @examples:
     * bff.cookie('the_cookie', 'the_value'); - Задает куки для сессии.
     * bff.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'site.com', secure: true }); Создает куки с опциями.
     * bff.cookie('the_cookie', null); - Удаляет куки.
     * bff.cookie('the_cookie'); - Возвращает значение куки.
     *
     * @param {String} name Имя куки.
     * @param {String} value Значение куки.
     * @param {Object} options Объект опций куки.
     * @option {Number|Date} expires Either an integer specifying the expiration date from now on in days or a Date object.
     *                               If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
     *                               If set to null or omitted, the cookie will be a session cookie and will not be retained
     *                               when the the browser exits.
     * @option {String} path The value of the path atribute of the cookie (default: path of page that created the cookie).
     * @option {String} domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
     * @option {Boolean} secure If true, the secure attribute of the cookie will be set and the cookie transmission will
     *                          require a secure protocol (like HTTPS).
     *
     * @return {mixed} значение куки
     * @author Klaus Hartl (klaus.hartl@stilbuero.de), Vlad Yakovlev (red.scorpix@gmail.com)
     * @version 1.0.1, @date 2009-11-12
     */
    _cookie: function (name, value, options) {
        if ('undefined' != typeof value) {
            options = options || {};
            if (null === value) {
                value = '';
                options.expires = -1;
            }
            // CAUTION: Needed to parenthesize options.path and options.domain in the following expressions,
            // otherwise they evaluate to undefined in the packed version for some reason…
            var path = options.path ? '; path=' + options.path : '',
                domain = options.domain ? '; domain=' + options.domain : '',
                secure = options.secure ? '; secure' : '',
                expires = '';

            if (options.expires && ('number' == typeof options.expires || options.expires.toUTCString)) {
                var date;
                if ('number' == typeof options.expires) {
                    date = new Date();
                    date.setTime(date.getTime() + (options.expires * 86400000/*24 * 60 * 60 * 1000*/));
                } else {
                    date = options.expires;
                }
                expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
            }
            window.document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
            return true;
        }

        var cookieValue = null;
        if (document.cookie && '' != document.cookie) {
            $.each(document.cookie.split(';'), function () {
                var cookie = $.trim(this);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    return false;
                }
            });
        }

        return cookieValue;
    },

    cookie: function (name, value, options) {
        return bff._cookie(name, value, options);
    },
    cookieExp: function (name, value, expires) {
        if (!expires) expires = 365;
        return bff._cookie(name, value, { expires: expires, path: '/', domain:'.'+app.host});
    },

    fadeAjax: function(html) {
        if (!$('#myBackdrop').length) {
            $('<div id="myBackdrop" style="display:none;opacity:0;"></div>'+
                '<div id="myBackdropText" style="display:none;opacity:0;">' +
                '<div class="modal-content">'+
                    '<div class="modal-header" style="display:none;"><button type="button" class="close"><span>×</span></button><h2 class="modal-title"></h2></div>'+
                    '<div class="modal-body"></div>'+
                '</div>'+
            '</div>').appendTo(document.body);
        }
        if ($("#myBackdrop").is(':hidden')) {
            $("#myBackdrop").attr('style', 'opacity: 0;');
            $("#myBackdropText").attr('style', 'opacity: 0;');
            $("#myBackdrop").animate({opacity: '0.5'}, 100, function(){
                $("#myBackdropText").animate({opacity: '1'}, 1000);
            });
        } else {
            // мигаем измененным текстом
            $("#myBackdropText .modal-body").animate({opacity: '0.3'}, 1000, function(){
                $("#myBackdropText .modal-body").animate({opacity: '1'}, 1000);
            });
        }
        $("#myBackdropText .modal-body").html(html);
    },
    fadeAjaxHide: function() {
        if ($("#myBackdrop").is(':visible')) {
            $("#myBackdrop").animate({opacity: '0'}, 100, function () {
                $("#myBackdrop").attr('style', 'display: none; opacity: 0;');
                $("#myBackdropText").attr('style', 'display: none; opacity: 0;');
            });
        }
    },
    fadeAjaxSetActionClose: function(title) {
        if (title)
            $("#myBackdropText .modal-title").text(title);
        $("#myBackdropText .modal-header").show();
        $("#myBackdrop, #myBackdropText").on('click', function(){
            bff.fadeAjaxHide();
            return false;
        });
    },

    /**
     * Получить урл с нужными параметрами
     * param - обект {param1:123, param2: 'abcd'}
     * url - не обязательно, берется текущий адрес
     * replace - true удалит все предыдущие параметры
     */
    getUrlWithNewParam: function (param, url, replace) {
        if (!url)
            url = this.convertAbsolutePathToRelative();
        var parsedUrl = urlDecode(url);
        if (replace)
            parsedUrl[0].args = param;
        else
            $.extend(parsedUrl[0].args, param);

        return urlEncode(parsedUrl);
    },
    /**
     *
     */
    convertAbsolutePathToRelative: function (link) {
        if (!link)
            link = document.location.href;
        link = link.replace(/^http:/, '');
        link = decodeURIComponent(link);
        return link;
    }

};

(function () {
    var ua = navigator.userAgent.toLowerCase(), browser = {};
    var matched = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
        /(webkit)[ \/]([\w.]+)/.exec(ua) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
        [];
    matched = {
        browser: matched[1] || "",
        version: matched[2] || "0"
    };
    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }
    if (browser.chrome) {
        browser.webkit = true;
    }
    else if (browser.webkit) {
        browser.safari = true;
    }
    bff.browser = browser;
}());

// Common shortcut-functions
function nothing(e) {
    if (!e) {
        if (window.event) e = window.event;
        else return false;
    }
    if (e.cancelBubble != null) e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
    if (e.preventDefault) e.preventDefault();
    if (window.event) e.returnValue = false;
    if (e.cancel != null) e.cancel = true;
    return false;
}

(function ($) {

    //------------------------------------------------------------------------------
    jQuery.fn.fadeToggle = function (speed, easing, callback) {
        return this.animate({opacity: "toggle"}, speed, easing, callback)
    };
    jQuery.fn.slideFadeToggle = function (speed, easing, callback) {
        return this.animate({opacity: "toggle", height: "toggle"}, speed, easing, callback)
    };

    // Simple JavaScript Templating, John Resig - http://ejohn.org/blog/javascript-micro-templating/ - MIT Licensed
    var cache = {};
    bff.tmpl = function (str, data) {
        var fn = !/\W/.test(str) ? cache[str] = cache[str] || bff.tmpl(document.getElementById(str).innerHTML) :
            new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};with(obj){p.push('" +
                str.replace(/[\r\t\n]/g, " ").split("<%").join("\t").replace(/((^|%>)[^\t]*)'/g, "$1\r")
                    .replace(/\t=(.*?)%>/g, "',$1,'").split("\t").join("');").split("%>").join("p.push('").split("\r").join("\\'")
                + "');}return p.join('');");
        return data ? fn(data) : fn;
    };

    /* Debounce and throttle function's decorator plugin 1.0.4 Copyright (c) 2009 Filatov Dmitry (alpha@zforms.ru)
     * Dual licensed under the MIT and GPL licenses: http://www.opensource.org/licenses/mit-license.php, http://www.gnu.org/licenses/gpl.html
     */
    $.extend({
        debounce: function (fn, timeout, invokeAsap, context) {
            if (arguments.length == 3 && typeof invokeAsap != 'boolean') {
                context = invokeAsap;
                invokeAsap = false;
            }
            var timer;
            return function () {
                var args = arguments;
                if (invokeAsap && !timer) {
                    fn.apply(context, args);
                }
                clearTimeout(timer);
                timer = setTimeout(function () {
                    if (!invokeAsap) {
                        fn.apply(context, args);
                    }
                    timer = null;
                }, timeout);
            };
        },
        throttle: function (fn, timeout, context) {
            var timer, args;
            return function () {
                args = arguments;
                if (!timer) {
                    (function () {
                        if (args) {
                            fn.apply(context, args);
                            args = null;
                            timer = setTimeout(arguments.callee, timeout);
                        }
                        else {
                            timer = null;
                        }
                    })();
                }
            };
        },
        assert: function (cond, msg, force_report) {
            if (!cond) {
                bff_report_exception('* '.msg, window.location.href, window.location.href);
            }
        }
    });

})(jQuery);

//------------------------------------------------------------------------------
function Flash() {
    this._swf = '';
    this._width = 0;
    this._height = 0;
    this._params = [];
}
Flash.prototype = {
    setSWF: function (_swf, _width, _height) {
        this._swf = _swf;
        this._width = _width;
        this._height = _height;
    },
    setParam: function (paramName, paramValue) {
        this._params[this._params.length] = paramName + '|||' + paramValue;
    },
    display: function () {
        var _txt = '';
        var params_res = '';
        _txt += '<object >\n';
        _txt += '<param width="' + this._width + '" height="' + this._height + '" name="movie" value="' + this._swf + '" />\n'
        _txt += '<param name="quality" value="high" />\n';
        for (i = 0; i < this._params.length; i++) {
            _param = this._params[i].split('|||');
            _txt += '\t<param name="' + _param[0] + '" value="' + _param[1] + '" />\n';
            params_res += _param[0] + '="' + _param[1] + '" ';
        }

        _txt += '<embed width="' + this._width + '" height="' + this._height + '" src="' + this._swf + '" ' + params_res + ' quality="high" type="application/x-shockwave-flash"></embed>';
        _txt += '</object>';
        document.write(_txt);
    }
};

/**
 * Copyright (c) Copyright (c) 2007, Carl S. Yestrau All rights reserved.
 * Code licensed under the BSD License: http://www.featureblend.com/license.txt
 */
var FlashDetect = new function () {
    var self = this;
    self.installed = false;
    (function () {
        if (navigator.plugins && navigator.plugins.length > 0) {
            var type = 'application/x-shockwave-flash', mimeTypes = navigator.mimeTypes;
            if (mimeTypes && mimeTypes[type] && mimeTypes[type].enabledPlugin && mimeTypes[type].enabledPlugin.description) {
                self.installed = true;
            }
        } else if (window.execScript) {
            var found = false, activeXDetectRules = ['ShockwaveFlash.ShockwaveFlash', 'ShockwaveFlash.ShockwaveFlash.7', 'ShockwaveFlash.ShockwaveFlash.6'];
            var getActiveXObject = function (name) {
                var obj = -1;
                try {
                    obj = new ActiveXObject(name);
                } catch (err) {
                    obj = {activeXError: true};
                }
                return obj;
            };
            for (var i = 0; i < activeXDetectRules.length && found === false; i++) {
                var obj = getActiveXObject(activeXDetectRules[i]);
                if (!obj.activeXError) {
                    self.installed = true;
                    found = true;
                }
            }
        }
    }());
};


/**
 * Auto Expanding Text Area (1.2.2) by Chrys Bader (www.chrysbader.com) chrysb@gmail.com
 * Copyright (c) 2008 Chrys Bader (www.chrysbader.com)
 * Dual licensed under the MIT (MIT-LICENSE.txt) and GPL (GPL-LICENSE.txt) licenses.
 */
(function (b) {
    b.fn.autogrow = function (a) {
        return this.each(function () {
            new b.autogrow(this, a)
        })
    };
    b.autogrow = function (a, c) {
        this.options = c || {};
        this.interval = this.dummy = null;
        this.line_height = this.options.lineHeight || parseInt(b(a).css("line-height"));
        this.min_height = this.options.minHeight || parseInt(b(a).css("min-height"));
        this.max_height = this.options.maxHeight || parseInt(b(a).css("max-height"));
        this.textarea = b(a);
        if (this.line_height == NaN)this.line_height = 0;
        if (this.min_height == NaN || this.min_height == 0)this.min_height = this.textarea.height();
        this.init()
    };
    b.autogrow.fn = b.autogrow.prototype = {autogrow: "1.2.2"};
    b.autogrow.fn.extend = b.autogrow.extend = b.extend;
    b.autogrow.fn.extend({
        init: function () {
            var a = this;
            this.textarea.css({overflow: "hidden", display: "block"});
            this.textarea.bind("focus", function () {
                a.startExpand()
            }).bind("blur", function () {
                a.stopExpand()
            });
            this.checkExpand()
        }, startExpand: function () {
            var a = this;
            this.interval = window.setInterval(function () {
                a.checkExpand()
            }, 400)
        }, stopExpand: function () {
            clearInterval(this.interval)
        }, checkExpand: function () {
            if (this.dummy == null) {
                this.dummy = b("<div></div>");
                this.dummy.css({
                    "font-size": this.textarea.css("font-size"),
                    "font-family": this.textarea.css("font-family"),
                    width: this.textarea.css("width"),
                    padding: this.textarea.css("padding"),
                    "line-height": this.line_height + "px",
                    "overflow-x": "hidden",
                    position: "absolute",
                    top: 0,
                    left: -9999
                }).appendTo("body")
            }
            var a = this.textarea.val().replace(/(<|>)/g, "");
            a = bff.browser.msie ? a.replace(/\n/g, "<BR>new") : a.replace(/\n/g, "<br>new");
            if (this.dummy.html() != a) {
                this.dummy.html(a);
                if (this.max_height > 0 && this.dummy.height() + this.line_height > this.max_height)this.textarea.css("overflow-y", "auto"); else {
                    this.textarea.css("overflow-y", "hidden");
                    if (this.textarea.height() < this.dummy.height() + this.line_height || this.dummy.height() < this.textarea.height())this.textarea.animate({height: this.dummy.height() + this.line_height + "px"}, 100)
                }
            }
        }
    })
})(jQuery);

/**
 * Copyright (c) 2007-2013 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.5
 */
;
(function ($) {
    var h = $.scrollTo = function (a, b, c) {
        $(window).scrollTo(a, b, c)
    };
    h.defaults = {axis: 'xy', duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1, limit: true};
    h.window = function (a) {
        return $(window)._scrollable()
    };
    $.fn._scrollable = function () {
        return this.map(function () {
            var a = this, isWin = !a.nodeName || $.inArray(a.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) != -1;
            if (!isWin)return a;
            var b = (a.contentWindow || a).document || a.ownerDocument || a;
            return /webkit/i.test(navigator.userAgent) || b.compatMode == 'BackCompat' ? b.body : b.documentElement
        })
    };
    $.fn.scrollTo = function (e, f, g) {
        if (typeof f == 'object') {
            g = f;
            f = 0
        }
        if (typeof g == 'function')g = {onAfter: g};
        if (e == 'max')e = 9e9;
        g = $.extend({}, h.defaults, g);
        f = f || g.duration;
        g.queue = g.queue && g.axis.length > 1;
        if (g.queue)f /= 2;
        g.offset = both(g.offset);
        g.over = both(g.over);
        return this._scrollable().each(function () {
            if (e == null)return;
            var d = this, $elem = $(d), targ = e, toff, attr = {}, win = $elem.is('html,body');
            switch (typeof targ) {
                case'number':
                case'string':
                    if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
                        targ = both(targ);
                        break
                    }
                    targ = $(targ, this);
                    if (!targ.length)return;
                case'object':
                    if (targ.is || targ.style)toff = (targ = $(targ)).offset()
            }
            $.each(g.axis.split(''), function (i, a) {
                var b = a == 'x' ? 'Left' : 'Top', pos = b.toLowerCase(), key = 'scroll' + b, old = d[key], max = h.max(d, a);
                if (toff) {
                    attr[key] = toff[pos] + (win ? 0 : old - $elem.offset()[pos]);
                    if (g.margin) {
                        attr[key] -= parseInt(targ.css('margin' + b)) || 0;
                        attr[key] -= parseInt(targ.css('border' + b + 'Width')) || 0
                    }
                    attr[key] += g.offset[pos] || 0;
                    if (g.over[pos])attr[key] += targ[a == 'x' ? 'width' : 'height']() * g.over[pos]
                } else {
                    var c = targ[pos];
                    attr[key] = (c && c.slice && c.slice(-1) == '%') ? parseFloat(c) / 100 * max : c
                }
                if (g.limit && /^\d+$/.test(attr[key]))attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);
                if (!i && g.queue) {
                    if (old != attr[key])animate(g.onAfterFirst);
                    delete attr[key]
                }
            });
            animate(g.onAfter);
            function animate(a) {
                $elem.animate(attr, f, g.easing, a && function () {
                        a.call(this, e, g)
                    })
            }
        }).end()
    };
    h.max = function (a, b) {
        var c = b == 'x' ? 'Width' : 'Height', scroll = 'scroll' + c;
        if (!$(a).is('html,body'))return a[scroll] - $(a)[c.toLowerCase()]();
        var d = 'client' + c, html = a.ownerDocument.documentElement, body = a.ownerDocument.body;
        return Math.max(html[scroll], body[scroll]) - Math.min(html[d], body[d])
    };
    function both(a) {
        return typeof a == 'object' ? a : {top: a, left: a}
    }
})(jQuery);

// Deserialize
(function ($) {
    $.fn.deserialize = function (data, clearForm, clearFilter) {
        this.each(function () {
            deserialize(this, data, !!clearForm, clearFilter || false);
        });
    };
    function deserialize(element, data, clearForm, clearFilter) {
        var splits = decodeURIComponent(data).split('&'), i = 0, split = null, key = null, value = null, splitParts = null;
        if (clearForm) {
            var clInp = $('input[type="checkbox"],input[type="radio"]', element);
            if (clearFilter) clInp = clInp.filter(clearFilter);
            clInp.prop('checked', false);
            clInp = $('select,input[type="text"],input[type="password"],input[type="hidden"],textarea', element);
            if (clearFilter) clInp = clInp.filter(clearFilter);
            clInp.val('');
        }
        var kv = {};
        while (split = splits[i++]) {
            splitParts = split.split('=');
            key = splitParts[0] || '';
            value = (splitParts[1] || '').replace(/\+/g, ' ');
            if (key != '') {
                if (key in kv) {
                    if ($.type(kv[key]) !== 'array') kv[key] = [kv[key]];
                    kv[key].push(value);
                } else kv[key] = value;
            }
        }

        for (key in kv) {
            value = kv[key];
            $('select[name="' + key + '"],input[type="text"][name="' + key + '"],input[type="password"][name="' + key + '"],input[type="hidden"][name="' + key + '"],textarea[name="' + key + '"]', element).val(value);
            if (!$.isArray(value)) value = [value];
            for (var key2 in value) {
                var value2 = value[key2];
                value2 = value2.replace(/"/g, '\\"');
                $('input[type="checkbox"][name="' + key + '"][value="' + value2 + '"],input[type="radio"][name="' + key + '"][value="' + value2 + '"]', element).prop('checked', true);
            }
        }
    }
})(jQuery);

var globalEval = function globalEval(src) {
    if (src == undefined || src == '') {
        return;
    }

    console.log('globalEval : ', src);

    if (typeof src === 'function') {
        src.call(window);
        return;
    }

    if (window.execScript) {
        window.execScript(src);
        return;
    }
    var fn = function () {
        window.eval.call(window, src);
    };
    fn();
};

function strpos(haystack, needle, offset) {	// Find position of first occurrence of a string
    var i = haystack.indexOf(needle, offset); // returns -1
    return i >= 0 ? i : false;
}

function getObjectLength(obj) {	// Find position of first occurrence of a string
    return $.map(obj, function (n, i) {
        return i;
    }).length;
}

function redirectPost(url, data) {
    var html = '<form action="' +url + '" id="redirectPost2145542" method="post" style="display:none;">';
    for(var i in data) {
        if (typeof data[i] == 'object' || typeof data[i] == 'array') {
            for(var k in data[i]) {
                html += '<input type="hidden" name="' + i + '[]" value="' + data[i][k] + '" />';
            }
        }
        else {
            html += '<input type="hidden" name="' + i + '" value="' + data[i] + '" />';
        }
    }
    html += '</form>';
    $('body').html(html);

    $('#redirectPost2145542').submit();
}

function intval(number) {
    return number && +number | 0 || 0;
}

function floatval(number) {
    return (parseFloat(number) || 0);
}

function copyInp(btn, slc, newTitle) {

    if (navigator.userAgent.match(/ipad|iphone/i)) {
        iosSelect($(slc).get(0))
    } else {
        $(slc).select();
        document.execCommand('copy');
    }
    if (newTitle) btn.innerText = newTitle;
    btn.focus();
}

function iosSelect(el) {
    var oldContentEditable = el.contentEditable,
        oldReadOnly = el.readOnly,
        range = document.createRange();

    el.contentEditable = true;
    el.readOnly = false;
    range.selectNodeContents(el);

    var s = window.getSelection();
    s.removeAllRanges();
    s.addRange(range);

    el.setSelectionRange(0, 999999); // A big number, to cover anything that could be inside the element.
    el.contentEditable = oldContentEditable;
    el.readOnly = oldReadOnly;
    document.execCommand('copy');
}

function copyStr(str) {
    var tmp   = document.createElement('INPUT'), // Создаём новый текстовой input
        focus = document.activeElement; // Получаем ссылку на элемент в фокусе (чтобы не терять фокус)

    tmp.value = str; // Временному input вставляем текст для копирования

    document.body.appendChild(tmp); // Вставляем input в DOM
    tmp.select(); // Выделяем весь текст в input
    document.execCommand('copy'); // Магия! Копирует в буфер выделенный текст (см. команду выше)
    document.body.removeChild(tmp); // Удаляем временный input
    focus.focus(); // Возвращаем фокус туда, где был
}

function urlDecode(str) {
    str = decodeURIComponent(str);
    var arr = str.split('#');

    var result = new Array();
    var ctr = 0;
    for (var part in arr) {
        part = arr[part];
        var qindex = part.indexOf('?');
        result[ctr] = {};
        if (qindex == -1) {
            result[ctr].mid = part;
            result[ctr].args = [];
            ctr++;
            continue;
        }
        result[ctr].mid = part.substring(0, qindex);
        var args = part.substring(qindex + 1);
        args = args.split('&');

        result[ctr].args = {};
        for (var val in args) {
            val = args[val];
            var keyval = val.split('=');
            var localctr = keyval[0];
            var i = localctr.indexOf('[]');
            if (i > 0) {
                localctr = localctr.substr(0, i);
                if (!result[ctr].args[localctr])
                    result[ctr].args[localctr] = [];
                result[ctr].args[localctr].push(keyval[1]);
            }
            else {
                result[ctr].args[localctr] = keyval[1];
            }
        }
        ctr++;
    }
    return result;
}

function urlEncode(objUrl) {
    var result = '';

    for (var i in objUrl) {
        var param = [];
        for (var k in objUrl[i].args) {
            var temp;
            if (typeof(objUrl[i].args[k]) == 'object') {
                for (var l in objUrl[i].args[k]) {
                    temp = k + '[]=' + objUrl[i].args[k][l];
                    param.push(temp);
                }

            }
            else {
                temp = k + '=' + objUrl[i].args[k];
                if (k)
                    param.push(temp);
            }

        }
        param = param.join('&');

        objUrl[i] = objUrl[i].mid;
        if (param)
            objUrl[i] += '?' + param;
    }
    result = objUrl.join('#');
    return result;
}

$(function () {
    // Placeholder
    bff.placeholder.init();

});