var jShopProductForm = (function(){
    var inited = false, o = {lang:{}, itemID:0, imgData:{}};
    var img = {url:'',$block:0,type:{},$togglers:0,uploader:0,active:{}, classes:{progress:'j-progress'}};
    var dp = {$block:0, cache:{}, child:{}, $price:0, price:{}};
    var geo = {$block:0,$regionBlocks:0, country:0,cityAC:0,cityID:0,cityPreSuggest:{}};

    var f, $f;

    function init()
    {
        o.typeID = intval(o.typeID);
        //submit
        $f = $('#j-product-form');
        f = app.form($f, false, {noEnterSubmit: true});
        bff.iframeSubmit($f, function(resp, errors){
            if(resp && resp.success) {
                if(resp.redirect){
                    bff.redirect( $('<div/>').html(resp.redirect).text() );
                }else{
                    f.alertSuccess(o.lang.saved_success);
                }
            } else if(resp['fields']) {
                f.fieldsError(resp.fields, errors);
            }
        },{
            beforeSubmit: function(){
                if( ! f.checkRequired({focus:true}) ) return false;
                var $bl = $f.find('.j-cats:visible');
                if($bl.length){
                    if( ! intval($bl.find('.j-cat-value').val())){
                        app.alert.error(o.lang.cat_wrong);
                        return false;
                    }
                }
                return true;
            },
            button: '.j-submit'
        });

        $f.on('click', '.j-submit', function(){
            $f.submit();
            return false;
        });

        // cancel
        $f.on('click', '.j-cancel', function(){
            history.back();
            return false;
        });

        if(typeof(jCatsSelect) == 'object'){
            jCatsSelect.init({block:$f.find('.j-cat'), cnt:1, limit:1});
            jCatsSelect.onSelect(function(){
                setDp();
            });
        }

        // geo
        geo.fn = (function(){
            geo.$block = $f;
            geo.$regionBlocks = geo.$block.find('.j-region');
            geo.country = o.defCountry;

            var $country = geo.$block.find('#j-product-country');
            if($country.length){
                $country.change(function(){
                    //country
                    geo.country = intval($country.val());
                    //o.reg1_country = geo.country;
                    o.geoCountry = $country.find(':selected').text();
                    if(geo.country){
                        geo.cityAC.setParam('country', geo.country);
                        if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
                            geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
                        }else{
                            bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
                                geo.cityAC.setSuggest[geo.country] = data;
                                geo.cityAC.setSuggest(data, true);
                            });
                        }
                        geo.$regionBlocks.removeClass('displaynone');
                    }else{
                        geo.$regionBlocks.addClass('displaynone');
                    }
                    return true;
                });
                geo.country = intval($country.val());
            }

            //city
            geo.$block.find('#j-product-city-select').autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
                valueInput: geo.$block.find('#j-product-city-value'), params:{reg:0, country: geo.country},
                suggest: app.regionPreSuggest,
                onSelect: function(cityID, cityTitle, ex){
                    geo.fn.onCity(cityID, ex);
                },
                doPrepareText: function(html){
                    var regionTitlePos = html.toLowerCase().indexOf('<br');
                    if( regionTitlePos != -1 ) {
                        html = html.substr(0, regionTitlePos);
                    }
                    html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
                    return $.trim(html);
                }
            }, function(){ geo.cityAC = this; });
            if( ! geo.country){
                geo.$regionBlocks.addClass('displaynone');
            }

            return {
                onCity: function(cityID, ex)
                {
                    if( ! ex.changed ) return;
                    geo.cityID = cityID;
                    app.inputError(geo.$block.find('#j-product-city-value'), false);
                }
            };
        }());

        // images
        img.url = bff.ajaxURL('shop&ev=img&hash='+app.csrf_token+'&item_id='+o.itemID, '');
        img.$block = $f.find('.j-images');
        img.type = {
            $ajax: img.$block.find('.j-images-type-ajax'),
            $simple: img.$block.find('.j-images-type-simple')
        };
        img.$togglers = img.$block.find('.j-images-type');
        img.$togglers.on('click', '.j-images-toggler', function(e){ nothing(e);
            var type = $(this).data('type');
            img.$togglers.addClass('hide');
            img.$togglers.filter('.j-images-type-' + type).removeClass('hide');
            img.$block.find('.j-images-type-value').val(type);
        });

        img.uploader = new qq.FileUploaderBasic({
            button: null,
            action: img.url+'upload',
            limit: intval(o.imgLimit), sizeLimit: o.imgMaxSize,
            uploaded: intval(o.imgUploaded),
            multiple: true, allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                return imgProgress(id, 'start', false);
            },
            onComplete: function(id, fileName, resp) {
                if(resp && resp.success) {
                    imgProgress(id, 'preview', resp);
                    imgRotate(true);
                } else {
                    if(resp.errors) {
                        app.alert.error(resp.errors);
                        imgProgress(id, 'remove');
                    }
                }
                return true;
            },
            onCancel: function(id, fileName) {
                imgProgress(id, 'remove');
            },
            showMessage: function(message, code) {
                app.alert.error(message);
            },
            messages: {
                typeError: o.lang.upload_typeError,
                sizeError: o.lang.upload_sizeError,
                minSizeError: o.lang.upload_minSizeError,
                emptyError: o.lang.upload_emptyError,
                limitError: o.lang.upload_limitError,
                onLeave: o.lang.upload_onLeave
            }
        });
        img.type.$ajax.find('.j-img-upload .j-img-link').each(function(){
            img.uploader._createUploadButton(this);
        });

        for(var i in o.imgData) {
            if( o.imgData.hasOwnProperty(i) ) {
                imgProgress('img'+i, 'start', true);
                imgProgress('img'+i, 'preview', o.imgData[i]);
            }
        }
        imgRotate();

        img.type.$ajax.on('click', '.j-img-delete', function(e){ nothing(e);
            imgRemove( $(this).data('id') );
        });

        dp.$block = $f.find('#j-product-form-dp');

        dp.$block.on('change', '.j-dp-child', function(){
            var $el = $(this);
            var t = o.typeID;
            if( ! t){
                t = intval(f.$field('type').filter(':checked').val());
            }
            var id = $el.data('id');
            var prefix = $el.data('name');
            var val = $el.val();
            var key = id+'-'+val;
            if( ! intval(val)) dp.child[key] = {form:''};
            if( dp.child.hasOwnProperty(key) ) {
                var data = dp.child[key];
                $('.j-dp-child-'+id, dp.$block).html( ( data.form.length ? data.form : '') ).toggleClass('hidden', ! data.form.length);
            } else {
                bff.ajax(bff.ajaxURL('shop','dp-child'), {dp_id:id, dp_value:val, name_prefix:prefix, type:t}, function(data){
                    if(data && data.success) {
                        dp.child[key] = data;
                        $('.j-dp-child-'+id, dp.$block).html( ( data.form.length ? data.form : '') ).toggleClass('hidden', ! data.form.length);
                    }
                });
            }
        });

        dp.$price = $f.find('#j-product-form-price');

        dp.$price.on('focus', '.j-price-val-ch', function(){
            $('#j-price-val').prop('checked', true);
        });

        var unloadProcessed = false;
        app.$W.bind('beforeunload', function(){
            if( ! unloadProcessed && intval(o.itemID) === 0) {
                unloadProcessed = true;
                var fn = [];
                for(var i in img.active) {
                    if( img.active.hasOwnProperty(i) && img.active[i].data!==false ) {
                        var data = img.active[i].data;
                        if( data.tmp ) {
                            fn.push( data.filename );
                        }
                    }
                }
                if( fn.length ) {
                    bff.ajax(img.url+'delete-tmp', {filenames:fn}, false, false, {async:false});
                }
            }
        });

    }

    function imgProgress(id, step, data)
    {
        var $slot, $preview;
        switch(step)
        {
            case 'start': {
                $slot = img.type.$ajax.find('.j-img-slot:not(.j-active) .j-img-upload:visible:first').closest('.j-img-slot');
                if( $slot.length ) {
                    img.active[id] = {slot:$slot,data:false};
                    $slot.addClass('j-active'+(data === false ? ' '+img.classes.progress : ''));
                    $slot.find('.j-img-img').attr('src', app.rootStatic+'/img/loader.gif');
                    $slot.find('.j-img-preview').removeClass('hidden');
                    $slot.find('.j-img-upload').addClass('hidden');
                }else{
                    return false;
                }
            } break;
            case 'preview': {
                if( img.active.hasOwnProperty(id) ) {
                    $slot = img.active[id].slot;
                    img.active[id].data = data;
                    $slot.removeClass(img.classes.progress)
                    $slot.find('.j-img-img').attr('src', data.i);
                    $slot.find('.j-img-fn').val(data.filename).attr('name','images['+data.id+']');
                    $slot.find('.j-img-delete').data('id', id);
                }
            } break;
            case 'remove': {
                imgRemove(id);
            } break;
        }
        return true;
    }

    function imgRotate(update)
    {
        var $slots = img.type.$ajax.find('.j-img-slots');
        if(update === true) {
            $slots.sortable('refresh');
        } else {
            $slots.sortable({
                items: '.j-active'
            });
        }
    }

    function imgRemove(id)
    {
        if( img.active.hasOwnProperty(id) )
        {
            var $slot = img.active[id].slot;
            var data = img.active[id].data;
            var clearSlot = function(id, $slot) {
                img.type.$ajax.find('.j-img-upload:visible:last').closest('.j-img-slot').after($slot);
                $slot.find('.j-img-img').attr('src', '');
                $slot.find('.j-img-fn').val('').attr('name','');
                $slot.find('.j-img-preview').addClass('hidden');
                $slot.find('.j-img-upload').removeClass('hidden');
                $slot.removeClass('j-active');

                img.uploader.decrementUploaded();
                delete img.active[id];
                imgRotate(true);
            };
            if( data !== false ) {
                bff.ajax(img.url+'delete',
                    {image_id:data.id, filename:data.filename}, function(resp){
                        if(resp && resp.success) {
                            clearSlot(id, $slot);
                        } else {
                            app.alert.error(resp.errors);
                        }
                    });
            } else {
                clearSlot(id, $slot);
            }
        }
    }

    function setDp()
    {
        var id = 0;
        var $bl = $f.find('.j-cat');
        if($bl.length){
            id = intval($bl.find('.j-cat-value').val());
        }
        if(id){
            if(dp.cache.hasOwnProperty(id)){
                dp.$block.html(dp.cache[id]);
                dp.$price.html(dp.price[id]);
            }else{
                bff.ajax(bff.ajaxURL('shop', 'dp-form'), {id:id}, function(data){
                    if(data && data.success){
                        if(data.hasOwnProperty('dp')){
                            dp.$block.html(dp.cache[id] = data.dp);
                        }
                        if(data.hasOwnProperty('price')) {
                            dp.$price.html(dp.price[id] = data.price);
                        }
                    }else{
                        dp.$block.html('');
                        dp.$price.html('');
                    }
                });
            }
        }else{
            dp.$block.html('');
            dp.$price.html('');
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());


