var jPortfolioOwnerList = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;
    var $specTitle;

    function init()
    {
        $block = $('#j-portfolio-owner-list');

        $form = $('#j-portfolio-owner-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        $specTitle = $form.find('.j-f-spec-title');

        $form.on('click', '.j-f-spec-select', function(){
            var $el = $(this);
            $form.find('[name="spec"]').val($el.data('id'));
            closePopup($el);
            massActions();
            listMngr.submit({}, true);
            return false;
        });

        $list.on('click', '.j-delete', function(){
            if(confirm(o.lang.item_delete)){
                bff.ajax(bff.ajaxURL('portfolio', 'item-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        listMngr.submit({popstate:true}, false);
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $list.on('click', '.j-spec-edit', function(){
            var $el = $(this);
            var id = intval($el.data('id'));
            var $bl = $el.closest('.j-spec');
            var $descr = $bl.find('.j-descr');
            if( ! $descr.length) return false;
            bff.ajax(bff.ajaxURL('portfolio', 'spec-edit-form'), {id:id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success){
                    $descr.addClass('hidden');
                    $bl.find('.j-edit').addClass('hidden');
                    $descr.after(resp.form);
                    var $f = $('#j-owner-spec-form-'+id);
                    var f = app.form($f, function(){
                        bff.ajax(bff.ajaxURL('portfolio', 'spec-edit'), $f.serialize(), function(data, errors){
                            if(data && data.success) {
                                $descr.html(data.descr);
                                f.alertSuccess(o.lang.spec_save_success);
                                if($f.find('[ name="order"]:checked').length){
                                    listMngr.submit({}, true);
                                }else{
                                    formClose();
                                }
                            } else {
                                f.fieldsError(data.fields, errors);
                            }
                        });
                    });

                    $f.on('click', '.j-cancel', function(){
                        formClose();
                        return false;
                    });

                    $f.find('.j-after-select').focus(function(){
                        $f.find('.j-after-check').prop('checked', true);
                    });

                    function formClose(){
                        $f.remove();
                        $descr.removeClass('hidden');
                        $bl.find('.j-edit').removeClass('hidden');
                    }
                }else{
                    app.alert.error(errors);
                }
            });

            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onAfterDeserialize: function() {
                massActions();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });
    }

    function closePopup($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function massActions()
    {
        var spec = intval($form.find('[name="spec"]').val());
        if(o.specs.hasOwnProperty(spec)){
            $specTitle.html(o.specs[spec]['t']);
        }
    }


    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
