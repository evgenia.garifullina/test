var jUsersSearch = (function(){

    var inited = false, o = {lang:{}, limitInvite: 10}, listMngr;
    var $main, $block, $form, $list, $pgn;
    var geo = {$block:0, $city:0, $country:0, country:0, cityAC:0,cityID:0,cityPreSuggest:{}, cityName:{}, metro:{name:{}, data:{}}};
    var map = {$block:0, $map:0, map:0, clusterer:0, points:[], bffmap:0, mapInfoWindow:0, mapMarkers:0};
    var dp = {$block:0, cache:{}, child:{}, t:1, id:0};
    var specscache = {};
    var onPopstate = false;
    var m = 0;
    var selectInvite = {};

    function init()
    {
        $main = $('#j-users-search');
        $block = $('#j-users-search-list');

        $form = $('#j-users-search-form-block').find('form');
        $order_specializations = $form.find('[name="fs"]');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');
        map.$map = $block.find('#map-desktop');
        map.$block = map.$map.parent();

        $(document).on('click', '.select-invite', function() {
            var $obj = $(this);
            var id = $obj.data('id');
            if ($obj.hasClass('active')) {
                delete selectInvite[id];
                $obj.removeClass('active');
                $obj.find('input').prop( "checked", false );
            }
            else {
                if (getObjectLength(selectInvite) >= o.limitInvite) {
                    app.alert.error(o.lang.limitAlert)
                    return;
                }
                selectInvite[id] = id;
                $obj.addClass('active');
                $obj.find('input').prop( "checked", true );
            }
            var cnt = getObjectLength(selectInvite);
            if (cnt) {
                $('#inviteBtn i').text(cnt);
                $('#inviteBtn').show();
            }
            else {
                $('#inviteBtn').hide();
            }
        });
        $('.j-list').bind("DOMSubtreeModified",function(){
            $('.j-list button.select-invite').each(function(){
                var id = $(this).data('id');
                if (selectInvite[id]) {
                    $(this).addClass('active').find('input').prop( "checked", true );
                }
            });
        });
        $('#inviteBtn').click(function() {
            if (!getObjectLength(selectInvite)) return;
            redirectPost('/orders/add', {'invite': selectInvite});
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $form.on('click', '.j-cat-title', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            $li.toggleClass('opened');
            $li.find('ul').toggleClass('hidden');
            $el.find('.fa').toggleClass('fa-chevron-down','fa-chevron-right');

            return false;
        });

        var $mobileCats = $form.find('#j-mobile-cats');
        $form.on('click', '.j-mobile-cat', function(){
            var $el = $(this);
            $form.find('#j-mobile-cat-'+$el.data('id')).collapse('show');
            $mobileCats.collapse('hide');
            return false;
        });

        $form.on('click', '.j-mobile-cat-back', function(){
            $(this).closest('.j-mobile-cat-block').collapse('hide');
            $mobileCats.collapse('show');
            return false;
        });

        $form.on('show.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.addClass('active');
            $el.find('i.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        });

        $form.on('hide.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.removeClass('active');
            $el.find('i.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        });

        $(".j-top-search-form").on('change', function(){
            listMngr.submit({}, true);
            return false;
        }).on('click','button', function(){
            listMngr.submit({}, true);
            return false;
        });


        // ��� (��������, ���������..)
        $form.on('click', '.j-cat-check', function(){
            var orderTypes = {};
            var $el = $(this);
            var $li = $el.closest('li');
            if(o.oneSpec) {
                $li.find('.j-cat-title').trigger('click');
                return false;
            }
            $li.removeClass('subchecked').toggleClass('checked selected-type');

            if($li.hasClass('checked')){
                $el.removeClass('fa-circle-o fa-circle').addClass('fa-check-circle');
                $li.find('ul li').addClass('checked');
                $li.find('ul i').removeClass('fa-square fa-check-square fa-check-square-o fa-square-o').addClass('fa-check-square-o');
            }else{
                $el.removeClass('fa-check-circle fa-circle').addClass('fa-circle-o');
                $li.find('ul li').removeClass('checked');
                $li.find('ul i').removeClass('fa-square fa-check-square-o').addClass('fa-square-o');
            }
            ordersValueSpecs(false);
            listMngr.submit({}, true);
            return false;
        });

        // ������������� (��������������, ��������� ...)
        $form.on('click', '.j-spec', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            if(o.oneSpec){
                if($li.hasClass('checked')){
                    document.location = o.locationDefault;
                    return false;
                }
                var url = $el.data('url');
                if(url){
                    document.location = url;
                    return false;
                }
            }else{
                $li.toggleClass('checked');
            }
            correctSpecParent($li);
            ordersValueSpecs();
            listMngr.submit({}, true);
            return false;
        });

        $form.on('click', '.j-spec-nocat', function(){
            var $li = $(this).closest('li');
            $li.toggleClass('checked');
            $li.find('i.fa').toggleClass('fa-square-o fa-check-square-o');

            var v = [];
            var s = {};
            var nSpec = 0;
            $form.find('li.checked > .j-spec-nocat').each(function(){
                var $el = $(this);
                v.push($el.data('keyword'));
                nSpec = intval($el.data('id'));
                s[ $el.data('id') ] = 1;
            });
            listMngr.submit({}, true);
            return false;
        });

        // ��������� (������������� �����, ����������� ..)
        $form.on('click', '.j-cat', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            if(o.oneSpec){
                if($li.hasClass('checked')){
                    document.location = o.locationDefault;
                    return false;
                }
                var url = $el.data('url');
                if(url){
                    document.location = url;
                    return false;
                }
            }else{
                $li.toggleClass('checked');
            }
            correctCategoryParent($li);
            ordersValueSpecs(false);
            listMngr.submit({}, true);
            return false;
        });

        $form.on('click', '.j-cat-parent', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            if(o.oneSpec){
                var url = $el.data('url');
                if(url){
                    document.location = url;
                    return false;
                }
            }else{
                $li.toggleClass('checked');
                var $i = $li.find('i');
                if($li.hasClass('checked')){
                    $i.removeClass('fa-circle-o').addClass('fa-check-circle');
                }else{
                    $i.addClass('fa-circle-o').removeClass('fa-check-circle');
                }
                ordersValueCats();
            }
            listMngr.submit({}, true);
            return false;
        });

        function ordersValueSpecs(bmobile)
        {
            var orderSpecs = {};
            var v = [];
            var s = {};
            var nSpec = 0;
            if(bmobile){
                $form.find('.j-mobile-spec:checked').each(function(){
                    var $el = $(this);
                    v.push($el.val());
                    if( ! s.hasOwnProperty($el.data('cat'))){
                        s[ $el.data('cat') ] = {};
                    }
                    s[ $el.data('cat') ][ $el.data('id') ] = 1;
                    nSpec = intval($el.data('id'));
                });
            }else{
                $form.find('li.spec-li.checked').each(function(){
                    var $el = $(this);
                    if (typeof(orderSpecs[$el.data('cat')]) === 'object' )
                    {
                        orderSpecs[$el.data('cat')][orderSpecs[$el.data('cat')].length] = $el.data('id');
                    }
                    else
                    {
                        orderSpecs[$el.data('cat')] = [];
                        orderSpecs[$el.data('cat')][orderSpecs[$el.data('cat')].length] = $el.data('id');
                    }
                    if( ! s.hasOwnProperty($el.data('cat'))){
                        s[ $el.data('cat') ] = {};
                    }
                    s[ $el.data('cat') ][ $el.data('id') ] = 1;
                    nSpec = intval($el.data('id'));
                });
            }
            if ($.isEmptyObject(orderSpecs))
            {
                $order_specializations.val(null);
            }
            else
            {
                $order_specializations.val(JSON.stringify(orderSpecs));
            }
        }

        function correctSpecParent($li)
        {
            if ($li.hasClass('checked'))
            {
                $li.find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
            }
            else
            {
                $li.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
            }
            var $ul = $li.parent();
            var $pli = $ul.closest('li');
            var $plii = $pli.find('i.category-i');

            var $typeLi = $li.closest('li.type-li');
            var $typeUl = $typeLi.children('ul');
            var $typeLiI = $typeLi.find('i.type-i');

            checkParent($ul, $pli, $plii, 'li.spec-li', 'fa-check-square-o', 'fa-square', 'fa-square-o');
            checkParent($typeUl, $typeLi, $typeLiI, 'li', 'fa-check-circle', 'fa-circle', 'fa-circle-o');
        }

        function checkParent($ul, $parentLi, $parentLiI, liClass, checkedClass, subcheckedClass, defaultClass)
        {
            var liCount = $ul.find(liClass).length;
            var liCheckedCount = $ul.find(liClass + '.checked').length;

            if (liCheckedCount == 0)
            {
                $parentLi.removeClass('checked subchecked');
                $parentLiI.removeClass(checkedClass + ' ' + subcheckedClass).addClass(defaultClass);
            }
            else if (liCheckedCount == liCount)
            {
                $parentLi.removeClass('subchecked').addClass('checked');
                $parentLiI.removeClass(subcheckedClass + ' ' + defaultClass).addClass(checkedClass);
            }
            else
            {
                $parentLi.removeClass('checked').addClass('subchecked');
                $parentLiI.removeClass(checkedClass + ' ' + defaultClass).addClass(subcheckedClass);
            }
        }

        function correctCategoryParent($li)
        {
            if ($li.hasClass('checked'))
            {
                $li.find('i').removeClass('fa-square fa-square-o').addClass('fa-check-square-o');
                $li.find('li').removeClass('checked').addClass('checked');
            }
            else
            {
                $li.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
                $li.find('li').removeClass('checked');
            }
            var $ul = $li.parent();
            var $pli = $ul.closest('li');
            var $plii = $pli.find('i.category-i');

            var $typeLi = $li.closest('li.type-li');

            var cnt = $ul.find('li').length;
            var ch = $ul.find('li.checked').length;

            checkParent($ul, $typeLi, $typeLi.find('i.type-i'), 'li.category-li', 'fa-check-circle', 'fa-circle', 'fa-circle-o');
        }

        geo.fn = (function(){
            geo.$block = $('#j-left-region');
            geo.$city = geo.$block.find('#j-region-city-select');
            var $cityVal = geo.$block.find('#j-region-city-value');
            geo.$metro = geo.$block.find('#j-region-metro-select');
            var $metroVal = geo.$block.find('#j-region-metro-value');
            var $region = $form.find('[name="r"]');
            geo.country = intval(o.defCountry);
            geo.cityID = intval($cityVal.val());

            geo.$country = geo.$block.find('#j-left-region-country');
            if(geo.$country.length){
                geo.$country.change(function(){
                    geo.country = intval(geo.$country.val());
                    if(geo.country){
                        initCity();
                        geo.cityAC.setParam('country', geo.country);
                        geo.$city.addClass('hidden');
                        if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
                            geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
                        }else{
                            bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
                                geo.cityAC.setSuggest[geo.country] = data;
                                geo.cityAC.setSuggest(data, true);
                            });
                        }
                        geo.$city.removeClass('hidden');
                    }else{
                        geo.$city.addClass('hidden');
                        geo.$metro.addClass('hidden');
                        $region.val(0);
                        $cityVal.val(0);
                        $metroVal.val(0);
                        if( ! onPopstate) {
                            listMngr.submit({}, true);
                        }
                    }
                    return true;
                });
                geo.country = intval(geo.$country.val());
            }else{
                $form.on('shown.bs.collapse', '#j-left-region', function(){
                    initCity();
                });
            }

            var cityInited = false;
            function initCity(){
                if(cityInited) return;
                cityInited = true;
                var hidden = false;
                if(geo.$city.hasClass('hidden')){
                    hidden = true;
                    geo.$city.removeClass('hidden');
                }
                geo.$city.autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
                    valueInput: $cityVal, params:{reg:1, country: geo.country},
                    suggest: o.preSuggest ? o.preSuggest : app.regionPreSuggest,
                    onSelect: function(cityID, cityTitle, ex){
                        geo.cityName[intval(cityID)] = cityTitle;
                        if(ex.hasOwnProperty('data') && ex.data.hasOwnProperty(3)){
                            $region.val(ex.data[3]);
                        }else{
                            $region.val(0);
                        }
                        geo.fn.onCitySelect(cityID);
                    },
                    doPrepareText: function(html){
                        var regionTitlePos = html.toLowerCase().indexOf('<br');
                        if( regionTitlePos != -1 ) {
                            html = html.substr(0, regionTitlePos);
                        }
                        html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
                        return $.trim(html);
                    }
                }, function(){ geo.cityAC = this; });
                if(hidden)  geo.$city.addClass('hidden');
            }
            if(geo.$city.is(':visible')) initCity();

            var metroInited = false;
            function initMetro(){
                if(metroInited) return;
                metroInited = true;
                var hidden = false;
                if(geo.$metro.hasClass('hidden')){
                    hidden = true;
                    geo.$metro.removeClass('hidden');
                }
                geo.$metro.autocomplete( bff.ajaxURL('geo', 'metro-suggest'),{
                    valueInput: $metroVal, params:{reg:1, city: geo.cityID},
                    onSelect: function(metroID, metroTitle, ex){
                        geo.metro.name[intval(metroID)] = metroTitle;
                        geo.fn.onMetroSelect(metroID);

                    },
                    doPrepareText: function(html){
                        var pos = html.toLowerCase().indexOf('<br');
                        if( pos != -1 ) {
                            html = html.substr(0, pos);
                        }
                        html = html.replace(/<\/?[^>]+>/gi, '');
                        return $.trim(html);
                    }
                }, function(){ geo.metroAC = this; });
                if(hidden)  geo.$metro.addClass('hidden');
            }
            if(geo.$metro.is(':visible')) initMetro();

            return {
                onCitySelect:function(cityID){
                    geo.cityID = cityID;
                    initMetro();
                    geo.metroAC.setParam('city', geo.cityID);
                    geo.$metro.addClass('hidden');
                    if( geo.metro.data.hasOwnProperty(geo.cityID) ) {
                        if(geo.metro.data[geo.cityID]){
                            geo.$metro.removeClass('hidden');
                        }else{
                            geo.$metro.addClass('hidden');
                        }
                    }else{
                        bff.ajax(bff.ajaxURL('geo', 'has-metro'), {city:geo.cityID}, function(data){
                            if(data && data.success){
                                geo.metro.data[geo.cityID] = data.hasMetro;
                                if(geo.metro.data[geo.cityID]){
                                    geo.$metro.removeClass('hidden');
                                }else{
                                    geo.$metro.addClass('hidden');
                                }
                            }
                        });
                    }
                    geo.$metro.val('');
                    $metroVal.val(0);

                    if( ! onPopstate) {
                        listMngr.submit({}, true);
                    }
                },
                onMetroSelect:function(metroID){
                    if( ! onPopstate) {
                        listMngr.submit({}, true);
                    }
                },
                clear:function(){
                    if (geo.$country.length) {
                        geo.$country.val(0);
                        geo.$city.addClass('hidden');
                        geo.$metro.addClass('hidden');
                    }
                    $region.val(0);
                    $cityVal.val(0); geo.$city.val('');
                    $metroVal.val(0); geo.$metro.val('');
                },
                onPopstate:function(){
                    if(geo.$country.length){
                        if( ! geo.$country.find(':selected').length){
                            geo.$country.find(':first').prop('selected', true);
                        }
                    }
                    var city = intval(geo.$block.find('#j-region-city-value').val());
                    if(geo.cityName.hasOwnProperty(city)){
                        geo.$city.val(geo.cityName[city]);
                    }
                    var metro = intval($metroVal.val());
                    if(geo.metro.name.hasOwnProperty(metro)){
                        geo.$metro.val(geo.metro.name[metro]);
                    }
                    if( geo.metro.data.hasOwnProperty(city) ) {
                        if(geo.metro.data[city]){
                            geo.$metro.removeClass('hidden');
                        }else{
                            geo.$metro.addClass('hidden');
                        }
                    }
                }
            };
        }());

        var flags = {};
        $form.find('.j-checkbox-flag').each(function(){
            var $el = $(this);
            var name = $el.data('name');
            flags[name] = {
                $el:$el,
                $input:$form.find('[name="'+name+'"]')
            };
        });

        $form.on('click', '.j-checkbox-flag', function(){
            var $el = $(this);
            var name = $el.data('name');
            $el.toggleClass('checked');
            flags[name].$input.val($el.hasClass('checked') ? 1 : 0);
            $el.find('i.fa').toggleClass('fa-check-square-o fa-square-o');
            if( ! onPopstate) {
                listMngr.submit({}, true);
            }
        });

        $form.on('click', '.j-clear-filter', function(){
            geo.fn.clear();
            $form.find('input:text, input:password, select, textarea').val('');
            $form.find('input:radio, input:checkbox').prop('checked', false);
            for(var i in flags){
                if(flags.hasOwnProperty(i)){
                    flags[i].$el.removeClass('checked');
                    flags[i].$input.val(0);
                    flags[i].$el.find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
                }
            }
            listMngr.submit({}, true);
            $form.find('.j-collapse.in:not(:first)').collapse('hide');
            return false;
        });

        $block.on('click', '.j-f-view-type', function(){
            var $el = $(this);
            m = intval($el.data('id'));
            $el.parent().addClass('active').siblings().removeClass('active');
            $form.find('[name="m"]').val(m);
            if(m){
                map.$block.removeClass('hidden');
                $list.before($pgn);
                map.fn.init();
            }else{
                map.$block.addClass('hidden');
                $pgn.before($list);
            }

            listMngr.submit({}, true);
            return false;
        });

        $block.on('click', '.j-map-marker', function(){
            if(map.bffmap.isYandex()){
                var n = intval($(this).data('n'));
                map.fn.openBalloon(n);
            }else if(map.bffmap.isGoogle()){
                var id = intval($(this).data('id'));
                map.fn.popupMarker(id, false);
            }
            $.scrollTo(map.$block, {duration:500, offset:-150});
            return false;
        });

        $form.on('change', '.j-input-change', function(){
            listMngr.submit({}, true);
        });


        m = intval($form.find('[name="m"]').val());
        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    if(m){
                        $.scrollTo(map.$block, {offset: -150, duration:500});
                    }else{
                        $.scrollTo($list, {offset: -150, duration:500});
                    }
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('count')){
                    $main.find('.j-users-count').text(resp.count);
                }
                if(resp.hasOwnProperty('points') && resp.points){
                    map.fn.showPoints(resp.points);
                }else{
                    map.fn.clearPoints();
                }

            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function($form, query) {
            },
            onAfterDeserialize: function($form, query){
                dp.$block.find('.j-dp-parent').each(function(){
                    addDpChild($(this));
                });
                $form.deserialize(query, true);
                openDp();
                for(var i in flags){
                    if(flags.hasOwnProperty(i)){
                        if(intval(flags[i].$input.val())){
                            flags[i].$el.addClass('checked');
                            flags[i].$el.find('i.fa').addClass('fa-check-square-o').removeClass('fa-square-o');
                        }else{
                            flags[i].$el.removeClass('checked');
                            flags[i].$el.find('i.fa').addClass('fa-square-o').removeClass('fa-check-square-o');
                        }
                    }
                }
                geo.fn.onPopstate();
            },
            ajax: o.ajax
        });

        map.fn = (function()
        {
            var inited = false;
            function init(){
                if (inited) return;
                inited = true;

                if (o.coordDefaults) {
                    o.coordDefaults = o.coordDefaults.split(',');
                } else {
                    o.coordDefaults = [55.76, 37.64];
                }

                map.bffmap = app.map(map.$map.get(0), o.coordDefaults, function(mmap){
                    if (this.isYandex()) {
                        map.map = mmap;
                        map.clusterer = new ymaps.Clusterer({
                            preset: 'islands#blueIcon',
                            groupByCoordinates:true,
                            clusterBalloonLeftColumnWidth:50
                        });
                    } else if (this.isGoogle()) {
                        map.mapInfoWindow = new google.maps.InfoWindow({});
                    }
                    if(o.points && o.points.length) showPoints(o.points);
                }, {controls: 'search'});
            }

            function showPoints(points){
                if ( ! inited) {
                    o.points = points;
                    return;
                }
                if ( ! map.bffmap) return;

                if (map.bffmap.isYandex()) {

                    clearPoints();
                    map.points = [];

                    for (var i in points) {
                        if (points.hasOwnProperty(i)) {
                            var coord;
                            if (o.coordorder == 'longlat') {
                                coord = [points[i].lng, points[i].lat];
                            } else {
                                coord = [points[i].lat, points[i].lng];
                            }
                            map.points[i] = new ymaps.Placemark(
                                coord,
                                {
                                    balloonContentBody: points[i].b,
                                    clusterCaption: points[i].n
                                }, {
                                    preset: 'islands#blueIcon'
                                });

                        }
                    }
                    map.clusterer.add(map.points);
                    map.map.geoObjects.add(map.clusterer);

                    map.map.setBounds(map.clusterer.getBounds(), {
                        checkZoomRange: true
                    });
                }
                else if (map.bffmap.isGoogle())
                {
                    if (map.clusterer) {
                        map.clusterer.clearMarkers();
                    }
                    var mapG = map.bffmap.getMap();
                    map.mapMarkers = {};
                    var mapMarkersToCluster = [];
                    map.mapInfoWindow.close();

                    for (var i in points) {
                        if (points.hasOwnProperty(i)) {
                            var v = points[i];
                            var id = intval(v.id);
                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(parseFloat(v.lat), parseFloat(v.lng))
                            });
                            marker.itemID = id;
                            map.mapMarkers[id] = {
                                position: marker.getPosition(),
                                balloon: v.b
                            };
                            mapMarkersToCluster.push(marker);
                            google.maps.event.addListener(marker, 'click', function () {
                                map.fn.popupMarker(this.itemID, true);
                            });
                        }
                    }

                    map.clusterer = new MarkerClusterer(mapG, mapMarkersToCluster, {
                        imagePath: app.rootStatic+'/js/markerclusterer/images/m'
                    });
                    if (mapMarkersToCluster.length > 1) {
                        map.clusterer.fitMapToMarkers();
                    }
                    map.bffmap.refresh();
                }
            }

            function clearPoints()
            {
                if ( ! inited) return;
                if (map.bffmap.isYandex() && map.clusterer !== 0) {
                    map.clusterer.removeAll();
                    map.map.geoObjects.remove(map.clusterer);
                }
            }

            function openBalloon(id)
            {
                if ( ! map.points.hasOwnProperty(id)) return;
                if (map.bffmap.isYandex()) {
                    try {
                        var geoObjectState = map.clusterer.getObjectState(map.points[id]);
                        if (geoObjectState.isShown) {
                            if (geoObjectState.isClustered) {
                                geoObjectState.cluster.state.set('activeObject', map.points[id]);
                                map.clusterer.balloon.open(geoObjectState.cluster);
                            } else {
                                try {
                                    map.points[id].balloon.open();
                                } catch (e) {
                                    map.map.setCenter(
                                        map.points[id].geometry.getCoordinates()
                                    ).then(function () {
                                            map.points[id].balloon.open();
                                        }, function (err) {
                                            bff_report_exception(err);
                                        }, this);
                                }
                            }
                        } else {
                            map.map.setCenter(
                                map.points[id].geometry.getCoordinates()
                            ).then(function () {
                                    openBalloon(id);
                                }, function (err) {
                                    bff_report_exception(err);
                                }, this);
                        }
                    } catch (e) {
                        bff_report_exception(e);
                    }
                }
            }

            function popupMarker(itemID, markerClick)
            {
                if (map.bffmap.isGoogle()) {
                    if (map.mapMarkers.hasOwnProperty(itemID)) {
                        var m = map.mapMarkers[itemID];
                        if (markerClick!==true) {
                            map.bffmap.getMap().panTo(m.position);
                        }
                        map.mapInfoWindow.close();
                        map.mapInfoWindow.setPosition(m.position);
                        map.mapInfoWindow.setContent(m.balloon);
                        map.mapInfoWindow.open(map.bffmap.getMap());
                    }
                }
                return false;
            }

            return {
                init:init,
                showPoints:showPoints,
                openBalloon:openBalloon,
                clearPoints:clearPoints,
                popupMarker:popupMarker
            }

        }());

        if(map.$map.is(':visible')){
            map.fn.init();
        }

        if(intval(o.currSpec)){
            dp.id = o.currSpec;
        }

        dp.$block = $form.find('#j-users-search-form-dp');

        dp.$block.on('change', '.j-dp-parent', function(){
            addDpChild($(this));
        });

        dp.$block.on('change', function(){
            listMngr.submit({}, true);
        });
    }

    function addDpChild($el)
    {
        var id = $el.data('id');
        var val = $el.val();
        var key = id+'-'+val;
        if($el.is(':checked')){
            var $dp = $('#j-dp-child-'+key, dp.$block);
            if($dp.length){
                $dp.toggleClass('hidden', false);
            }else{
                var $jdp = $el.closest('.j-dp');

                if( ! dp.child.hasOwnProperty(dp.t)) dp.child[dp.t] = {};
                if(dp.child[dp.t].hasOwnProperty(key)){
                    var data = dp.child[dp.t][key];
                    $jdp.after(data.form);
                    reorderChildDp($jdp, id);
                }else{
                    bff.ajax(bff.ajaxURL('users','dp-child-search'), {dp_id:id, dp_value:val, id:dp.id}, function(data){
                        if(data && data.success) {
                            if(data.form.length){
                                dp.child[dp.t][key] = data;
                                $jdp.after(data.form);
                                reorderChildDp($jdp, id);
                            }
                        }
                    });
                }
            }
        }else{
            $('#j-dp-child-'+key, dp.$block).toggleClass('hidden', true);
        }
    }

    function reorderChildDp($jdp, id)
    {
        var $childs = dp.$block.find('.j-dp-child-'+id);
        if($childs.length > 1){
            $childs.sort(function(a,b){
                var a_num = intval( $(a).data('num') );
                var b_num = intval( $(b).data('num') );
                return a_num < b_num ? 1 : -1;
            });
            $.each($childs, function(index, row){
                $jdp.after(row);
            });
        }
    }

    function openDp()
    {
        dp.$block.find('.j-dp').each(function(){
            var $bl = $(this);
            var open = $bl.find('input[type="checkbox"]:checked').length > 0;
            if( ! open){
                $bl.find('input[type="text"]').each(function(){
                    if(intval($(this).val())){
                        open = true;
                    }
                });
            }
            if(open){
                $bl.collapse();
            }
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
