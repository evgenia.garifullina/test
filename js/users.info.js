var jUsersInfo = (function(){
    var inited = false, o = {lang:{}, url_info:''};
    var $block;

    function init()
    {
        $block = $('#j-owner-info-block');

        $block.find('#j-favs-all').on('click', function(){
            var $el = $(this);
            bff.ajax(o.url_info, {id:$el.data('id'), act:'favs-all', hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $block.find('#j-favs').html(resp.html);
                    $el.remove();
                    if(o.my){
                        myInit();
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        if(o.my){

            var $my = $block.find('#j-favs-my');
            $block.find('#j-favs-my-all').on('click', function(){
                var $el = $(this);
                bff.ajax(o.url_info, {act:'favs-my-all', hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $my.html(resp.html);
                        $el.remove();
                        myInit();
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

            $my.on('click', '.j-delete-fav', function(){
                var $el = $(this);
                bff.ajax(bff.ajaxURL('users', 'fav-del'), {id:$el.data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.closest('.j-my-fav').remove();
                    } else {
                        app.alert.error(errors);
                    }
                });

                return false;
            });

            myInit();
        }
    }

    function myInit()
    {
        if(typeof(jUsersNote) == 'object'){
            jUsersNote.$init();
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());
