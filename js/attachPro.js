var jAttachPro = (function() {
    function initAttach(options) {
        var o = {
            lang: {upload_typeError: '', upload_sizeError: '', upload_minSizeError: '', upload_emptyError: '', upload_limitError: '', upload_onLeave: ''},
            itemID: 0,
            jSelector: '',
            module: ''
        };
        var attach = {url: '', $block: 0, uploader: 0};

        $.extend(true, o, options || {});

        function checkDownloadAll() //Показывать ли кнопку скачать все
        {
            if(attach.$list.find('.j-attach').length > 1){
                $('.j-download-all').show();
            }else{
                $('.j-download-all').hide();
            }
        }

        function attachAdd(data)
        {
            console.log(data)
            attach.$list.append('<li class="j-attach">'+
                '<a href="#" class="ajax-link j-delete" data-id="'+data.id+'" data-filename="'+data.filename+'"><i class="fa fa-trash"></i></a>'+
                '<a href="'+data.i+'" download="'+data.origin+'" target="_blank">'+data.origin+'</a> ('+data.size+')'+
                (intval(o.itemID) ? '' : '<input type="hidden" class="j-attachfn" name="attach[' + data.id + ']" value="'+(data.filename)+'" />') +
                '</li>');
            if(attach.$list.find('.j-attach').length >= intval(o.attachLimit)){
                attach.$block.find('.j-upload').hide();
            }
            checkDownloadAll();
        }
        // var unloadProcessed = false;
        // app.$W.bind('beforeunload', function () {
        //     if (!unloadProcessed && intval(o.itemID) === 0) {
        //         unloadProcessed = true;
        //         var fn = [];
        //         for (var i in img.active) {
        //             if (img.active.hasOwnProperty(i) && img.active[i].data !== false) {
        //                 var data = img.active[i].data;
        //                 if (data.tmp) {
        //                     fn.push(data.filename);
        //                 }
        //             }
        //         }
        //         if (fn.length) {
        //             bff.ajax(img.url + 'delete-tmp', {filenames: fn}, false, false, {async: false});
        //         }
        //         fn = [];
        //         attach.$list.find('.j-attachfn').each(function () {
        //             fn.push($(this).val());
        //         });
        //         if (fn.length) {
        //             bff.ajax(attach.url + 'delete-tmp', {filenames: fn}, false, false, {async: false});
        //         }
        //     }
        // });


        attach.url = bff.ajaxURL(o.module + '&ev=attachPro&hash=' + app.csrf_token + '&item_id=' + o.itemID, '');
        attach.$block = $(o.jSelector);
        attach.$list = attach.$block.find('.j-list');
        attach.$progress = attach.$block.find('.j-progress');
        attach.$descr = attach.$block.find('.j-loader-description');

        console.log('!!!!!!', o.jSelector, attach.$block, attach.$block.find('.j-upload').get(0), attach.url + 'upload');

        attach.uploader = new qq.FileUploaderBasic({
            button: attach.$block.find('.j-upload').get(0),
            action: attach.url + 'upload',
            limit: intval(o.attachLimit), sizeLimit: o.attachMaxSize,
            uploaded: intval(o.attachUploaded),
            multiple: true,
            onSubmit: function (id, fileName) {
                attach.$progress.removeClass('hidden');
                attach.$descr.removeClass('hidden');
            },
            onComplete: function (id, fileName, resp) {
                attach.$progress.addClass('hidden');
                attach.$descr.addClass('hidden');
                if (resp && resp.success) {
                    attachAdd(resp);
                } else {
                    if (resp.errors) {
                        app.alert.error(resp.errors);
                    }
                }
                return true;
            },
            showMessage: function (message, code) {
                app.alert.error(message);
            },
            messages: {
                typeError: o.lang.upload_typeError,
                sizeError: o.lang.upload_sizeError,
                minSizeError: o.lang.upload_minSizeError,
                emptyError: o.lang.upload_emptyError,
                limitError: o.lang.upload_limitError,
                onLeave: o.lang.upload_onLeave
            }
        });

        i = 0;
        for (i in o.attachData) {
            if (o.attachData.hasOwnProperty(i)) {
                attachAdd(o.attachData[i]);
            }
        }

        attach.$list.on('click', '.j-delete', function () {
            var $el = $(this);
            bff.ajax(attach.url + 'delete',
                {file_id: $el.data('id'), filename: $el.data('filename')}, function (resp) {
                    if (resp && resp.success) {
                        $el.closest('.j-attach').remove();
                        attach.uploader.decrementUploaded();
                        attach.$block.find('.j-upload').show();
                        checkDownloadAll();
                    } else {
                        app.alert.error(resp.errors);
                    }
                });
            return false;
        });
    }

    return {
        init: function(options)
        {
            initAttach(options);
        }
    };
}());