
// AHTUNG - трэш и садомия
jUser = (function() {
    var inited = false, o = {
        lang:{},
        url_settings:'',
        contactLimit : null,
        contacts : null,

        contactsOptions : null
    };
    var $types, submitAndBack = false;

    function init()
    {
        // походу этой формы уже нет в помине

        $types = $('.j-type');

        initContacts(o.contactLimit, o.contacts);
    }

    function initContacts(limit, contacts)
    {
        var index  = 0, total = 0;
        var $block = $('#j-user-contacts');
        var $add = $('#j-add-contact');

        function add(contact)
        {
            contact = contact || {};
            if(limit>0 && total>=limit) return;
            index++; total++;
            var value = '';
            if(contact.hasOwnProperty('v') && contact.v) value = contact.v;
            $block.append('<div class="j-contact">'+
                '<select class="left j-contact-type" name="contacts['+index+'][t]" style="width:85px;">' + o.contactsOptions + '</select>'+
        '<input class="left j-value" type="text" maxlength="40" name="contacts['+index+'][v]" value="'+value.replace(/"/g, "&quot;")+'" style="margin-left:4px;"/>'+
        '<div class="left" style="margin: 3px 0 0 4px;"><a href="#" class="but cross j-remove"></a></div>'+
        '<div class="clear"></div>'+
        '</div>');
            if(contact.hasOwnProperty('t')){
                $block.find('.j-contact:last').find('.j-contact-type').val(contact.t);
            }
            if(limit>0 && total>=limit) {
                $add.hide();
            }
        }

        $block.on('click', '.j-remove', function(e){ nothing(e);
            var $contact = $(this).closest('.j-contact');
            if( $contact.find('.j-value').val() != '' ) {
                if(confirm('Удалить контакт?')) {
                    $contact.remove(); total--;
                }
            } else {
                $contact.remove(); total--;
            }
            if(limit>0 && total<limit) {
                $add.show();
            }
        });

        $add.click(function(e){ nothing(e);
            add();
            return false;
        });

        contacts = contacts || {};
        for(var i in contacts) {
            if( contacts.hasOwnProperty(i) ) {
                add(contacts[i]);
            }
        }
        if( ! total ) {
            add();
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        back: function()
        {
            submitAndBack = true;
        },
        deleteAvatar: function(defaultAvatar)
        {
            if(confirm('Удалить текущий аватар?')) {
                $('#avatar').attr('src', defaultAvatar);
                $('#avatar_delete_flag').val(1);
                $('#avatar_delete_link').remove();
            }
            return false;
        },
        doChangePassword: function(change)
        {
            $('#passwordCurrent, #passwordChange').toggle();
            $('#changepass').val( change );

            if(change)
                $('#password').focus();

            return false;
        },
        onType: function($type)
        {
            $types.hide();
            $types.filter('.j-type-'+$type.val()).show();
        }
    };
}());