var jSpecsSelect = (function(){
    var o = {block:false, cnt:0, limit:0, onSelect:false, onDelete:false, onExists:false, catOnSelect:1, cancelIfExists:0, existsMessage:false, url_settings:'', url_send:'' };
    var cache = {};
    var prev = {};
    var $block;
    var $lowerMenuInitial;

    function init(options)
    {
        app.form('#userSettingForm', function () {
            var f = this;
            if (!f.checkRequired({focus: true})) return;
            f.ajax(o.url_send, {}, function (data, errors) {
                if (data && data.success) {
                    f.alertSuccess("<?=_t('users', 'Специализации сохранены')?>", {reset: true});
                    location.href = o.url_settings;
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});
        $('#userSettingSubmit').on('click', function () {
            $('#userSettingForm').submit();
        });

        $('#add-spec-ajax').on('click', function() {
            bff.ajax(bff.ajaxURL('specializations','add-spec-form'), {}, function(resp){
                if(resp) {
                    //var html = '<div>'+resp.html+'<div class="j-spec-ex hidden"></div><div class="clearfix"></div></div>';
                    //$block.find('.j-specs-block').append(html);
                    //toggleAddBlock();
                    $('#additional-specs').append(resp.html);
                }
            });
        });
        o = $.extend(o, options || {});
        $block = $(o.block);
        $lowerMenuInitial = $('.j-menu-lower').parent().html();

        var catID = 0;
        $block.on('show.bs.dropdown', '.j-spec-select', function(){
            var $el = $(this);
            var d = $el.metadata();
            prev[d.count] = $el.find('.j-menu').html();
            catID = 0;
        });

        $block.on('hide.bs.dropdown', '.j-spec-select', function(){
            var $el = $(this);
            var d = $el.metadata();
            if (prev.hasOwnProperty(d.count)) {
                $el.find('.j-menu').html(prev[d.count]);
            }
            catID = 0;
        });

        $block.on('click', '.j-back', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            if( ! catID) catID = d.id;
			var typeID = d.type_id;
            menuBlock(typeID, catID, $el.closest('.j-menu'), function($bl){
                var $cats = $bl.find('.j-cat');
                $cats.parent().removeClass('active');
                $cats.each(function(){
                    var $el = $(this);
                    var cd = $el.metadata();
                    if(cd.id == catID){
                        $el.parent().addClass('active');
                    }
                });
            });
            return false;
        });

        $('.j-serv-value').on('select2:select', function (e) {
            var $el = $(this);
            var d = $el.metadata();
            var $bl = $el.closest('.j-spec-select');

            var isTopLevel = !($(this).parent().parent().parent().parent().find('.j-spec-ex').length);
            var orderPage = $(this).parent().parent().parent().parent().parent().hasClass('order-page-flag');

            //сброс нижнего выбора
            // $el.closest('.spec').find('.dropdown.j-spec-select').last().html($lowerMenuInitial);
            //$bl.find('.j-cat-value').val(0);
            //$bl.find('.j-spec-value').val(0);
            // задача №2 - сброс дисциплины отключить

            //сброс кэша для принудительного нового запроса о дочерних категориях и
            cache = {};

            var bd = $bl.metadata();
            $el.closest('.spec').find('.j-title-empty').last().removeClass('disabled');

            //$bl.find('.j-serv-value').val(d.serv_id).change();
            var $menu = $el.closest('.j-menu');
            $menu.find('.j-serv').parent().removeClass('active');
            $el.parent().addClass('active');
            //console.log('+++ Тип работы');
            if (prev.hasOwnProperty(bd.count)) {
                delete prev['bd.count'];
            }


            $bl.removeClass('open');
            $bl.find('.j-title-empty').addClass('hide');

            $bl.find('.j-title-selected > .j-title').html(
                $el.text()
            );
            $bl.find('.j-title-selected').removeClass('hide');

            menuBlock($el.metadata().id, 0, $el.closest('.spec').find('.dropdown-menu').last());
            jOrdersOrderForm.addDueDate();
            // jOrdersOrderForm.getAvgPrice();
            jOrdersOrderForm.getDefaultCountPage(d);

            return false;
        });

		$block.on('click', '.j-cat', function(e){ nothing(e);
			var $el = $(this);

            var isTopLevel = $(this).parent().parent().parent().parent().hasClass('j-spec-main');

            if (isTopLevel) {
                menuBlock($el.metadata().type, $el.metadata().id, $el.closest('.j-spec-main .j-menu-lower'));
            }
            else {
                menuBlock($el.metadata().type, $el.metadata().id, $el.closest('.j-spec-secondary .j-menu-lower'));
            }

			return false;
		});

        $('.j-spec-select2').on('select2:select', function (e) {
            var data = e.params.data;
            var $el = $('.j-spec-select');
            var spec = data.id;
            var cat = $(this).find(":selected").data("cat");
            /*if (!intval(data.id)) {
                spec = 999;
                cat = 9;
            }*/
            $el.find('.j-spec-value').val(spec)
            $el.find('.j-cat-value').val(cat);
        });

        /*$('.j-spec-select').on('select2:select', function (e) {
            var $el = $(this);
            var d = $el.metadata();
            var $bl = $el.closest('.j-spec-select');
            var bd = $bl.metadata();

            var isTopLevel = $(this).parent().parent().parent().parent().hasClass('j-spec-main');

            var $existsSpecs = $block.find('input.j-spec-value[value="'+d.spec+'"]');
            var $existsType = $block.find('input.j-type-value[value="'+d.type+'"]');
            if ($existsSpecs.length) {
                var $existsBlock = $existsSpecs.closest('.j-spec-select');
                var $existsTypeBlock = $existsType.closest('.j-spec-select');
                if ($existsTypeBlock.length > 1)
                {
                    if (o.existsMessage) {
                        app.alert.error(o.existsMessage);
                    }
                    return;
                }
                if ($existsBlock.is($bl)) return; // tring to select the same
                if (isDisabledBlock($existsBlock)) {
                    $existsBlock.removeClass('hidden').attr('data-disabled', 0).find('.j-disabled').val(0);
                    $existsBlock.parent().find('.j-spec-ex').removeClass('hidden');
                    if (true) {//$bl.parent().hasClass('j-spec-main')) { // main spec: replace with disabled
                        $existsBlock.parent().addClass('j-spec-main');
                        $existsBlock.find('.j-delete').remove();
                        $bl.parent().replaceWith($existsBlock.parent());
                    } else {
                        onDeleteBlock($bl);
                    }
                    return;
                } else {
                    if (o.cancelIfExists) {
                        if (o.existsMessage) {
                            //app.alert.error(o.existsMessage);
                        }
                        //return;
                    }
                }
            } else {
                if (isDisabledBlock($bl)) {
                    $bl.find('.j-disabled').val(0);
                }
            }

            $el.closest('.spec').find('.j-type-value').val(d.type);
            $el.closest('.spec').find('.j-cat-value').val(d.cat);
            $el.closest('.spec').find('.j-spec-value').val(d.spec).change();

            var $menu = $el.closest('.j-menu-lower');
            $menu.find('.j-spec').parent().removeClass('active');
            $el.parent().addClass('active');
            // console.log('+++ Дисциплина');
            if (prev.hasOwnProperty(bd.count)) {
                delete prev['bd.count'];
            }


            $bl.removeClass('open');
            $bl.find('.j-title-empty').addClass('hide');
            $bl.find('.j-title-selected > .j-title').html((o.catOnSelect && d.cat_title.length ? d.cat_title + ' / ' : '') + $el.text());
            $bl.find('.j-title-selected').removeClass('hide');

            if ($existsSpecs.length > 1) {
                if(o.onExists){
                    if(o.onExists($bl, d.spec)) return;
                } else {
                    var cnt = $existsSpecs.length;
                    $existsSpecs.each(function(){
                        if(cnt <= 1) return;
                        var $del = $(this).closest('.j-spec-select').find('.j-delete');
                        if($del.length){
                            $del.trigger('click');
                            cnt--;
                        }
                    });
                }
            }

            if (o.onSelect) {
                //o.onSelect(d, $bl.parent());
                o.onSelect(d, $el.closest('.spec'));
            }
            // jOrdersOrderForm.getAvgPrice();
        });*/

        $block.on('click', '.j-delete', function(e){ nothing(e);
            var $bl = $(this).closest('.j-spec-select');
            onDeleteBlock($bl);
            return false;
        });

        $block.on('click', '.j-spec-add', function(e){ nothing(e);
            var cnt = $block.find('.j-spec-select:not(.hidden)').length;
            // dividing to 2 because splitted logic of selecting
            if (cnt /2  >= o.limit) {
                toggleAddBlock();
                return false;
            }
            bff.ajax(bff.ajaxURL('specializations','add'), {cnt: o.cnt}, function(resp){
                if(resp && resp.success) {
                    var html = '<div>'+resp.html+'<div class="j-spec-ex hidden"></div><div class="clearfix"></div></div>';
                    $block.find('.j-specs-block').append(html);
                    toggleAddBlock();
                    // todo j-spec-secondary add fadein effect
                }
            });

            o.cnt++;
            return false;
        });

        $block.on('click', '.j-secondary-title', function(e) { nothing(e);
            var cnt = $block.find('.j-spec-select:not(.hidden)').length;
            // dividing to 2 because splitted logic of selecting
            if (cnt /2  >= 2) {
                toggleAddBlock();
                $('.j-secondary-form-group').slideToggle();
                return false;
            }
            bff.ajax(bff.ajaxURL('specializations','add'), {cnt: o.cnt}, function(resp){
                if(resp && resp.success) {
                    var html = '<div>'+resp.html+'<div class="j-spec-ex hidden"></div><div class="clearfix"></div></div>';
                    $block.find('.j-specs-block').append(html);
                    toggleAddBlock();
                    $('.j-secondary-form-group').slideToggle();
                }
            });

            o.cnt++;
        });

        $block.on('click', '.j-spec-reset', function(e)
        {
            nothing(e);
            reset($block);
        });


        //дублирует функционал обработчика $block.on('click', '.j-cat'
        $block.on('click', '.j-spec-back', function(e)
        {
            nothing(e);
            var $el = $(this);

            var isTopLevel = $(this).parent().parent().parent().parent().hasClass('j-spec-main');

            if (isTopLevel) {
                menuBlock($el.metadata().type, $el.metadata().id, $el.closest('.j-spec-main .j-menu-lower'));
            }
            else {
                menuBlock($el.metadata().type, $el.metadata().id, $el.closest('.j-spec-secondary .j-menu-lower'));
            }

            return false;

        });

    }

    function isDisabledBlock($bl)
    {
        return intval($bl.data('disabled'));
    }

    function onDeleteBlock($bl)
    {
        if (isDisabledBlock($bl)) {
            $bl.addClass('hidden').find('.j-disabled').val(1);
            $bl.parent().find('.j-spec-ex').addClass('hidden');
            toggleAddBlock();
            return;
        }
        if (o.onDelete){
            o.onDelete($bl.parent());
        }

        $bl.parent().remove();
        toggleAddBlock();
    }

    function menuBlock(type, cat, $bl, onComplete)
    {
        $block.find('.j-spec-reset').removeClass('disabled');
        if (cache.hasOwnProperty(cat)) {
            $bl.html(cache[cat]);
            if (onComplete) onComplete($bl);
        } else {
            bff.ajax(bff.ajaxURL('specializations','form-menu'), {cat:cat, type:type}, function(resp){
                if (resp && resp.success) {
                    cache[cat] = resp.menu;
                    menuBlock(type, cat, $bl, onComplete);
                }
            });
        }
    }

    function reset($bl)
    {
        $bl.find('.j-spec-reset').addClass('disabled');
        $bl.find('.j-type-value').val(0);
        $bl.find('.j-cat-value').val(0);
        $bl.find('.j-spec-value').val(0);
        $bl.find('.j-serv-value').val(0);
        $bl.removeClass('open');
        $bl.find('.j-title-empty').removeClass('hide');
        $bl.find('.j-title-selected').addClass('hide');
        $bl.find('.j-menu > li.active').removeClass('active');
    }

    function toggleAddBlock()
    {
        var cnt = $block.find('.j-spec-select:not(.hidden)').length;
        if (cnt >= o.limit) {
            $block.find('.j-spec-add').addClass('hide');
        } else {
            $block.find('.j-spec-add').removeClass('hide');
        }
    }

    return {
        init:init,
        onSelect:function(f){ o.onSelect = f; },
        reset:reset
    }
}());