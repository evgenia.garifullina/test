jQuery(document).ready(function($){
    var slides = document.getElementsByClassName("my-share");
    for (var i = 0; i < slides.length; i++) {
        Ya.share2(slides.item(i), {
            content: {
                url: slides.item(i).dataset.link,
                title: slides.item(i).dataset.title,
            },
            theme: {
                services: 'vkontakte,facebook,odnoklassniki,telegram,twitter,whatsapp',
                size: 'm',
                bare: false,
                limit: 0,
                moreButtonType: 'short',
                popupDirection: 'top',
                shape: 'round'
            }
        });
    }

});
