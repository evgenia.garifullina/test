var jOrdersRespondentList = (function(){

    var inited = false, o = {lang:{}, sideStatus: [], fields: []}, listMngr;
    var $block, $form, $list, $pgn, $btnPerformerStatusChange;

    function init()
    {
        $block = $('#j-orders-respondent-list');

		$btnPerformerStatusChange = $('.j-performer-change-status');
        $form = $('#j-orders-respondent-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        $('body').on('click', '.j-f-status', function(){
            var $el = $(this);
            $form.find('[name="st"]').val($el.data('id'));
            closePopup($el);
            massActions();
            listMngr.submit({}, true);
            return false;
        });

        $list.on('click', '.j-trash', function(){
            bff.ajax(bff.ajaxURL('orders', 'offer-trash'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    listMngr.submit({popstate:true}, false);
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $('.fa-question-circle').popover();

        $(document).on('click', '#j-no-allow-edit', function(){
           app.alert.error(o.lang.no_allow_edit); 
        });
        $(document).on('click', '.j-performer-change-status', function(){
            $('.j-performer-change-status').prop('disabled', true);
			var status = intval($(this).data('status'));
			var order = intval($(this).data('order'));
			var offer = intval($(this).data('offer'));

			bff.ajax(bff.ajaxURL('orders', 'performer-status'), {status:status, order:order, offer:offer, hash: app.csrf_token}, function(resp, errors){
				if(resp && resp.success) {
					if (o.order_page) {

						location.reload();
					}
					else {
						listMngr.submit({popstate:true}, false);
					}
				} else {
					app.alert.error(errors);
				}
			});
		});

        $(document).on('click', '#j-alert-arbitrage', function(){
            app.openPopup(o.lang.arbitrage_title, o.lang.arbitrage_body);
            document.getElementById('j-arbitr-link').href = o.link;
            SendNoNotify();
        });
        
        $(document).on('click', '.j-open-history', function(){
            bff.ajax(bff.ajaxURL('Orders', 'GiveHistory'), {order_id: o.order_id}, function(data, errors) {
                if (data && data.success) {
                    SendHistory(data.historyArray);
                }
            });
            
        });

        if (window.location.hash) {
            var string =  window.location.hash.split('#');
            if(string[1] == 'history-window'){
                var history = $(document).find('.j-open-history');
                history.trigger('click');
            }
        }

        // Ошибка наверняка из за listMngr

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                var st = intval($form.find('[name="st"]').val());
                if(resp.hasOwnProperty('counts')){
                    for(var t in resp.counts){
                        if(resp.counts.hasOwnProperty(t)){
                            $form.find('.j-cnt-status-' + t ).text('(' + resp.counts[t] + ')');
                            if(st == t){
                                $form.find('.j-f-status-cnt').text('(' + resp.counts[t] + ')');
                            }
                        }
                    }
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
                $('.fa-question-circle').popover();
            },
            onPopstate: function() {
                massActions();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });
    }

    function SendNoNotify()
    {
        bff.ajax(bff.ajaxURL('Orders', 'SendNoNotifyArbitr'), {order_id: o.order_id}, function(data, errors) {
            if (data && data.success) {
                console.log("Success save no notify arbitrage");
            }
        });
    }
    function SendHistory(history)
    {
        var history_body = '';
        prevdataedit = '';
        $.each(history, function(i, msg){//Переборка истории
            $.each(msg, function(u, data){//Переборка значений истории
                switch (u){
                    case 'field':
                        $.each(o.fields, function(idx, fld){//Переборка полей
                            if (idx == data){
                                datafield = fld;
                            }
                        });
                        break;
                    case 'date_edit':
                        dataedit = data+':';
                        break;
                    case 'edited_to':
                        datato = data;
                        break;
                }
            });
            if (prevdataedit == dataedit){
                dataedit = '';
            }else{
                prevdataedit = dataedit; 
            }
            if (dataedit != '' && i != 0){
                test = '<hr>';
            }else{
                test = '';
            }
           
            to_popup =test+'<div class="row"><div class="col-sm-4 "">'+dataedit+'</div><div class="col-sm-8 tooMuchSymbols">'+datafield+' - '+ datato + '</div></div>';
            history_body = history_body + to_popup;
        });
        app.openPopup(o.lang.history_title, history_body);
    }
    function closePopup($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function massActions()
    {
        var t = intval($form.find('[name="st"]').val());
        if(o && o['types'] && o.types.hasOwnProperty(t)){
            $form.find('.j-f-status-title').text(o.types[t].t);
        }
        var $t = $('body').find('.j-f-status');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == t){
                $(this).parent().addClass('active');
            }
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());