var jSvcOrders = (function(){
    var inited = false, o = {lang:{}, balance:0};

    function init()
    {
        o.balance = o.balance - 0;
        o.mark = intval(o.mark);
        o.fix = intval(o.fix);
        o.preset = intval(o.preset);
        o.svc_marked = intval(o.svc_marked);
        o.svc_fixed  = intval(o.svc_fixed);

        var $form = $('#j-svc-orders'); if ( ! $form.length ) return;
        var f = app.form($form, function(){
            if( ! f.checkRequired() ) return;
            var $sel = $form.find('[name="svc[]"]:checked');
            if( ! $sel.length){
                return;
            }
            $sel = $form.find('[name="ps"]:checked');
            if( ! $sel.length){
                app.alert.error(o.lang.select_pay);
                return;
            }

            f.ajax('', {}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.form){
                        jBillsSelectPay.pay(resp.form);
                    }else{
                        app.alert.success(o.lang.success);
                        window.setTimeout(function(){
                            if(resp.redirect){
                                bff.redirect(resp.redirect);
                            }else{
                                location.reload();
                            }
                        }, 1000);
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });

        var $submit = $form.find('.j-submit');
        var $sum = $form.find('.j-sum');
        var $order = $('#j-order-block');
        $form.on('change', '[name="svc[]"]', function(){
            var $el = $(this);
            var id = intval($el.val());
            if($el.is(':checked')){
                $form.find('#j-svc-'+id).addClass('in');
            }else{
                $form.find('#j-svc-'+id).removeClass('in');
            }

            switch(id){
                case o.mark:
                    if(o.svc_marked) break;
                    if($el.is(':checked')){
                        $order.find('.j-order').addClass('highlited');
                    }else{
                        $order.find('.j-order').removeClass('highlited');
                    }
                    break;
                case o.fix:
                    if(o.svc_fixed) break;
                    if($el.is(':checked')){
                        $order.find('.j-fixed').removeClass('hidden');
                    }else{
                        $order.find('.j-fixed').addClass('hidden');
                    }
                    break;
            }
            var sum = 0;
            var cntSvc = 0;
            $form.find('[name="svc[]"]:checked').each(function(){
                sum += intval($(this).data('price'));
                cntSvc++;
            });
            $sum.html(sum);
            if(cntSvc > 0){
                $submit.removeClass('hidden');
            }else{
                $submit.addClass('hidden');
            }
        });

        $form.find('.j-cancel').click(function(){
            history.back();
            return false;
        });

        if(o.preset){
            var $ch = $form.find('[name="svc[]"]');
            if(o.preset & o.fix){
                $ch.filter('[value="'+ o.fix+'"]').trigger('change');
            }
            if(o.preset & o.mark){
                $ch.filter('[value="'+ o.mark+'"]').trigger('change');
            }
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
})();


