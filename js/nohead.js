﻿$(document).ready(function(){
	$('#preformIndex').attr('action', 'https://studlandia.com/');

	$(document).on('change', '#selectSpecIndex', function () {
		$(this).parents('form').find('#inputSpecCatIndex').val(this[this.selectedIndex].getAttribute('data-cat'));
	});

	$('input.phone').mask('+7 (999) 999-99-99', {autoclear: true}).on('click', function(){
		$(this).setCursorPosition(4);
	});

	AOS.init({
		once: true, duration: 900, offset: 250, disable: 'mobile'
	});

	$('.step9__list').slick({
		speed: 700,
		slidesToShow: 2,
		prevArrow: '.step9__arr.l',
		nextArrow: '.step9__arr.r',
		responsive: [
			{
				breakpoint: 1000,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	$('.step10__list').slick({
		speed: 700,
		prevArrow: '.step10__arr.l',
		nextArrow: '.step10__arr.r'
	});

	var throttleTimeout;
	$(window).bind('resize', function(){
		var $this = $(this);
		var width = $this.width();
		if (!throttleTimeout) {
			throttleTimeout = setTimeout(
				function()
				{
					if (width > 1000)
					{
						if ($('.step3__list').is('.slick-initialized'))
						{
							$('.step3__list').slick('unslick');
						}
					}
					else
					{
						if (!$('.step3__list').is('.slick-initialized'))
						{
							$('.step3__list').slick({
								speed: 700,
								slidesToShow: 2,
								prevArrow: '.step3__arr.l',
								nextArrow: '.step3__arr.r',
								responsive: [
									{
										breakpoint: 500,
										settings: {
											slidesToShow: 1
										}
									}
								]
							});
						}
					}
					throttleTimeout = null;
				},
				100
			);
		}
	}).resize();

	$('.select2').select2({
		searchInputPlaceholder: 'Начните вводить...'
	});

});

$.fn.setCursorPosition = function(pos)
{
	if ($(this).get(0).setSelectionRange)
	{
		$(this).get(0).setSelectionRange(pos, pos);
	}
	else if ($(this).get(0).createTextRange)
	{
		var range = $(this).get(0).createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
};