
// browser detector
(function () {
    var a = {
        init: function () {
            this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
            this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
            this.OS = this.searchString(this.dataOS) || "an unknown OS"
        },
        searchString: function (e) {
            for (var b = 0; b < e.length; b++) {
                var c = e[b].string;
                var d = e[b].prop;
                this.versionSearchString = e[b].versionSearch || e[b].identity;
                if (c) {
                    if (c.indexOf(e[b].subString) != -1) {
                        return e[b].identity
                    }
                } else {
                    if (d) {
                        return e[b].identity
                    }
                }
            }
        },
        searchVersion: function (c) {
            var b = c.indexOf(this.versionSearchString);
            if (b == -1) {
                return
            }
            return parseInt(c.substring(b + this.versionSearchString.length + 1))
        },
        dataBrowser: [{
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        }, {
            string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        }, {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        }, {prop: window.opera, identity: "Opera"}, {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        }, {string: navigator.vendor, subString: "KDE", identity: "Konqueror"}, {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        }, {string: navigator.vendor, subString: "Camino", identity: "Camino"}, {
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        }, {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "msie",
            versionSearch: "MSIE"
        }, {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        }, {string: navigator.userAgent, subString: "Mozilla", identity: "Netscape", versionSearch: "Mozilla"}],
        dataOS: [{string: navigator.platform, subString: "Win", identity: "Windows"}, {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        }, {string: navigator.userAgent, subString: "iPhone", identity: "iPhone/iPod"}, {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }]
    };
    a.init();
    window.$.client = {os: a.OS, browser: a.browser, ver: a.version}
})();

app =
    {
        initReady: false,
        _popups: {}, h: (window.history && window.history.pushState), csrf_token: '',
        devices: {desktop: 'desktop', tablet: 'tablet', phone: 'phone'}, $B: 0, $D: 0, $W: 0,
        isLoading: false,
        uid: function () {
            return '';
        },
        init: function (o) {
            //options
            for (var k in o) app[k] = o[k];
            app.$B = $('body');
            app.$D = $(document);
            app.$W = $(window);
            //browser
            app.$B.addClass($.client.browser.toLowerCase()).addClass($.client.browser.toLowerCase() + $.client.ver).addClass($.client.os.toLowerCase());
            //popups
            app.$D.on('touchstart click keydown', function (e) {
                if (e.type == 'keydown') {
                    if (e.keyCode == 27) app.popupsHide(false, e);
                }
                else {
                    app.popupsHide(false, e);
                }
            });
            app.$W.resize($.debounce(function () {
                app.popupsHide();
            }, 300, true));
            //device
            this.device = (function () {
                var cookieKey = app.cookiePrefix + 'device', current = o.device;
                var deviceOnResize = $.debounce(function (e) {
                    e = e || {type: 'onload'};
                    var width = window.innerWidth;
                    var width_device = ( width >= 980 ? app.devices.desktop : ( width >= 768 ? app.devices.tablet : app.devices.phone ) );
                    if (width_device != current || e.type == 'focus') {
                        bff.cookie(app.cookiePrefix + 'device', (current = width_device), {
                            expires: 200,
                            path: '/',
                            domain: '.' + app.host
                        });
                        if (e.type == 'resize') app.$W.trigger('app-device-changed', [width_device]);
                        //var img = (window.Image ? (new Image()) : document.createElement('img'));
                        //img.src = '/index.php?bff=device&type='+(current = width_device)+'&r='+Math.random();
                    }
                }, 600);
                app.$W.on('focus resize', deviceOnResize);
                deviceOnResize();
                return function (check) {
                    return ( check || false ? check === current : current );
                };
            }());
            this.csrf_token = app.$B.prev().find('meta[name="csrf_token"]').attr('content') || '';
            bff.map.setType(app.mapType);
            //user
            app.user.init({logined: app.logined});
            //select

            if(app.layout !== 'landing'){
                app.select2.init();
            }

            $(document).on('focus', 'input.js-select-subject', function () {
                if(!$(this).hasClass('ui-autocomplete-input')){
                    app.acomplete.init();
                }
            });


            //fav
            app.$D.on('click', '.j-i-fav', function (e) {
                nothing(e);
                var $el = $(this).blur(), data = $.extend({id: 0, h: 0}, $el.metadata() || {});
                app.items.fav(data.id, function (added) {
                    $el.toggleClass('active', added);
                    if (data.h) $el.toggleClass('hide', !added);
                    $el.find('.j-i-fav-icon').toggleClass('glyphicon glyphicon-star', added).toggleClass('glyphicon glyphicon-star-empty', !added);
                });
            });
            //tool-tip
            $(document).on('click', '[data-toggle="tooltip"],.show-tooltip', function () {
                $(this).tooltip('show');
                return false;
            });
            //Поповер
            // $('[data-toggle="popover"]').popover();
            $(document).on('click', '[data-toggle="popover"]', function () {
                $(this).popover('show');
                // Закрытие поповера при нажатие на любую область, кроме самого поповера.
                // Событие отслеживается, после запуска метода у popover - show.
                var popoverHide =  function (e) {
                    $('[data-toggle="popover"]').each(function () {
                        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false;
                            $('body').off('click', popoverHide);
                        }
                    });
                };
                $('body').on('click', popoverHide);
                return false;
            });

            $(document).on('click', '.confirmLink', function () {
                if (confirm($(this).attr('title'))) {
                    return true;
                }
                return false;
            });

            $(document).on('click', '#PopupModal .close', function () {
                 document.getElementById('PopupModal').remove();
            });

            $(document).on('click', '.closemodal', function () {
                 $(document).find('#PopupModal .close').trigger('click');
            });

            this.fastScrollToTop();

            this.navbarShow();

            if (app.userId && app.userActivated) {
                //app.showChangeHistoryModal();
                app.showNewMessagesPush();
            }

            app.initReady = true;
            console.log('app.init READY', app.lang);

            $(document).on("click", "#close", function() {
             $("#PopupModal").removeClass("show").addClass("fade");
            });
        },
        user: function () {
            var i = false, settKey, settData, $menu;
            return {
                init: function (o) {
                    if (i) return;
                    i = true;
                    settKey = app.cookiePrefix + 'usett';
                    settData = intval(bff.cookie(settKey) || 0);
                    if (app.logined) {
                        $menu = $('#j-header-user-menu');
                    }
                },
                logined: function () {
                    return app.logined;
                },
                counter: function (key, value) {
                    if (!key || !app.logined) return;
                    var $counter = $menu.find('.j-cnt-' + key);
                    if ($counter.length) $counter.text(value).removeClass('hide');
                },
                settings: function (key, save) {
                    if ('undefined' != typeof save) {
                        var cs = {expires: 350, domain: '.' + app.host, path: '/'};
                        if (save === true) {
                            if (!(settData & key))
                                bff.cookie(settKey, (settData |= key), cs);
                        } else {
                            bff.cookie(settKey, (settData -= key), cs);
                        }
                    } else {
                        return (settData & key);
                    }
                }
            };
        }(),
        select2: function () {
            return {
                init: function () {
                    for (let i = 0; i < $('.select2').length; i++) {
                        app.select2.include($('.select2')[i]);
                    }
                },
                include: function (el) {
                    bff.includeSrc({
                        css: ['/css/select2.min.css'],
                        js: ['/js/select2.min.js'],
                        script: [function(){
                            $(el).select2({
                                searchInputPlaceholder: 'Начните вводить...'
                            });
                            if($(el).hasClass('selectSpec') || $(el).hasClass('j-spec-select2')){
                                $(el).select2({
                                    tags: true
                                });
                            }
                        }]
                    });
                }
            };
        }(),
        acomplete: function () {
            return {
                init: function () {
                    if($('#selectSpecIndexText').length > 0){
                        bff.ajax(bff.ajaxURL('specializations','get-tags'), {}, function(resp){
                            if(resp && resp.success) {
                                var availableTags = resp.data
                                $('input#selectSpecIndexText').each(function() {
                                    app.acomplete.include(this, availableTags);
                                });
                            }
                        });
                    }
                },
                include: function (el, availableTags) {
                    $(el).autocomplete({
                        minLength: 3,
                        source: availableTags,
                        select: function( event, ui ) {
                            $(el).val( ui.item.label );
                            $(el).attr( 'data-keyword', ui.item.keyword );
                            $(el).attr( 'data-found', true );
                            $(el).attr( 'data-val', ui.item.value );
                            $(el).attr( 'data-cat', ui.item.cat );

                            if($(el).next('.j-spec-value').length > 0){
                                $(el).next('.j-spec-value').val( ui.item.value );
                                $(el).next().next('.j-cat-value').val( ui.item.cat );
                            }

                            return false;
                        }
                    })
                }
            };
        }(),

        seoInsert: function () {
            var desc = document.querySelector('meta[name="description"]'),  h1 = $('h1');
            return {
                page: function (page) {
                    if(!document.title.includes("страница")) {
                        var pageTitle = ' - страница ' + page + ' ';
                        var numPages = document.title.indexOf('|');
                        document.title = document.title.replace(document.title.substr(numPages, 1), pageTitle + '|');
                        desc.setAttribute("content", desc.content + pageTitle);
                        h1.html(h1.html() + ' страница ' + page);
                    }else{
                        document.title = app.seoInsert.insert(document.title, 'страница', 'страница ' + page, 0);
                        desc.setAttribute("content", app.seoInsert.insert(desc.content, 'страница', 'страница ' + page, 0));
                        h1.html(app.seoInsert.insert(h1.html(), 'страница', 'страница ' + page, 0));
                    }
                },
                spec: function (spec) {
                    if(!document.title.includes("науки")) {
                        var numPages = document.title.indexOf('|');
                        var indexDesc = desc.content.indexOf('работ');
                        var pageTitle = spec + ' ';
                        var pageDesc = ' ' + spec + '.';
                        document.title = document.title.replace(document.title.substr(numPages, 1), pageTitle + '|');
                        desc.setAttribute("content", desc.content.replace(desc.content.substr(indexDesc + 5, 1), pageDesc));
                        $('h1').html(spec);
                    }else {
                        document.title = app.seoInsert.insert(document.title, 'науки', spec, 1);
                        desc.setAttribute("content", app.seoInsert.insert(desc.content, 'науки.', spec + '.', 1));
                        h1.html(app.seoInsert.insert(h1.html(), 'науки', spec, 1));
                    }
                },
                specClean: function () {
                    document.title = app.seoInsert.insert(document.title, 'науки', '', 1);
                    desc.setAttribute("content", app.seoInsert.insert(desc.content, 'науки.', '', 1));
                    h1.html(app.seoInsert.insert(h1.html(), 'науки', '', 1));
                },
                insert: function (ev, field, data, type) {
                    var sData = ev.split(' ');
                    for (let i=0; i<sData.length; i++) {
                        if (sData[i] === field){
                            sData[i] = '';
                            if (type){
                                sData[i-1] = data;
                            }else{
                                sData[i+1] = data;
                            }
                        }
                    }
                    return sData.join(' ');
                },
            };
        }(),

        popup: function (id, popupSelector, linkSelector, o) {
            if (id && app._popups[id]) {
                return app._popups[id];
            }
            o = o || {};
            var $popup = $(popupSelector);
            if (!$popup.length) return false;
            var $popup_link = $(linkSelector);
            if (!$popup_link.length) {
                $popup_link = false;
                o.pos = false;
            }
            o = $.extend({bl: false, scroll: false, pos: false, top: true}, o || {});

            var visible = false, focused = false, inited = false;
            if (o.pos !== false) {
                o.pos = $.extend({left: 0, top: 0}, (o.pos === true ? {} : o.pos));
            }
            var self = {
                o: o,
                init: function () {
                    if (inited) return;
                    inited = true;
                    if (o.onInit) o.onInit.call(self, $popup);
                    $popup.on('click', 'a[rel="close"],a.cancel-link,a.close,.j-close', function (e) {
                        nothing(e);
                        self.hide();
                    });
                },
                show: function (event) {
                    self.init();
                    if (event) nothing(event);
                    if (visible) return false;
                    if (o.pos !== false) {
                        var pos = $popup_link.position();
                        $popup.css({left: o.pos.left + pos.left, top: o.pos.top + pos.top});
                    }
                    focused = false;
                    visible = true;
                    if (o.onShow) o.onShow.call(self, $popup);
                    else $popup.fadeIn(100);
                    if (o.scroll) $.scrollTo($popup, {offset: -100, duration: 500});
                    app.popupsHide(id, event); // hide popups, except this one ($popup)
                    if (o.bl) app.busylayer();
                    return true;
                },
                hide: function (callback) {
                    callback = callback || $.noop;
                    if (!visible) return false;
                    if (o.onHide) {
                        o.onHide.call(self, $popup);
                        callback();
                    }
                    else $popup.fadeOut(300, callback);
                    focused = visible = false;
                    if (o.bl) app.busylayer(false);
                    return true;
                },
                toggle: function (event) {
                    return ( visible ? self.hide() : self.show(event) );
                },
                isVisible: function () {
                    return visible;
                },
                isFocused: function () {
                    return focused;
                },
                setFocused: function (state) {
                    focused = state;
                },
                isInited: function () {
                    return inited;
                },
                getPopup: function () {
                    return $popup;
                },
                getLink: function () {
                    return $popup_link;
                },
                setOptions: function (opts) {
                    o = opts || {};
                }
            };

            $popup.on('mouseleave mouseenter touchstart', function (e) {
                focused = !( e.type == 'mouseleave' );
            });

            if ($popup_link) {
                $popup_link.on('click touchstart', function (e) {
                    self.toggle(e);
                    nothing(e);
                });
            }
            $popup.data('popup-id', id);
            return ( app._popups[id] = self );
        },
        popupForm: function (id, popupSelector, linkSelector, o) {
            o = $.extend({bl: false, scroll: false}, o || {});
            return app.popup(id, popupSelector, linkSelector, o);
        },
        popupDropdown: function (id, linkSelector, o) {
            var $link = $(linkSelector);
            if (!$link.length) return false;
            var $popup = $link.parents('.dropdown-block');
            var $list = $popup.find('ul.dropdown-list');

            function dropdownUpdated() {
                if (app.$W.width() > 480) {
                    $list.find('>li').bind({
                        mouseenter: function () {
                            $(this).find('ul').show();
                        },
                        mouseleave: function () {
                            $(this).find('ul').hide();
                        }
                    });
                } else {
                    $list.find('>li').bind('click', function () {
                        $(this).parents('ul').find('li ul').hide();
                        $(this).find('ul').toggleClass('displaynone');
                    });
                }
            }

            o = $.extend({
                bl: false, scroll: false,
                onShow: function () {
                    this.setFocused(true);
                    $popup.addClass('open-dropdown');
                },
                onHide: function () {
                    $popup.removeClass('open-dropdown');
                },
                dropdownUpdated: dropdownUpdated
            }, o || {});

            dropdownUpdated();

            return app.popup(id, $popup, $link, o);
        },
        popupsHide: function (exceptID, event) {
            $.each(this._popups, function (id, p) {
                if (exceptID !== false) {
                    if (id === exceptID) return;
                    p.hide();
                } else if (p.isVisible() && !p.isFocused() && p.getPopup().has(event.target).length === 0) {
                    p.hide();
                }
            });
        },
        busylayer: function (toggle, callback) {
            callback = callback || $.noop;
            var id = 'busyLayer', $el = $('#' + id);
            if (!$el.length) {
                app.$B.append('<div id="' + id + '" class="l-busy-layer hide"></div>');
                $el = $('#' + id);
            }
            if ($el.is(':visible') || toggle === true) {
                if (toggle === false) {
                    $el.fadeOut('fast', function () {
                        $el.hide();
                        callback();
                    });
                }
                return $el;
            }
            $el.height(app.$D.height() - 10).fadeIn('fast', callback);
            return $el;
        },
        alert: (function () {
            var timer = false, o_def = {title: false, hide: 5000},
                $block, $wrap, $title, $message, type_prev;

            $(function () {
                $block = $('#j-alert-global');
                if (!$block.length) return;
                $wrap = $block.find('.j-wrap');
                $title = $block.find('.j-title');
                $message = $block.find('.j-message');
                $block.on('click', '.close', function (e) {
                    nothing(e);
                    _hide();
                });
            });

            function _show(type, msg, o) {
                if (!$wrap) {
                    setTimeout(function(){_show(type, msg, o);}, 500);
                    return;
                }
                if (timer) clearTimeout(timer);
                do {
                    if (!msg) break;
                    if ($.isArray(msg)) {
                        if (!msg.length) break;
                        msg = msg.join(', ');
                    } // array
                    else if ($.isPlainObject(msg)) { // object
                        var res = [];
                        for (var i in msg) res.push(msg[i]);
                        msg = res.join(', ');
                    } else {
                    } // string

                    o = $.extend({}, o_def, o || {});
                    $wrap.removeClass('alert-' + type_prev).addClass('alert-' + type);
                    type_prev = type;
                    if (o.title) $title.html(o.title).removeClass('hide');
                    else $title.html('').addClass('hide');
                    $message.html(msg);
                    $block.fadeIn(300);
                    if($(window).width() < 992) {
                        $('html, body').animate({
                            scrollTop: $block.offset().top
                        }, 2000);
                    }
                    if (o.hide !== false && o.hide >= 0) {
                        // скрываем уведомление, если есть пользователь двига мышь
                        timer = setTimeout(function () {
                            _hide();
                        }, o.hide);
                    }
                    return;
                } while (false);
                _hide();
            }

            function _hide() {
                $block.fadeOut(1000);
            }

            return {
                error: function (msg, o) {
                    _show('danger', msg, o);
                },
                info: function (msg, o) {
                    _show('info', msg, o);
                },
                success: function (msg, o) {
                    _show('success', msg, o);
                },
                show: _show,
                hide: _hide,
                noHide: function () {
                    if (timer) clearTimeout(timer);
                }
            };
        }()),
        inputError: function (field, show, _focus) {
            if ($.isArray(field)) {
                $.each(field, function (i, v) {
                    app.inputError(v, show, _focus);
                });
                return false;
            }
            var $field = $(field);
            if ($field.length) {
                var $group = $field.closest('.form-group');
                if (show !== false) {
                    if (_focus !== false) $field.focus();
                    if ($group.length) $group.addClass('has-error');
                    else $field.addClass('input-error');
                } else {
                    if ($group.length) $group.removeClass('has-error');
                    else $('.input-error').removeClass('input-error');;
                }
            }
            return false;
        },
        form: function (formSelector, onSubmit, o) {
            if (!app.initReady) {
                bff_report_exception('APP NOT READY');
            }
            console.log('app.form RUN');
            o = $.extend({btn_loading: app.lang.form_btn_loading, onInit: $.noop, noEnterSubmit: false}, o || {});
            var $form, form, $btn, btnVal, inited = false, process = false, fieldFirstError = false;
            var classes = {input_error: 'input-error'};
            var self = {
                init: function () {
                    if (inited) return;
                    inited = true;
                    $form = $(formSelector);
                    $form.data('appForm', this);
                    if ($form.length) {
                        if (app.user.logined()) $form.append('<input type="hidden" name="hash" value="' + app.csrf_token + '" />');
                        form = $form.get(0);
                        $btn = $form.find('button.j-submit:last');
                        if (o.onInit) o.onInit.call(self, $form);
                        if (onSubmit && onSubmit !== false) {
                            $form.submit(function (e) {
                                nothing(e);
                                if (self.isProcessing()) return false;
                                onSubmit.call(self, $form);
                                return false;
                            });
                        }
                        // prevent enter submit
                        if (o.noEnterSubmit) {
                            $form.bind('keyup keypress', function (e) {
                                if ((e.keyCode || e.which) == 13 && !$(e.target).is('textarea')) {
                                    e.preventDefault();
                                    return false;
                                }
                            });
                        }
                    }
                },
                getForm: function () {
                    return $form;
                },
                processed: function (yes) {
                    process = yes;
                    if ($btn && $btn.length) {
                        console.log($btn);
                        if (yes) {
                            btnVal = $btn.text();
                            $btn.attr('disabled', true);
                            $btn.text(o.btn_loading);
                        } else {
                            $btn.removeAttr('disabled');
                            $btn.text(btnVal);
                        }
                    }
                },
                isProcessing: function () {
                    return process;
                },
                field: function (name) {
                    return form.elements[name];
                },
                $field: function (name) {
                    return $(form.elements[name]);
                },
                fieldStr: function (name) {
                    return ( form.elements[name] ? $.trim(form.elements[name].value) : '' );
                },
                fieldError: function (fieldName, message, opts) {
                    opts = opts || {};
                    if (!opts.hasOwnProperty('title') && !app.device(app.devices.phone)) opts.title = app.lang.form_alert_errors;
                    self.fieldsError([fieldName], [message], opts);
                },
                fieldsError: function (fieldsNames, message, opts) {
                    opts = $.extend({title: false, focus: true, scroll: false}, opts || {});
                    self.fieldsResetError();
                    var fields = [];
                    for (var i in fieldsNames) {
                        if (form.elements[fieldsNames[i]]) {
                            fields.push(form.elements[fieldsNames[i]]);
                        }
                        else {
                            var tmp = $(form).find('[name=' + fieldsNames[i] + ']:first');
                            if (!tmp.length) {
                                tmp = $(form).find('.form-el-' + fieldsNames[i] + ':first');
                            }
                            if (tmp.length) {
                                fields.push(tmp);
                            }
                        }
                    }
                    fieldFirstError = false;
                    if (fields.length > 0) { // mark fields
                        console.log('mark fields', fields);
                        fieldFirstError = fields[0];
                        app.inputError(fields, true, false);
                    }
                    if (message) {
                        $('.input-error-message').remove();
                        for (var ik in message){
                            var field = $form.find('[name="'+ik+'"]')
                            if(ik == 'serv_id'){
                                var selectField = field.next('span.select2');
                                selectField.find('.select2-selection--single').addClass('input-error');
                                selectField.after('<p class="input-error-message">' + message[ik] + '</p>');
                            }else if(ik == 'count_pages_from' || ik == 'count_pages_to'){
                                var blockField = field.parent('.s-top-form-item');
                                blockField.after('<p class="input-error-message">' + message[ik] + '</p>');
                            }else{
                                field.after('<p class="input-error-message">' + message[ik] + '</p>');
                            }
                        }
                        /*$form.find('.errorBlock').remove();
                        $form.append('<div class="errorBlock alert alert-danger"></div>');
                        var errorBlock = $form.find('.errorBlock');

                        do {
                            if (!message) break;
                            if ($.isArray(message)) {
                                if (!message.length) break;
                                message = message.join('<br/>');
                            }
                            else if ($.isPlainObject(message)) { // object
                                var res = [];
                                for (var ik in message) res.push($('<p>'+message[ik]+'</p>').text());
                                message = res.join('<br/>');
                            }
                            errorBlock.html(message);
                        } while (false);*/
                        // app.alert.error(message, {title: opts.title});
                    }
                    if (fieldFirstError) { // focus & scroll to first field
                        if (opts.scroll) $.scrollTo(fieldFirstError, {offset: -150, duration: 500});
                        if (opts.focus) $(fieldFirstError).focus();
                    }
                },
                fieldsResetError: function () {
                    app.inputError(form.elements, false);
                    $form.find('.errorBlock').remove();
                },
                checkRequired: function (opts) {
                    self.fieldsResetError();
                    var fields = [];
                    $('.j-required', ( (opts && opts['searchIn']) ? opts['searchIn'] : form)).each(function () {
                        var $this = $(this), empty;

                        if ($this.is('div')) {
                            console.log('checkRequired', $this);
                            empty = !$this.find('input,textarea,a').length;
                        }
                        else if ($this.is(':checkbox')) {
                            empty = ( !$form.find('input:checkbox[name="' + $this.attr('name') + '"]:checked').length );
                        } else if ($this.is(':radio')) {
                            empty = ( !$form.find('input:radio[name="' + $this.attr('name') + '"]:checked').length );
                        } else {
                            empty = ( $this.val() == 0 || !$.trim($this.val()).length );
                        }

                        if (empty) {
                            console.log('Error in ', $this);
                            fields.push($this.attr('name'));
                        }
                    });
                    if (fields.length > 0) {
                        //console.log('Error in ', fields);

                        var messAlert = app.lang.form_alert_required;
                        if (opts && opts['noAlert']) messAlert = null;

                        self.fieldsError(fields, messAlert, $.extend({
                            focus: false,
                            title: (app.device(app.devices.phone) ? false : app.lang.form_alert_errors)
                        }, opts || {}));
                    }
                    return ( !fields.length );
                },
                alertError: function (message, opts) {
                    app.alert.error(message, opts);
                },
                alertSuccess: function (message, o) {
                    o = $.extend({reset: false}, o || {});
                    self.fieldsResetError();
                    if (o.reset) self.reset();
                    app.alert.success(message, o);
                },
                buttonSuccess: function (btn, opts) {
                    if (!btn) btn = $btn;
                    opts = $.extend({text: false, revert: false, reset: false, speed: 250}, opts || {});
                    var button_text = btn.html(),
                        button_class = btn.attr('class'),
                        success_text = ( opts.text !== false ? opts.text : btn.next('.success-text').html() ),
                        popup = $form.parents('.box-shadow'),
                        revert = (opts.revert ? opts.revert : popup.length);

                    btn.fadeTo(opts.speed, 0, function () {
                        btn.attr({
                            class: 'success-send',
                            disabled: 'disabled'
                        }).html(success_text).fadeTo(opts.speed, 1);
                    });

                    if (revert) {
                        setTimeout(function () {
                            if (popup.length) {
                                app.popup(popup.data('popup-id')).hide();
                            }
                            btn.fadeTo(opts.speed, 0, function () {
                                btn.attr({class: button_class}).prop('disabled', false)
                                    .html(button_text).fadeTo(opts.speed, 1);
                                if (opts.reset) form.reset();
                            });
                        }, 2000)
                    } else {
                        if (opts.reset) form.reset();
                    }
                },
                reset: function () {
                    form.reset();
                },
                ajax: function (url, params, callback, $progress, opts) {
                    if (self.isProcessing()) return;

                    // эксперемент прошел удачно, но форма не отзывчивая, в топку это
                    if (0 && $form.attr('enctype') == 'multipart/form-data') {
                        bff.iframeSubmit($form, callback, {});
                        $form.submit();
                    }
                    else {
                        bff.ajax(url, $form.serialize() + '&' + $.param(params || {}), callback, function (p) {
                            self.processed(p);
                            if ($progress && $progress.length) $progress.toggle();
                        }, opts);
                    }
                }
            };
            self.init();
            return self;
        },
        list: function (formSelector, o) {
            o = $.extend({
                onInit: $.noop,
                onSubmit: $.noop,
                onPopstate: $.noop,
                onProgress: $.noop,
                onDeviceChanged: $.noop,
                onAfterDeserialize: $.noop,
                ajax: true,
                url: document.location.pathname,
                urlSearch: document.location.search,
                submitOnDeviceChanged: true
            }, o || {});
            var $form;

            $form = $(formSelector);
            if (!$form.length) {
                console.error('app.list is empty form ', formSelector);
                // bff_report_exception('app.list is empty form');
                return;
            }
            if (o.ajax) {
                var queryInitial = getQuery();
                app.$W.on('popstate', function (e) {
                    console.log(document.location ,  JSON.stringify(e.state));
                    var loc = history.location || document.location;
                    if (o.url == loc.pathname && o.urlSearch == loc.search) {
                        return;
                    }

                    var query = loc.search.substr(1);
                    if (query.length == 0) query = queryInitial;

                    console.log('EV popstate', formSelector, query, e);

                    o.onPopstate.call(this, $form, query);
                    $form.deserialize(query, true);
                    o.onAfterDeserialize($form, query);
                    o.url = loc.pathname;
                    o.urlSearch = loc.search;
                    onSubmit({popstate: true});
                });
            }

            app.$W.on('app-device-changed', function (e, device) {
                if (!o.submitOnDeviceChanged) return;
                onSubmit({fade: false, deviceChanged: true, device: device});
            });

            o.onInit.call(this, $form);

            function onSubmit(ex, resetPage) {
                ex = $.extend({popstate: false, scroll: false, fade: true, url: false}, ex || {});
                if (resetPage) onPage(1, false);
                var query = getQuery();
                var url = ex.url ? ex.url : o.url;
                if (o.ajax) {
                    bff.ajax(url, query + '&hash=' + app.csrf_token + '&device=' + app.device(), function (response, errors) {
                        if (response && response.success) {
                            o.onSubmit(response, ex);
                            if (!ex.popstate) history.pushState(null, null, url + '?' + query);
                        } else {
                            app.alert.error(errors);
                            bff_report_exception('POPSTATE url = ' + url + ' , query = ' + query + '&hash=' + app.csrf_token + '&device=' + app.device() + ' ; formSelector = ' + $form.selector);
                        }
                    }, function (p) {
                        o.onProgress(p, ex);
                    });
                } else {
                    bff.redirect(url + '?' + query);
                }
            }

            function onPage(pageId, update) {
                pageId = intval(pageId);
                if (pageId <= 0) pageId = 0;
                var $val = $form.find('[name="page"]:first');
                if (!$val.length) return;
                if (pageId && intval($val.val()) != pageId) {
                    $val.val(pageId);
                    if (update !== false) onSubmit({scroll: true});
                }
                return $val.val();
            }

            function getQuery() {
                var query = [];
                $.each($form.serializeArray(), function (i, field) {
                    if (field.value && field.value != 0 && field.value != '')
                        query.push(field.name + '=' + encodeURIComponent(field.value));
                });
                $.each($('.j-top-search-form').serializeArray(), function (i, field) {
                    if (field.value && field.value != 0 && field.value != '')
                        query.push(field.name + '=' + encodeURIComponent(field.value));
                });
                return query.join('&');
            }

            return {
                submit: onSubmit,
                page: onPage,
                getForm: function () {
                    return $form;
                },
                getQuery: getQuery,
                getURL: function () {
                    return o.url;
                }
            };
        },
        showMapPopup: function ($popup, $popupLink, $coords, mapBlockID) {
            var mapInited = false, map;
            var popup = app.popupForm(mapBlockID, $popup, $popupLink, {
                onShow: function ($p) {
                    $p.fadeIn(300, function () {
                        if (!mapInited) {
                            ymaps.ready(function () {
                                map = new ymaps.Map(mapBlockID, {
                                    zoom: 13,
                                    center: $coords.data('coords').split(','),
                                    behaviors: ['default', 'scrollZoom']
                                });
                                map.controls.add('zoomControl', {top: 5, left: 5});
                                var marker = new ymaps.Placemark(map.getCenter());
                                map.geoObjects.add(marker);
                                map.container.fitToViewport();
                            });
                            mapInited = true;
                        } else {
                            map.container.fitToViewport();
                        }
                    });
                }
            });

            return {};
        },
        tabs: function (className) {
            $(function () {
                var $ul = $('#' + className);
                var $tabs = $('.' + className);
                var $title = $ul.parent().find('.j-title');
                if (bff.h) {
                    app.$W.on('popstate', function (e) {
                        var loc = history.location || document.location;
                        var query = loc.search.substr(1);
                        $bFst = false;
                        if (query.length == 0) {
                            $bFst = true;
                        } else {
                            var i = query.indexOf('tab=');
                            if (i > -1) {
                                var amp = query.indexOf('&', i);
                                var tab;
                                if (amp > i + 4) {
                                    tab = query.substr(i + 4, amp - i - 4);
                                } else {
                                    tab = query.substr(i + 4);
                                }
                                var $t = $ul.find('li.j-tab-' + tab);
                                $t.addClass('active').siblings().removeClass('active');
                                $title.html($t.find('a').html());
                                $tabs.addClass('displaynone');
                                $tabs.filter('.' + className + '-' + tab).removeClass('displaynone');
                            } else {
                                $bFst = true;
                            }
                        }
                        if ($bFst) {
                            var $fst = $ul.find('li:first');
                            $fst.addClass('active').siblings().removeClass('active');
                            $title.html($fst.find('a').html());
                            $tabs.addClass('displaynone');
                            $tabs.filter(':first').removeClass('displaynone');
                        }
                    });
                }

                $ul.on('click', 'li > a', function () {
                    var $el = $(this);
                    console.log('!', $el.attr('href'));
                    if ($el.attr('href') != '#') {
                        location.href = $el.attr('href');
                        return false;
                    }
                    var active = $el.data('a');
                    $title.html($el.html());
                    $el.parent().addClass('active').siblings().removeClass('active');
                    $tabs.addClass('displaynone');
                    $tabs.filter('.' + className + '-' + active).removeClass('displaynone');
                    if (bff.h) {
                        var loc = document.location.href;
                        var i = loc.indexOf('tab=');
                        if (i > 0) {
                            loc = loc.replace(/tab=([^&$]+)/, 'tab=' + active);
                        } else {
                            if (loc.indexOf('?') > -1) {
                                loc += '&tab=';
                            } else {
                                loc += '?tab=';
                            }
                            loc += active;
                        }
                        window.history.pushState({}, document.title, loc);
                    }
                    return false;
                });
            });
        },
        map: function (container, center, callback, o) {
            return bff.map.init(container, center, callback, o);
        },
        push_confirm: function (status) {
            return bff.ajax('/user/settings', {
                hash: app.csrf_token,
                act: 'push_confirm',
                status: status
            }, function (data, errors) {
                console.log(' * AJAX push_confirm ', data, errors);
                if (data && data.success) {
                    if (data['reload']) {
                        location.reload(true);
                    }
                    else if (status) {
                        app.alert.success('Вы успешно подписаны на Push!');
                    } else {
                        app.alert.success('Push сообщения отключены!');
                    }
                    // if ('/user/settings?tab=enotify' == location.pathname + location.search) {
                    //     app.push_form(status);
                    // }
                } else if (errors) {
                    app.alert.error(errors);
                }
            });
        },
        reportClick: function(obj) {
            var mess = $(obj).parent().prev().text();
            mess = location.href + "\n" + mess;
            $(obj).attr('href', $(obj).attr('href') + '?message=' + encodeURIComponent(mess));
            return true;
        },

        devInfo: function (devRate) {
            $('body').find('.j-dev-rate-detail').append(devRate);
        },

        /*runGoal: function (tag) {
            if (typeof tag !== 'undefined') {
                ym(34831085,'reachGoal', tag);
            }
        },*/
        openPopup: function (title, modalbody) {
            var buttonclose = ('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
            var Modal =  ('<div class="modal modelCenter fade" tabindex="-1" role="dialog" id="PopupModal"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header">'+ buttonclose + title +'</div><div class="modal-body"><p>'+ modalbody +'</p></div></div></div></div>');
            $('body').append(Modal);
            $('#PopupModal').modal('show')
        },
        openPopupSettingSpec: function () {
            if (app.isLoading) return;
            app.isLoading = 'openPopupSettingSpec';
            // TODO wait gif
            if (!$('#userSettingSpecPopUp').length) {
                if (typeof jUserSettingSpec === 'undefined') {
                    bff.ajax('/popupUserSettingSpec', [], function (data, errors) {
                        if (data && data.success) {
                            $('body').append(data.html);
                            $('#userSettingSpecPopUp').modal('show');
                        } else if (errors) {
                            app.alert.error(errors);
                        }
                        app.isLoading = false;
                    }, function (p) {
                        // self.processed(p);
                        // if ($progress && $progress.length) $progress.toggle();
                    }, {isPost: false});
                } else {
                    app.isLoading = false;
                }
            } else {
                $('#userSettingSpecPopUp').modal('show');
                app.isLoading = false;
            }
            return false;
        },
        hideBells: function (bellsID) {
            bff.cookie('bells', bellsID, {expires: 180, path: '/', domain: '.' + app.host});
            $('.bar-alert').removeClass('open');
        },
        hideBellsCount: function (bellsID) {
            bff.cookie('bellsCount', bellsID, {expires: 180, path: '/', domain: '.' + app.host});
            location.reload();
        },

        openPopupPayLib: function () {
            var popup = $('#payLibModal');
            popup.modal('show');
            popup.find('button[type=submit]').on('click', function() {
                bff.ajax(popup.find('form').attr('action'), {'action': 'pay', 'hash' : app.csrf_token},
                    function (data, errors) {
                        if (data && data.success) {
                            popup.find('.modal-footer').html(data.message);
                            window.open(data.download, '_blank');
                        } else if (data && data.reload) {
                            location.reload();
                        } else if (errors) {
                            app.alert.error(errors);
                        }
                    }, function (p) {
                        // self.processed(p);
                        // if ($progress && $progress.length) $progress.toggle();
                    }, {isPost: true});
                return false;
            });
        },

        payLib: function () {
            bff.ajax($('#pay-lib-form').attr('action'), {'action': 'pay-steps', 'hash' : app.csrf_token},
                function (data, errors) {
                    if (data && data.success) {
                        bff.redirect(data.redirect);
                    } else if (errors) {
                        app.alert.error(errors);
                    }
                }, function (p) {
                    // self.processed(p);
                    // if ($progress && $progress.length) $progress.toggle();
                }, {isPost: true});
            return false;
        },

        openPopupSettingPhone: function (successCall) {
            if (app.isLoading) return;
            if (successCall === 'reload') successCall = function() {location.reload();};
            if (successCall === 'whatsapp') successCall = function() {window.location.replace("/user/whatsapp");};
            app.isLoading = 'openPopupSettingPhone';
            // TODO wait gif
            if (!$('#phoneConfirmModal').length) {
                bff.ajax('/popupUserSettingPhone', {}, function (data, errors) {
                    if (data && data.success) {
                        $('body').append(data.html);
                        $('#phoneConfirmModal').modal('show');
                        if (successCall) {
                            jUserPhoneCode.successSet(successCall);
                        }
                    } else if (errors) {
                        app.alert.error(errors);
                    }
                    app.isLoading = false;
                }, function (p) {
                    // self.processed(p);
                    // if ($progress && $progress.length) $progress.toggle();
                }, {isPost: false});
            } else {
                $('#phoneConfirmModal').modal('show');
                app.isLoading = false;
                if (successCall) {
                    jUserPhoneCode.successSet(successCall);
                }
            }
            return false;
        },
        openPopupPhoneCode: function (successCall, phone) {

        },
        openPopupStatusActivate: function (id, successCall) {
            if (app.isLoading) return;
            app.isLoading = 'openPopupStatusActivate';
            // TODO wait gif
            if (!$('#modalStatusActivate').length) {

                var url = window.location.href;
                var profile_isset = 0;

                if(url.indexOf('?profile_isset=1') != -1)
                    profile_isset = 1;
                else if(url.indexOf('&profile_isset=1') != -1)
                    profile_isset = 1;

                bff.ajax(bff.ajaxURL('orders', 'status') + '&id='+id + '&profile_isset='+profile_isset, [], function (data, errors) {
                    if (data && data.success) {
                        $('body').append(data.html);
                        if (typeof data.script[3] === "undefined")  data.script[3] = [];
                        $('#modalStatusActivate').modal({backdrop: 'static', keyboard: false});
                        if (successCall) {
                            successCall.call();
                        }
                    } else if (errors) {
                        app.alert.error(errors);
                    }
                    app.isLoading = false;
                }, function (p) {
                    // self.processed(p);
                    // if ($progress && $progress.length) $progress.toggle();
                }, {isPost: false});
            } else {
                $('#modalStatusActivate').modal('show');
                app.isLoading = false;
                if (successCall) {
                    successCall.call();
                }
            }
            return false;
        },
        openPopupStatusActivateMail: function (id, successCall) {
            app.openPopupStatusActivate(id, successCall);
            if (!$('#modalStatusActivate').length){
                setTimeout(function(){
                    $('#j-mail-link').click();
                    var link = $('#j-mail-link').attr('href');
                    window.open(link, '_blank');
                    $('#modalStatusActivate').modal('hide');
                }, 700);
            }else{
                window.open($('#j-mail-link').attr('href'), '_blank');
                $('#modalStatusActivate').modal('hide');
            }

            return false;
        },
        hideNotifyBell: function (vl, cName) {
            $(vl).unwrap().remove();
            var notifyCount = $('.bar-alert i.j-cnt-msg').html();
            if(notifyCount == 1){
                $('.bar-alert i.j-cnt-msg').remove();
            }else{
                $('.bar-alert i.j-cnt-msg').html(notifyCount - 1);
            }
            bff.cookie(cName, 1, {expires: 180, path: '/', domain: '.' + app.host});
        },
        openPopupEmailReConfirm: function (id, successCall) {
            if (app.isLoading) return;
            app.isLoading = 'openPopupEmailReConfirm';
            // TODO wait gif
            if (!$('#modalEmailReConfirm').length) {
                $(document).on('click', '#emailReConfirm-submit', function(){

                    bff.ajax(bff.ajaxURL('users', 'emailReConfirm'), {'email':$('#emailReConfirm-value').val()}, function (data, errors) {
                        if (data && data.success) {
                            location.reload();
                        } else if (errors) {
                            app.alert.error(errors);
                        }
                    }, function (p) {
                        // self.processed(p);
                        // if ($progress && $progress.length) $progress.toggle();
                    }, {isPost: true});
                });
                bff.ajax(bff.ajaxURL('users', 'emailReConfirm'), [], function (data, errors) {
                    console.log('!!!!', data);
                    if (data && data.success) {
                        $('body').append(data.html);
                        if (typeof data.script[3] === "undefined")  data.script[3] = [];
                        $('#modalEmailReConfirm').modal({backdrop: 'static', keyboard: false});
                        if (successCall) {
                            successCall.call();
                        }
                    } else if (errors) {
                        app.alert.error(errors);
                    }
                    app.isLoading = false;
                }, function (p) {
                    // self.processed(p);
                    // if ($progress && $progress.length) $progress.toggle();
                }, {isPost: false});
            } else {
                $('#modalEmailReConfirm').modal('show');
                app.isLoading = false;
                if (successCall) {
                    successCall.call();
                }
            }
            return false;
        },
        retrySendMail: function () {
            if (app.isLoading) return;
            app.isLoading = 'retrySendMail';
            var a = $(this).html('письмо отправляется...');
            bff.ajax(bff.ajaxURL('orders', 'retrySendMail'), {}, function (data, errors) {
                if (data && data.success) {
                    app.alert.success('Письмо было успешно отправлено');
                    a.parent().fadeOut();
                } else {
                    app.alert.error(errors);
                    a.html('Ошибка');
                }
                app.isLoading = false;
            });

        },
        newSendMail: function () {
            var a = $(this);
            var p = a.parent();
            if (!p.hasClass('isActive')) {
                p.addClass('isActive');
                a.html('Отправить');
            } else {
                if (app.isLoading) return;
                var newEmail = p.find('input').val();
                app.isLoading = 'newSendMail';
                a.html('письмо отправляется...');
                bff.ajax(bff.ajaxURL('orders', 'retrySendMail'), {new_email: newEmail}, function (data, errors) {
                    if (data && data.success) {
                        app.alert.success('Письмо было успешно отправлено');
                        p.fadeOut();
                        $('.newSendMailSpan').html(newEmail);
                    } else {
                        app.alert.error(errors);
                        a.html('Отправить');
                    }
                    app.isLoading = false;
                });
            }

        },

//        showChangeHistoryModal: function() {
//            bff.ajax(bff.ajaxURL('Orders', 'getHistoryModal'), {}, function(data, errors) {
//                if (data && data.success) {
//                    if (data.result.length > 0){
//                        $.each(data.result, function(i, msg){
//                            $.each(msg, function(idx, val){
//                                if (idx == 'order_id'){
//                                    order_id = val;
//                                    history_link = '<a class="push-message-body" onclick="app.OpenHistory('+val+');">историей изменения заказа</div>';
//                                }
//                            });
//                        });
//                    var HistoryBody =  'Инфрмируем вас о том, что в заказе #'+ order_id +' к которому вы оставили заявку произошли важные изменения , прделагаем ознакомится с '+ history_link;
//                    var HistoryTitle = 'Изменение заказа';
//                    app.openPopup(HistoryTitle,HistoryBody);
//                    }
//                }
//            });
//        },

        OpenHistory: function(order_id) {
            bff.ajax(bff.ajaxURL('Orders', 'getHistoryLink'), {order_id: order_id}, function(data, errors) {
                if (data && data.success) {
                    if(window.location.href == data.dialogUrl) {
                        window.location.reload();
                    }
                    else {
                        window.location = data.dialogUrl;
                    }
                }
            });
        },

        showNewMessagesPush: function() {

            if($('#pushMessagesBox').length) {
                return;
            }

            var pushMessagesBox = $('<div id="notify-wrapper"><div id="pushMessagesBox"></div><div class="hide-all" onclick="app.delAllMessagesFromPush()">Скрыть всё</div></div>');

            bff.ajax(bff.ajaxURL('InternalMail', 'getPushMessages'), {}, function(data, errors) {
                if (data && data.success) {
                    if(data.new_messages.length > 0) {
                        $.each(data.new_messages, function(i, msg){
                            var msgHtml = app.getPushMessageHtml(msg);
                            msgHtml.addClass('delay-show');
                            pushMessagesBox.find('#pushMessagesBox').append(msgHtml);
                        });

                        setTimeout(function(){
                            $('#notify-wrapper .hide-all').addClass('show');
                        }, 1500);
                    }
                }
            });

            $('body').append(pushMessagesBox);

            setTimeout(function() {
                $('#pushMessagesBox .push-message-body').each(function(i, el) {
                    $(el).addClass('show');
                });

            },1000)

        },
        addNewMessageInPush: function(data) {

            if(!$('#pushMessagesBox').length) {
                app.showNewMessagesPush();
                return;
            }

            var msg = app.getPushMessageHtml(data);
            var first_message = $('#pushMessagesBox .push-message-body:last-child');
            var delFirstElement = false;

            if($('#pushMessagesBox .push-message-body').length > 2)
                delFirstElement = true;

            $('#pushMessagesBox').prepend(msg);


            if(delFirstElement)
                first_message.removeClass('show');

            setTimeout(function() {

                if(delFirstElement) {
                    first_message.slideUp(200, function(){
                        $(this).remove()
                    });
                }

                $('#message-' + data.id).addClass('show');
            }, 500);

            $('#notify-wrapper .hide-all').addClass('show');
        },
        closeMsg: function(msg_id) {

            bff.ajax(bff.ajaxURL('InternalMail', 'setMessageHide'), {message_id:msg_id}, function(data, errors) {
                if (data && data.success) {
                    var msg_push_elem = $('#message-' + data.message_id);
                    msg_push_elem.removeClass('show');

                    if($('#pushMessagesBox .push-message-body').length == 1)
                        $('#notify-wrapper .hide-all').removeClass('show');

                    var wait_animation_time = 500;
                    if($('#pushMessagesBox .push-message-body').index(msg_push_elem) == 0) {
                        setTimeout(function() {
                            msg_push_elem.slideUp(200);
                        }, wait_animation_time);

                        wait_animation_time = 700;
                    }

                    bff.ajax(bff.ajaxURL('InternalMail', 'getPushMessages'), {}, function(data, errors) {
                        if (data && data.success) {
                            if(data.new_messages.length > 0) {
                                var find_new_msg = false;
                                $.each(data.new_messages, function(i, msg){
                                    if($('#message-' + msg.id).length == 0) {
                                        find_new_msg = true;
                                        setTimeout(function() {
                                            var msgHtml = app.getPushMessageHtml(msg);
                                            msg_push_elem.remove();

                                            if($('#pushMessagesBox .push-message-body').index(msg_push_elem) == 0) {
                                                $('#pushMessagesBox').prepend(msgHtml);
                                            }
                                            else {
                                                $('#pushMessagesBox').append(msgHtml);
                                            }

                                            setTimeout(function() {
                                                $('#message-' + msg.id).addClass('show');
                                            }, 100);

                                        }, wait_animation_time);
                                    }
                                });

                                if(!find_new_msg) {
                                    setTimeout(function() {
                                        msg_push_elem.remove();
                                    }, wait_animation_time);
                                }

                            }
                            else {
                                setTimeout(function() {
                                    msg_push_elem.remove();
                                }, wait_animation_time);
                            }
                        }
                    });

                }
            });
        },
        delAllMessagesFromPush: function() {
            bff.ajax(bff.ajaxURL('InternalMail', 'setAllMessagesHide'), {}, function(data, errors) {
                if (data && data.success) {
                    var wait_time = 100;
                    $('#pushMessagesBox .push-message-body').each(function(i, el){
                        $(el).removeClass('delay-show');

                        setTimeout(function(){
                            $(el).removeClass('show');
                            setTimeout(function(){$(el).remove();}, 600);
                        },wait_time);

                        wait_time+=200;
                    });

                    $('#notify-wrapper .hide-all').removeClass('show');
                }
            });
        },
        OpenDialog: function(order_id, msg_id) {
            bff.ajax(bff.ajaxURL('InternalMail', 'getDialogLink'), {order_id: order_id, message_id: msg_id}, function(data, errors) {
                if (data && data.success) {
                    if(window.location.href == data.dialogUrl) {
                        window.location.reload();
                    }
                    else {
                        window.location = data.dialogUrl;
                    }
                }
            });
        },
        getPushMessageHtml: function(msg) {
            if(msg.order_id == 0){
                msg.order_id = "Администрация";
            }
            var new_msg = $('<div class="push-message-body" onclick="app.OpenDialog('+msg.order_id+', '+msg.id+');"></div>');
            new_msg.attr('id','message-' + msg.id);
            new_msg.append($('<div class="msg-close" onclick="app.closeMsg('+msg.id+')"><i class="fa fa-times" aria-hidden="true"></i></div>'));
            new_msg.append($('<div class="msg-body"><p class="msg-body__inner">' + msg.message + '</p></div>'));
            return new_msg;
        },

        fastScrollToTop: function() {
            var $csrollTop = $('#j-scrolltop');
            if ($csrollTop.length) {
                $csrollTop.hide();
                app.$W.scroll(function () {
                    if (app.device() != app.devices.desktop) return;
                    if ($(this).scrollTop() > 1000) {
                        $csrollTop.fadeIn();
                    } else {
                        $csrollTop.fadeOut();
                    }
                });
                $csrollTop.click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 400);
                    return false;
                });
            }
        },

        navbarShow: function() {

            // Затемнение при нажатии на меню
            var scroll;
            var last_scroll;

            $( document ).scroll(function() {
                scroll = $( document ).scrollTop();
            });

            $( ".navbar-toggle" ).click(function() {
                if($(".site-overlay").is(":hidden")) {
                    $(".site-overlay").show();
                    $("body").css("position","fixed");
                    $("body").css("top", -scroll);
                    $(".navmenu").animate({ width: 'show' },300);
                    $(".navmenu").css("left","0%");
                    $("body").animate({
                        left: "60%",
                    }, 300 );

                    last_scroll = scroll;

                }else{
                    $(".site-overlay").hide();
                    $("body").css("position","relative");
                    $("body").css("top", 0);
                    $(".navmenu").animate({ width: 'hide' },300);
                    $("body").animate({
                        left: "0%",
                    }, 300 , function() {
                        $( ".site-overlay").hide();
                    });
                    $( document ).scrollTop(last_scroll);

                }

            });

            $(document).click(function(e){
                if($(e.target).closest('nav.navmenu').length == 0 && $(e.target).closest('.navbar-toggle').length == 0) {
                    if($('nav.navmenu').hasClass('visible')){
                        $( ".navbar-toggle" ).trigger('click');
                    }
                }
            });

            $( ".site-overlay" ).click(function() {
                $("body").css("position","relative");
                $("body").css("top", 0);
                $(".navmenu").animate({ width: 'hide' },300);

                $("body").animate({
                    left: "0%",
                }, 300 , function() {
                    $( ".site-overlay").hide();
                });

                $( document ).scrollTop(last_scroll);
            });

        }
    };

function unloadAlert() {

    var message = "Вы уверены, что хотите покинуть страницу?";

    var unload = function(e)
    {
        (e || window.event).returnValue = message;
        console.log(message);
        return message;
    };

    // this.resetUnload = function()
    // {
    //     $(window).off('beforeunload', o.unload);
    //
    //     setTimeout(function(){
    //         $(window).on('beforeunload', o.unload);
    //     }, 2000);
    // };
    //
    // this.initAuto = function()
    // {
    //     $(window).on('beforeunload', o.unload);
    //
    //     $('a').on('click', function(){o.resetUnload});
    //     $(document).on('submit', 'form', function(){o.resetUnload});
    //     $(document).on('keydown', function(event){
    //         if((event.ctrlKey && event.keyCode == 116) || event.keyCode == 116){
    //             o.resetUnload;
    //         }
    //     });
    // };

    return {
        on: function(mess) {
            message = mess;
            $(window).on('beforeunload', unload);
        },
        off: function() {
            $(window).off('beforeunload');
        }
    };
}
