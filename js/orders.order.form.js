var jOrdersOrderForm = (function() {
    var inited = false, o = {
        lang: {fadeAjaxText1: '', fadeAjaxText2: ''},
        itemID: 0,
        imgData: {},
        limitInvite: 10,
        allowChangeInvite: false
    };
    var geo = {
        $block: 0, $regionBlocks: 0, $cityBlocks: 0, country: 0, cityAC: 0, cityID: 0, cityPreSuggest: {},
        addr: {$block: 0, map: 0, mapEditor: 0, lastQuery: '', inited: 0}
    };
    var img = {url: '', $block: 0, type: {}, $togglers: 0, uploader: 0, active: {}, classes: {progress: 'j-progress'}};
    var dp = {$block: 0, cache: {}, child: {}};
    var price = {cache: {}, $block: 0, spec_id: 0};
    var tFade;
    var f, $f;
    var $checkedInvite = {};

    function init() {
        o.typeID = intval(o.typeID);
        //submit
        $f = $('#j-order-form');
        if(o.issetServ){
            var $el = $('.j-serv-value');
            var d = $el.metadata();
            jOrdersOrderForm.addDueDate();
            jOrdersOrderForm.getDefaultCountPage(d);
        }
        f = app.form($f, false, {noEnterSubmit: true});
        bff.iframeSubmit($f, function (data, errors) {
            clearTimeout(tFade);
            if (data && data['emailError']) {
                bff.fadeAjax('<p>' + errors['email'] + '</p>');
                bff.fadeAjaxSetActionClose('Внимание');
                if (data['fields']) f.fieldsError(data.fields);
                return;
            }
            bff.fadeAjaxHide();
            console.log('RESULT AJAX ', data);
            if (data && data['success']) {
                if (data['redirect']) {
                    var rdr = $('<div/>').html(data.redirect).text();
                    console.log('REDIRECT TO ', rdr);
                    if ($f.attr('data-tag')) {
                        runGoal($f.attr('data-tag'));
                        bff.redirect(rdr);
                    } else {
                        bff.redirect(rdr);
                    }

                } else {
                    console.log('SUCCESS MES');
                    f.alertSuccess(o.lang.saved_success);
                }
            } else if (data['fields']) {
                console.log('ALERT  MESS');
                f.fieldsError(data.fields, errors);
            }
        }, {
            beforeSubmit: function () {
                var specInput = $f.find('#selectSpecIndexText');
                if(specInput.val() != '' && specInput.attr('data-found') == 'false'){
                    $f.find('.j-spec-value').val($f.find('#selectSpecIndexText').val());
                }
                if (!f.checkRequired({focus: true})) return false;

                var $bl = $f.find('.j-specs:visible');

                if ($bl.length) {
                    if (!$bl.find('.j-spec-value').val().length) {
                        app.alert.error(o.lang.spec_wrong);
                        return false;
                    }
                }

                // обязательное поле "Тип работы" [2]
                if ($bl.length) {
                    if (!intval($bl.find('.j-serv-value').val())) {
                        app.alert.error(o.lang.service_wrong);
                        return false;
                    }
                }

                $bl = $f.find('.j-cats:visible');
                if ($bl.length) {
                    if (!intval($bl.find('.j-cat-value').val())) {
                        app.alert.error(o.lang.cat_wrong);
                        return false;
                    }
                }
                $bl = $f.find('.j-agreement');
                if ($bl.length) {
                    if (!$bl.is(':checked')) {
                        app.alert.error(o.lang.agreement);
                        return false;
                    }
                }
                tFade = setTimeout(function () {
                    bff.fadeAjax(o.lang.fadeAjaxText1);
                    tFade = setTimeout(function () {
                        bff.fadeAjax(o.lang.fadeAjaxText2);
                    }, 10000);
                }, 500);

                return true;
            },
            button: '.j-submit'
        });
        $f.on('click', '.j-submit', function () {
            $f.submit();
            return false;
        });
        // $f.on('change', '.j-spec-value', specValueChange);
        // $f.on('change', '.j-serv-value', addDueDate);
        // $f.on('change', '.j-serv-value', getDefaultCountPage);
        if ($('#avgPriceLabel').length) {
            $f.on('change', '.check-avg,.j-serv-value,.j-spec-value', getAvgPrice);
        }
        $f.on('change', '#count_pages_to', function () {
            var count_pages_from = $('#count_pages_from').val();
            var count_pages_to = $('#count_pages_to');
            if (parseInt(count_pages_from) > parseInt(count_pages_to.val())) {
                count_pages_to.val(count_pages_from);
            }
            var num = $(this).val();
            var numNoZero = num.replace(/^0+/, '');
            $(this).val(numNoZero)
        });
        $f.on('change', '#count_pages_from', function () {
            $('#count_pages_to').val('');
        });
        $f.on('keyup', '#count_pages_from, #count_pages_to', function () {
            var num = $(this).val();
            var numNoZero = num.replace(/^0+/, '');
            $(this).val(numNoZero)
        });

        $f.on('change', '#due_date', dateOfDelivery);
        $f.on('change', '#origin_checker', originChecker);
        // cancel
        $f.on('click', '.j-cancel', function () {
            history.back();
            return false;
        });
        $f.on('change', '#j-promocode', checkPromocode);

        var $typeBlocks = $f.find('.j-type');
        f.$field('type').change(function () {
            var t = $(this).val();
            $typeBlocks.addClass('hidden');
            $typeBlocks.filter('.j-type-' + t).removeClass('hidden');
            setPrice(t);
            setDp();
        });

        if (typeof (jSpecsSelect) == 'object') {
            jSpecsSelect.init({block: $f.find('.j-specs'), cnt: 1, limit: 1});
            jSpecsSelect.onSelect(function () {
                setPrice();
                setDp();
            });
        }
        if (typeof (jCatsSelect) == 'object') {
            jCatsSelect.init({block: $f.find('.j-cats'), cnt: 1, limit: 1});
            jCatsSelect.onSelect(function () {
                setDp();
            });
        }
        //
        // // geo
        // geo.fn = (function(){
        //     geo.$block = $f;
        //     geo.$regionBlocks = geo.$block.find('.j-region');
        //     geo.$cityBlocks = geo.$block.find('.j-city');
        //     geo.country = o.defCountry;
        //
        //     var $country = geo.$block.find('#j-order-country');
        //     if($country.length){
        //         $country.change(function(){
        //             //country
        //             geo.country = intval($country.val());
        //             //o.reg1_country = geo.country;
        //             o.geoCountry = $country.find(':selected').text();
        //             if(geo.country){
        //                 geo.cityAC.setParam('country', geo.country);
        //                 geo.$cityBlocks.addClass('hidden');
        //                 if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
        //                     geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
        //                 }else{
        //                     bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
        //                         geo.cityAC.setSuggest[geo.country] = data;
        //                         geo.cityAC.setSuggest(data, true);
        //                     });
        //                 }
        //                 geoMapSearch();
        //                 geo.$regionBlocks.removeClass('displaynone');
        //             }else{
        //                 geo.$regionBlocks.addClass('displaynone');
        //             }
        //             return true;
        //         });
        //         geo.country = intval($country.val());
        //     }
        //
        //     //city
        //     geo.$block.find('#j-order-city-select').autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
        //         valueInput: geo.$block.find('#j-order-city-value'), params:{reg:0, country: geo.country},
        //         suggest: app.regionPreSuggest,
        //         onSelect: function(cityID, cityTitle, ex){
        //             geo.fn.onCity(cityID, ex);
        //         },
        //         doPrepareText: function(html){
        //             var regionTitlePos = html.toLowerCase().indexOf('<br');
        //             if( regionTitlePos != -1 ) {
        //                 html = html.substr(0, regionTitlePos);
        //             }
        //             html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
        //             return $.trim(html);
        //         }
        //     }, function(){ geo.cityAC = this; });
        //     if( ! geo.country){
        //         geo.$regionBlocks.addClass('displaynone');
        //         geo.$cityBlocks.addClass('hidden');
        //     }
        //     //addr
        //     geo.addr.$addr = geo.$block.find('#j-order-addr-addr');
        //     geo.addr.$lat = geo.$block.find('#j-order-addr-lat');
        //     geo.addr.$lon = geo.$block.find('#j-order-addr-lng');
        //     if(intval(o.geoCity) > 0){
        //         geoMapInit();
        //     }
        //
        //     return {
        //         onCity: function(cityID, ex)
        //         {
        //             if( ! ex.changed ) return;
        //             geo.cityID = cityID;
        //             app.inputError(geo.$block.find('#j-order-city-value'), false);
        //             // map
        //             if(ex.title.length > 0) {
        //                 if(cityID){
        //                     geoMapInit();
        //                 }
        //                 geoMapSearch();
        //             }
        //             if(cityID){
        //                 geo.$cityBlocks.removeClass('hidden');
        //             }else{
        //                 geo.$cityBlocks.addClass('hidden');
        //             }
        //         }
        //     };
        // }());

        // // images
        // img.url = bff.ajaxURL('orders&ev=img&hash='+app.csrf_token+'&item_id='+o.itemID, '');
        // img.$block = $f.find('.j-images');
        // img.type = {
        //     $ajax: img.$block.find('.j-images-type-ajax'),
        //     $simple: img.$block.find('.j-images-type-simple')
        // };
        // img.$togglers = img.$block.find('.j-images-type');
        // img.$togglers.on('click', '.j-images-toggler', function(e){ nothing(e);
        //     var type = $(this).data('type');
        //     img.$togglers.addClass('hide');
        //     img.$togglers.filter('.j-images-type-' + type).removeClass('hide');
        //     img.$block.find('.j-images-type-value').val(type);
        // });
        //
        // img.uploader = new qq.FileUploaderBasic({
        //     button: null,
        //     action: img.url+'upload',
        //     limit: intval(o.imgLimit), sizeLimit: o.imgMaxSize,
        //     uploaded: intval(o.imgUploaded),
        //     multiple: true, allowedExtensions: ['jpeg','jpg','png','gif'],
        //     onSubmit: function(id, fileName) {
        //         return imgProgress(id, 'start', false);
        //     },
        //     onComplete: function(id, fileName, resp) {
        //         if(resp && resp.success) {
        //             imgProgress(id, 'preview', resp);
        //             imgRotate(true);
        //         } else {
        //             if(resp.errors) {
        //                 app.alert.error(resp.errors);
        //                 imgProgress(id, 'remove');
        //             }
        //         }
        //         return true;
        //     },
        //     onCancel: function(id, fileName) {
        //         imgProgress(id, 'remove');
        //     },
        //     showMessage: function(message, code) {
        //         app.alert.error(message);
        //     },
        //     messages: {
        //         typeError: o.lang.upload_typeError,
        //         sizeError: o.lang.upload_sizeError,
        //         minSizeError: o.lang.upload_minSizeError,
        //         emptyError: o.lang.upload_emptyError,
        //         limitError: o.lang.upload_limitError,
        //         onLeave: o.lang.upload_onLeave
        //     }
        // });
        // img.type.$ajax.find('.j-img-upload .j-img-link').each(function(){
        //     img.uploader._createUploadButton(this);
        // });
        //
        // for(var i in o.imgData) {
        //     if( o.imgData.hasOwnProperty(i) ) {
        //         imgProgress('img'+i, 'start', true);
        //         imgProgress('img'+i, 'preview', o.imgData[i]);
        //     }
        // }
        // imgRotate();
        //
        // img.type.$ajax.on('click', '.j-img-delete', function(e){ nothing(e);
        //     imgRemove( $(this).data('id') );
        // });
        //

        price.$block = $f.find('#j-order-price-block');
        price.hide = function () {
            price.$block.addClass('hidden');
        };
        price.set = function (specid) {
            if (price.spec_id == specid) {
                price.$block.removeClass('hidden');
                return;
            }
            if (price.cache.hasOwnProperty(specid)) {
                price.show(price.cache[specid]);
                price.spec_id = specid;
            } else {
                bff.ajax(bff.ajaxURL('orders', 'price-form-sett'), {spec_id: specid}, function (resp) {
                    if (resp && resp.success) {
                        price.show(price.cache[specid] = resp.html);
                        price.spec_id = specid;
                    }
                });
            }
        };
        price.show = function (data) {
            price.$block.html(data);
            price.$block.removeClass('hidden');
        };

        price.$block.on('focus', '[name="price"]', function () {
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });
        price.$block.on('focus', '[name="price_curr"]', function () {
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });
        price.$block.on('focus', '[name="price_rate"]', function () {
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });

        if (!intval(o.itemID)) setPrice();

        dp.$block = $f.find('#j-order-form-dp');

        dp.$block.on('change', '.j-dp-child', function () {
            var $el = $(this);
            var t = o.typeID;
            if (!t) {
                t = intval(f.$field('type').filter(':checked').val());
            }
            var id = $el.data('id');
            var prefix = $el.data('name');
            var val = $el.val();
            var key = id + '-' + val;
            if (!intval(val)) dp.child[key] = {form: ''};
            if (dp.child.hasOwnProperty(key)) {
                var data = dp.child[key];
                $('.j-dp-child-' + id, dp.$block).html((data.form.length ? data.form : '')).toggleClass('hidden', !data.form.length);
            } else {
                bff.ajax(bff.ajaxURL('orders', 'dp-child'), {
                    dp_id: id,
                    dp_value: val,
                    name_prefix: prefix,
                    type: t
                }, function (data) {
                    if (data && data.success) {
                        dp.child[key] = data;
                        $('.j-dp-child-' + id, dp.$block).html((data.form.length ? data.form : '')).toggleClass('hidden', !data.form.length);
                    }
                });
            }
        });

        //$('select[name=origin_checker]').change(function(){
        //    if ($(this).val()>0) {
        //        $('select[name=origin_percent]').show();
        //    }
        //    else {
        //        $('select[name=origin_percent]').hide();
        //    }
        //});

        /************************** ************************/
        /*************** Open invite modal *****************/
        /************************** ************************/
        function  loadInviteSearch(url, callback, params) {
            $('#inviteModal .isSrhBtn').removeClass('active');
            if (!url) url = '/users/';
            params = (params ? params : {});
            params.isInvite = true;
            params.q = $('#inviteModal .isSrhTxt').val();
            var fs = $('#inviteModal select.isSpecBtn').val();
            if (fs && fs.length) {
                // TODO: 18 - это хардкод конечно, но я пока не догнал для чего он нужен вообще.
                params.fs = '{"18":' + JSON.stringify(fs) + '}';
            }
            bff.ajax(url, params,
                function(response, errors){
                    if(response && response.success) {
                        $('#inviteModal .modal-title').text('Найдено ' + response.count); //  + ', выбрано '+ getObjectLength($checkedInvite)
                        $('#inviteModal .modal-body-list').html(response.list + response.pgn);
                        updateLoadInvite();
                        $('#inviteModal').modal('show');
                    }
                    else {
                        app.alert.error(errors);
                    }
                    if (callback) callback(response);
                },
                false,
                {isPost: false}
            );
        }

        if (o.allowChangeInvite) {
            // @see http://davidstutz.github.io/bootstrap-multiselect/
            $('#inviteModal select.isSpecBtn').multiselect({
                enableClickableOptGroups: true,
                enableCollapsibleOptGroups: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: o.lang.filterPlaceholder,
                nonSelectedText: o.lang.nonSelectedText,
                nSelectedText: 'Выбрано',
                numberDisplayed: 0,
                maxHeight: 400,
                buttonWidth: '170px',
                onChange: function (option, checked, select) {
                    $('#inviteModal .l-search-button').addClass('active');
                }
            });

            $('#inviteSelect').on('click', function () {
                //var $btn = $(this).button('loading');
                $('#invitePopUpChecked').html($('#inviteInputs').html());
                $checkedInvite = {};
                $('#inviteInputs').find('input').each(function () {
                    var val = $(this).val();
                    $checkedInvite[val] = val;
                });
                $('.navmenu').addClass('hide');
                if($(window).width() < 992) {
                    $('html, body').animate({
                        scrollTop: $(".navmenu").offset().top
                    }, 2000);
                }
                loadInviteSearch(null, function () {
                    //$btn.button('reset');
                });
            });

            // Любой клик по ссылкам в списке, это поиск по тегам или спец и сортировка
            $('#inviteModal .modal-body-list').on('click', 'a.ajax,a.j-pgn-page', function () {
                $('#inviteModal .modal-title').text(o.lang.waitTitle);
                $('#inviteModal .modal-body-list').html('<div class="waitLoad">' + o.lang.waitText + '</div>');
                loadInviteSearch($(this).attr('href'));
                return false;
            });

            $('#inviteModal .modal-body-list').on('click', 'a.ajax,a.pagination__next', function () {
                $('#inviteModal .modal-title').text(o.lang.waitTitle);
                $('#inviteModal .modal-body-list').html('<div class="waitLoad">' + o.lang.waitText + '</div>');
                loadInviteSearch($(this).attr('href'));
                return false;
            });

            $('#inviteModal .modal-body-list').on('click', 'a.ajax,a.pagination__prev', function () {
                $('#inviteModal .modal-title').text(o.lang.waitTitle);
                $('#inviteModal .modal-body-list').html('<div class="waitLoad">' + o.lang.waitText + '</div>');
                loadInviteSearch($(this).attr('href'));
                return false;
            });

            $('#inviteModal .close').on('click', function () {
                $('.navmenu').removeClass('hide');
            });

            // Форма поиска - очистка
            $('#inviteModal .isRmvBtn').on('click', function () {
                $('#inviteModal .isSrhTxt').val('');
                $('#inviteModal select.isSpecBtn').multiselect('deselectAll', false).multiselect('updateButtonText');
                $('#inviteModal .l-search-button').removeClass('active');
                loadInviteSearch();
            });
            $('#inviteModal .isSrhBtn').on('click', function () {
                loadInviteSearch();
            });
            $('#inviteModal .isSrhTxt').on('keypress', function (e) {
                if (e.which == 13) {
                    loadInviteSearch();
                } else {
                    $('#inviteModal .l-search-button').addClass('active');
                }
            });

            // Выбор из списка в попапе
            $('#inviteModal .modal-body-list').on('change', 'input', function () {
                var val = $(this).val();
                //console.log(val, $(this).is(':checked'), $checkedInvite[val]);
                if ($(this).is(':checked')) {
                    if (getObjectLength($checkedInvite) >= o.limitInvite) {
                        alert(o.lang.limitAlert);
                        $(this).removeAttr('checked');
                        return;
                    }
                    if (!$checkedInvite[val]) {
                        $('#invitePopUpChecked').append('<button type="button" class="btn btn-info btn-xs">' + $(this).data('login') + '<input type="hidden" value="' + val + '" name="invite[]"/></button>');
                        $checkedInvite[val] = val;
                        $(this).parents('label').addClass('selected');
                    }
                } else {
                    delete ($checkedInvite[val]);
                    $('#invitePopUpChecked input[value=' + val + ']').parent().remove();
                    $(this).parents('label').removeClass('selected');
                }
            });

            // eудаление выбранных экспертов
            $('#invitePopUpChecked, #inviteInputs').on('click', 'button', function () {
                var val = $(this).find('input').val();
                delete ($checkedInvite[val]);
                $(this).remove();
                if ($('#inviteModal').is(':visible')) {
                    var el = $('#inviteModal .modal-body-list input[value=' + val + ']');
                    el.removeAttr('checked');
                    el.parents('label').removeClass('selected');
                }
            });

            // Подтверждение выбора
            $('#inviteModal .btn-primary').on('click', function () {
                // add to input selected users
                $('#inviteInputs').html($('#invitePopUpChecked').html());
                $('#inviteModal').modal('hide');
            });

        }

        // Раскрытие списка услуг
        $('#orderPaySelect').on('click', function () {
            var i = $(this).find('i');
            var sl = $(this).next();
            if (i.hasClass('fa-angle-double-down')) {
                i.removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
                sl.slideDown();
            } else {
                i.removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
                sl.slideUp();
            }
        });
        /*********************************/

    }

    function updateLoadInvite() {
        $('#inviteModal .modal-body-list input').each(function(){
            var el = $(this);
            if ($checkedInvite[el.val()]) {
                el.attr('checked', 'checked');
                el.parents('label').addClass('selected');
            }
        });
    }
    function geoMapInit()
    {
        if(geo.addr.inited) return;
        geo.addr.inited = 1;
        geo.addr.map = app.map(geo.$block.find('#j-order-addr-map').show().get(0), [geo.addr.$lat.val(), geo.addr.$lon.val()], function(map){
            geo.addr.mapEditor = bff.map.editor();
            geo.addr.mapEditor.init({
                map: map, version: '2.1',
                coords: [geo.addr.$lat, geo.addr.$lon],
                address: geo.addr.$addr,
                addressKind: 'house',
                updateAddressIgnoreClass: 'typed'
            });

            geo.addr.$addr.bind('change keyup input', $.debounce(function(){
                if( ! $.trim(geo.addr.$addr.val()).length ) {
                    geo.addr.$addr.removeClass('typed');
                } else {
                    geo.addr.$addr.addClass('typed');
                    geoMapSearch();
                }
            }, 700));
            geoMapSearch();
        }, {zoom: 13});

    }

    function geoMapSearch()
    {
        if( ! geo.addr.mapEditor) { return; }
        var query = [o.geoCountry];
        var city = $.trim( geo.$block.find('#j-order-city-select').val() );
        if(city) query.push(city);
        var addr = $.trim( geo.addr.$addr.val() );
        if(addr) query.push(addr);
        query = query.join(', ');
        if( geo.addr.lastQuery == query ) return;
        geo.addr.mapEditor.search( geo.addr.lastQuery = query, false, function(){
            geo.addr.mapEditor.centerByMarker();
        } );
    }

    function imgProgress(id, step, data)
    {
        var $slot, $preview;
        switch(step)
        {
            case 'start': {
                $slot = img.type.$ajax.find('.j-img-slot:not(.j-active) .j-img-upload:visible:first').closest('.j-img-slot');
                if( $slot.length ) {
                    img.active[id] = {slot:$slot,data:false};
                    $slot.addClass('j-active'+(data === false ? ' '+img.classes.progress : ''));
                    $slot.find('.j-img-img').attr('src', app.rootStatic+'/img/loader.gif');
                    $slot.find('.j-img-preview').removeClass('hidden');
                    $slot.find('.j-img-upload').addClass('hidden');
                }else{
                    return false;
                }
            } break;
            case 'preview': {
                if( img.active.hasOwnProperty(id) ) {
                    $slot = img.active[id].slot;
                    img.active[id].data = data;
                    $slot.removeClass(img.classes.progress)
                    $slot.find('.j-img-img').attr('src', data.i);
                    $slot.find('.j-img-fn').val(data.filename).attr('name','images['+data.id+']');
                    $slot.find('.j-img-delete').data('id', id);
                }
            } break;
            case 'remove': {
                imgRemove(id);
            } break;
        }
        return true;
    }

    function imgRotate(update)
    {
        return false; // disable
        // var $slots = img.type.$ajax.find('.j-img-slots');
        // if (!$slots.length) return;
        // if(update === true) {
        //     $slots.sortable('refresh');
        // } else {
        //     $slots.sortable({
        //         items: '.j-active'
        //     });
        // }
    }

    function imgRemove(id)
    {
        if( img.active.hasOwnProperty(id) )
        {
            var $slot = img.active[id].slot;
            var data = img.active[id].data;
            var clearSlot = function(id, $slot) {
                img.type.$ajax.find('.j-img-upload:visible:last').closest('.j-img-slot').after($slot);
                $slot.find('.j-img-img').attr('src', '');
                $slot.find('.j-img-fn').val('').attr('name','');
                $slot.find('.j-img-preview').addClass('hidden');
                $slot.find('.j-img-upload').removeClass('hidden');
                $slot.removeClass('j-active');

                img.uploader.decrementUploaded();
                delete img.active[id];
                imgRotate(true);
            };
            if( data !== false ) {
                bff.ajax(img.url+'delete',
                    {image_id:data.id, filename:data.filename}, function(resp){
                        if(resp && resp.success) {
                            clearSlot(id, $slot);
                        } else {
                            app.alert.error(resp.errors);
                        }
                    });
            } else {
                clearSlot(id, $slot);
            }
        }
    }

    function setPrice(type)
    {
        type = intval(type);
        if(type && type != intval(o.typeService)){
            price.hide();
            return;
        }

        var specid = 0;
        var $bl = $f.find('.j-specs:visible:first');
        if($bl.length){
            specid = intval($bl.find('.j-spec-value').val());
        }

        if( ! specid){
            price.hide();
            return;
        }
        price.set(specid);
    }

    function setDp()
    {
        var t = o.typeID, id = 0;
        if( ! t){
            t = intval(f.$field('type').filter(':checked').val());
        }
        if(t == intval(o.typeService)){
            var $bl = $f.find('.j-spec-select:first');
            if ($bl.length) {
                id = intval($bl.find('.j-spec-value').val());
            }
        }
        else if(t == intval(o.typeProduct)){
            var $bl = $f.find('.j-cat-select:first');
            if ($bl.length) {
                id = intval($bl.find('.j-cat-value').val());
            }
        }
        if(t && id){
            if (dp.cache.hasOwnProperty(t) && dp.cache[t].hasOwnProperty(id)) {
                dp.$block.html(dp.cache[t][id]);
            } else {
                if ( ! dp.cache.hasOwnProperty(t)) {
                    dp.cache[t] = {};
                }
                if ( ! dp.cache[t].hasOwnProperty(id)) {
                    bff.ajax(bff.ajaxURL('orders', 'dp-form'), {type:t, id:id}, function(data){
                        if(data && data.dp){
                            dp.$block.html(dp.cache[t][id] = data.dp);
                        }else{
                            dp.$block.html('');
                        }
                    });
                }
            }
        } else {
            dp.$block.html('');
        }
    }

    function getAvgPrice() {
        bff.ajax(bff.ajaxURL('orders', 'getAvgPrice'), $f.serialize(), function(data){
            if(data.result) {
                $('#avgPriceLabel').html('от ' + data.min_avg_price + (data.max_avg_price ? ' до ' + data.max_avg_price : '') + ' рублей');
                // $('#avgPriceLabel').html(data.avg_price + ' рублей');
            }
        });
    };

    function dateOfDelivery() {
        $('#term_notify').hide();
        // $('#min_term').hide();
        bff.ajax(bff.ajaxURL('orders', 'checkDateOfDelivery'), $f.serialize(), function(data){
            if(data.changeTerm) {
                $('#term_notify').show();
                $('#min_term span').html(data.minTerm);
            }
        });
    };

    function checkPromocode() {
        var promocode = $(this).val();
        var $parent = $(this).parent();

        if(!$parent.hasClass('form-control'))
            $parent = $parent.parent();

        $parent.removeClass('has-success');

        if(promocode!="") {
            bff.ajax('/promocode/check', {promocode: promocode}, function(data){

                if(data.valid == 'true') {
                    f.fieldsResetError();
                    $parent.addClass('has-success');
                }
                else {
                    f.fieldError('promocode','Неверно задан прокомод');
                }
            });
        }
        else{
            f.fieldsResetError();
        }
    };

    function originChecker(i) {
        if (intval($(this).val())) $(this).next().show();
        else $(this).next().hide();
    };

    function addDueDate() {
        $('#term_notify').hide();
        // $('#min_term').hide();
        bff.ajax(bff.ajaxURL('orders', 'addDueDate'), $f.serialize(), function(data){
            if( data.result && data.dueDate) {
                if($('#due_date_new').length > 0){
                    var date = data.dueDate.split('-');
                    var dateStr = date[2] + '-' + date[1] + '-' + date[0];
                    document.getElementById("due_date_new").value = dateStr;
                }
                $('#due_date').val(data.dueDate);
            }
        });
    };

    function getDefaultCountPage(options) {
        var count_pages_from = $('#count_pages_from');
        var count_pages_to = $('#count_pages_to');
        var val1 = intval(count_pages_from.val());
        var val2 = intval(count_pages_to.val());
        if (!val1 && !val2) {
            bff.ajax(bff.ajaxURL('specializations', 'get-default-count-page'), {serv_id : $('.j-serv-value').val()}, function(data){
                if(data.result) {
                    count_pages_from.val(data.count_pages_from);
                    count_pages_to.val(data.count_pages_to);
                }
            });
        }
    };

    function specValueChange(options) {
        var spec = $(".j-spec-value option:selected");
        $(".j-cat-value").val(spec.attr('data-catid'));
        $(".j-type-value").val(spec.attr('data-typeid'));
    };

    return {
        getAvgPrice: getAvgPrice,
        dateOfDelivery: dateOfDelivery,
        getDefaultCountPage: getDefaultCountPage,
        addDueDate: addDueDate,
        init: function(options)
        {
            if(inited) return; inited = true;
            $.extend(true, o, options || {});
            $(function(){ init();});
        }
    };
}());