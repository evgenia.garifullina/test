
/* https://github.com/filamentgroup/loadCSS/ */
/*! loadCSS. [c]2017 Filament Group, Inc. MIT License */
(function(w){
    "use strict";
    /* exported loadCSS */
    var loadCSS = function( href, before, media ){
        // Arguments explained:
        // `href` [REQUIRED] is the URL for your CSS file.
        // `before` [OPTIONAL] is the element the script should use as a reference for injecting our stylesheet <link> before
        // By default, loadCSS attempts to inject the link after the last stylesheet or script in the DOM. However, you might desire a more specific location in your document.
        // `media` [OPTIONAL] is the media type or query of the stylesheet. By default it will be 'all'
        var doc = w.document;
        var ss = doc.createElement( "link" );
        var ref;
        if( before ){
            ref = before;
        }
        else {
            var refs = ( doc.body || doc.getElementsByTagName( "head" )[ 0 ] ).childNodes;
            ref = refs[ refs.length - 1];
        }

        var sheets = doc.styleSheets;
        ss.rel = "stylesheet";
        ss.href = href;
        // temporarily set media to something inapplicable to ensure it'll fetch without blocking render
        ss.media = "only x";

        // wait until body is defined before injecting link. This ensures a non-blocking load in IE11.
        function ready( cb ){
            if( doc.body ){
                return cb();
            }
            setTimeout(function(){
                ready( cb );
            });
        }
        // Inject link
        // Note: the ternary preserves the existing behavior of "before" argument, but we could choose to change the argument to "after" in a later release and standardize on ref.nextSibling for all refs
        // Note: `insertBefore` is used instead of `appendChild`, for safety re: http://www.paulirish.com/2011/surefire-dom-element-insertion/
        ready( function(){
            ref.parentNode.insertBefore( ss, ( before ? ref : ref.nextSibling ) );
        });
        // A method (exposed on return object for external use) that mimics onload by polling document.styleSheets until it includes the new sheet.
        var onloadcssdefined = function( cb ){
            var resolvedHref = ss.href;
            var i = sheets.length;
            while( i-- ){
                if( sheets[ i ].href === resolvedHref ){
                    return cb();
                }
            }
            setTimeout(function() {
                onloadcssdefined( cb );
            });
        };

        function loadCB(){
            if( ss.addEventListener ){
                ss.removeEventListener( "load", loadCB );
            }
            ss.media = media || "all";
        }

        // once loaded, set link's media back to `all` so that the stylesheet applies once it loads
        if( ss.addEventListener ){
            ss.addEventListener( "load", loadCB);
        }
        ss.onloadcssdefined = onloadcssdefined;
        onloadcssdefined( loadCB );
        return ss;
    };
    // commonjs
    if( typeof exports !== "undefined" ){
        exports.loadCSS = loadCSS;
    }
    else {
        w.loadCSS = loadCSS;
    }
}( typeof global !== "undefined" ? global : this ));

/*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License */
(function( w ){
    // rel=preload support test
    if( !w.loadCSS ){
        return;
    }
    var rp = loadCSS.relpreload = {};
    rp.support = function(){
        try {
            return w.document.createElement( "link" ).relList.supports( "preload" );
        } catch (e) {
            return false;
        }
    };

    // loop preload links and fetch using loadCSS
    rp.poly = function(){
        var links = w.document.getElementsByTagName( "link" );
        for( var i = 0; i < links.length; i++ ){
            var link = links[ i ];
            if( link.rel === "preload" && link.getAttribute( "as" ) === "style" ){
                w.loadCSS( link.href, link, link.getAttribute( "media" ) );
                link.rel = null;
            }
        }
    };

    // if link[rel=preload] is not supported, we must fetch the CSS manually using loadCSS
    if( !rp.support() ){
        rp.poly();
        var run = w.setInterval( rp.poly, 300 );
        if( w.addEventListener ){
            w.addEventListener( "load", function(){
                rp.poly();
                w.clearInterval( run );
            } );
        }
        if( w.attachEvent ){
            w.attachEvent( "onload", function(){
                w.clearInterval( run );
            } )
        }
    }
}( this ));

// imagesLoaded
/*(function (c, n) {
    var k = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
    c.fn.imagesLoaded = function (l) {
        bff_report_exception('DEPRECATED imagesLoaded');
        function m() {
            var b = c(h), a = c(g);
            d && (g.length ? d.reject(e, b, a) : d.resolve(e));
            c.isFunction(l) && l.call(f, e, b, a)
        }

        function i(b, a) {
            b.src === k || -1 !== c.inArray(b, j) || (j.push(b), a ? g.push(b) : h.push(b), c.data(b, "imagesLoaded", {
                isBroken: a,
                src: b.src
            }), o && d.notifyWith(c(b), [a, e, c(h), c(g)]), e.length === j.length && (setTimeout(m), e.unbind(".imagesLoaded")))
        }

        var f = this, d = c.isFunction(c.Deferred) ? c.Deferred() :
            0, o = c.isFunction(d.notify), e = f.find("img").add(f.filter("img")), j = [], h = [], g = [];
        e.length ? e.bind("load.imagesLoaded error.imagesLoaded", function (b) {
                i(b.target, "error" === b.type)
            }).each(function (b, a) {
                var e = a.src, d = c.data(a, "imagesLoaded");
                if (d && d.src === e) i(a, d.isBroken); else if (a.complete && a.naturalWidth !== n) i(a, 0 === a.naturalWidth || 0 === a.naturalHeight); else if (a.readyState || a.complete) a.src = k, a.src = e
            }) : m();
        return d ? d.promise(f) : f
    }
})(jQuery);
*/

function yaSetGoal(target, callback) {
    if (typeof ga !== "undefined") {
        console.log('+yaSetGoal', target);
        // google
        gtag('config', 'G-WW3XEVMJN1', {'page_path': '/'+target});
        gtag('event', target, {'preform': 'submit'});

        // mail.ru
        var _tmr = window._tmr || (window._tmr = []);
        _tmr.push({ id: "34831085", type: "reachGoal", goal: target });

        // yandex
        if (typeof yaCounter === "undefined") {
            var ik=0;
            var i = setInterval(function () {
                ik++;
                if (typeof yaCounter === "undefined") {
                    if (ik > 10) clearInterval(i);
                    return '';
                }
                yaCounter.reachGoal(target);
                clearInterval(i);
            }, 500)
        } else {
            yaCounter.reachGoal(target);
        }
        setTimeout(callback, 500);
        // callback.apply();
    } else if(callback) {
        console.log('-yaSetGoal', target);
        callback.apply();
    } else {
        console.log('--yaSetGoal', target);
    }
}

// https://fancyapps.com/fancybox/3/docs/
function openSlideHowItWork(callBackClose) {// заказчики
    yaSetGoal('SlideHowItWork');
    var params = {
        loop : true,
        clickContent: "next",
        clickSlide: "next"
    };
    if (callBackClose) {
        params['afterClose'] = callBackClose;
    }
    bff.includeSrc({
        css: ['/js/bff/fancybox3/jquery.fancybox.css'],
        js: ['/js/bff/fancybox3/jquery.fancybox.js'],
        script: [function(){
            $.fancybox.open([
                {src  : '/img/how_it_work/0.png'},
                {src  : '/img/how_it_work/1.png'},
                {src  : '/img/how_it_work/2.png'},
                {src  : '/img/how_it_work/3.png'},
                {src  : '/img/how_it_work/4.png'},
                {src  : '/img/how_it_work/5.png'},
                {src  : '/img/how_it_work/6.png'},
                {src  : '/img/how_it_work/7.png'},
                {src  : '/img/how_it_work/8.png'},
                {src  : '/img/how_it_work/9.png'},
                {src  : '/img/how_it_work/10.png'},
                {src  : '/img/how_it_work/11.png'},
                {src  : '/img/how_it_work/12.png'},
                {src  : '/img/how_it_work/13.png'}
            ], params);
        }]
    });

}
function SlideForWorkerHelp(callBackClose) { // авторы
    yaSetGoal('SlideHowItWork2');
    var params = {
        loop : true,
        clickContent: "next",
        clickSlide: "next"
    };
    if (callBackClose) {
        params['afterClose'] = callBackClose;
    }
    bff.includeSrc({
        css: ['/js/bff/fancybox3/jquery.fancybox.css'],
        js: ['/js/bff/fancybox3/jquery.fancybox.js'],
        script: [function(){
            $.fancybox.open([
                {src  : '/img/hiw2/0.png'},
                {src  : '/img/hiw2/1.png'},
                {src  : '/img/hiw2/2.png'},
                {src  : '/img/hiw2/3.png'},
                {src  : '/img/hiw2/4.png'},
                {src  : '/img/hiw2/5.png'},
                {src  : '/img/hiw2/6.png'},
                {src  : '/img/hiw2/7.png'},
                {src  : '/img/hiw2/8.png'},
                {src  : '/img/hiw2/9.png'},
                {src  : '/img/hiw2/10.png'},
                {src  : '/img/hiw2/11.png'},
                {src  : '/img/hiw2/12.png'},
                {src  : '/img/hiw2/13.png'},
                {src  : '/img/hiw2/14.png'}
            ], params);
        }]
    });
}
