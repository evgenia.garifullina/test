(function(){
	$('.editOrder').on('click', onEditOrderClick);
})();

function onSuccessGetNotDeclinedOffers(defaultEvent) {
	return function(data) {
		if (data && data.success && data.not_declined_offers_count > 0)
		{
			app.alert.error('При наличии предложений от экспертов невозможно изменить заказ. Создайте новый заказ с другим описанием.');
		}
		else
		{
			window.location = defaultEvent.target.href;
		}
	};
}

function onEditOrderClick(e)
{
    window.location = e.target.href;
	// nothing(e);
	// var order_id = $(e.target).data('order_id');
	// bff.ajax(
	// 	"/index.php?bff=ajax&s=orders&act=not_declined_offers_count",
	// 	{order_id: order_id},
	// 	onSuccessGetNotDeclinedOffers(e)
	// );
}
