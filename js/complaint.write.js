var jComplaintWrite = (function() {
	var inited = false, o = {lang: {}};
	var $block;

	function init() {
		$block = $('#profile-collapse');
		var $complaintPopup = false;
		$('#j-complaint-write').click(function () {
			if ($('#j-complaint-write').data('recipient-id') != 'undefined'
				&& $('#j-complaint-write').data('recipient-id') > 0) {
				recipient_id = $('#j-complaint-write').data('recipient-id');
			}
			else {
				recipient_id = o.id;
			}
			console.log('#j-complaint-write - was clicked');
			console.log('recipient_id ' + recipient_id);
			if ($complaintPopup === false) {
				bff.ajax(bff.ajaxURL('complaints', 'write'), {id: recipient_id}, function (data, errors) {
					if (data && data.success) {
						console.log('Data was received');
						$block.after(data.popup);
						$complaintPopup = $('#j-complaint-modal');
						$complaintPopup.modal('show');
					} else {
						app.alert.error(errors);
					}
				});
			} else {
				$complaintPopup.modal('show');
			}
			return false;
		});
	}
	function sendComplaint() {
		$('#j-complaint-send').click(function () {
			$(this).attr('disabled', 'disabled');
			console.log('clicked j-complaint-send');
			$data = $('#j-complaint-form').serialize();
			url = o.sUrl;

			$('#j-complaint-modal .modal-body .row').fadeOut(function () {
				$('#j-complaint-modal .modal-body').append('<h4>Ожидайте...</h4>');
			});


			bff.ajax(bff.ajaxURL('complaints', 'send'), $data, function (resp) {
				if (resp.success) {
					setTimeout(function () {
						$('#j-complaint-modal .modal-body h4').remove();
						$('#j-complaint-modal .modal-body').append('<h4>Спасибо, ваша жалоба отправлена</h4>');
					}, 500);
				}
				else {
					setTimeout(function () {
						$('#j-complaint-modal .modal-body .row').remove();
						$('#j-complaint-modal .modal-body').append('<h4>Что-то пошло не так, перезагрузите страницу и попробуйте еще раз. </h4>');
					}, 500);
				}
				setTimeout(function () {
					$('#j-complaint-modal').modal('hide');
					setTimeout(function () {
						$('#j-complaint-modal .modal-body .row').show();
						$('#j-complaint-modal .modal-body h4').remove();
						$('#complaint_message').val('');
						$(this).removeAttr('disabled');
					}, 500);
				}, 1000);
			});
		});
	}
	return {
		init: function(options)
		{
			if(inited) return; inited = true;
			o = $.extend(o, options || {});
			$(function(){ init(); });
		},
		send: function(options)
		{
			o = $.extend(o, options || {});
			$(function () { sendComplaint(o.sUrl); })
		}
	};
}());