var jUserAuth = (function(){
    var inited = false, url = document.location.pathname, o = {lang:{}};

    function init(options, callback)
    {
         if(inited) return; inited = true;
         o = $.extend(o, options || {});
         $(function(){
             callback();
         });
    }

    function initLoginSocialButtons()
    {
        var url_login = o.login_social_url,
            url_return = o.login_social_return,
            popup;

        function socialPopup(o)
        {
            o = $.extend({w: 450, h: 380}, o || {});
            if(popup !== undefined) popup.close();
            var centerWidth = ($(window).width() - o.w) / 2;
            var centerHeight = ($(window).height() - o.h) / 2;
            popup = window.open(url_login+ o.provider+'?ret='+encodeURIComponent(url_return), "u_login_social_popup", "width=" + o.w + ",height=" + o.h + ",left=" + centerWidth + ",top=" + centerHeight + ",resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes");
            popup.focus();
            return false;
        }
        $('.j-u-login-social-btn').on('click', function(e){ nothing(e);
            var meta = $(this).metadata();
            if( meta && meta.hasOwnProperty('provider') ) {
                socialPopup(meta);
            }
        });
    }

    return {
        post: function(options)
        {
            $(document).on("click", "#j-mail-link", function() {
                var mail_link = '';
                var sendmail = $("#j-email-sender").val();
                var parts_sendmail = sendmail.split("@");
                var val = parts_sendmail[1];
                $.each(options.post_url, function(i, msg){
                    if (i == val){
                        mail_link = msg;
                    }
                });
                $(this).attr("href", mail_link);
            });
        },
        login: function(options)
        {
            o.login_social_url = '/';
            o.login_social_return = '/orders/';
            init(options, function()
            {
                var f = app.form('#j-u-login-form', function($f){
                    if( ! f.checkRequired() ) return;
                    if( ! f.fieldStr('pass').length ) {
                        return f.fieldError('pass', o.lang.pass);
                    }
                    f.ajax(url, {}, function(data, errors){
                        if(data && data.success) {
                            bff.redirect(data.redirect);
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
                initLoginSocialButtons();
                app.$B.on('click', '.j-resend-activation', function(e){ nothing(e);
                    if( ! bff.isEmail( f.fieldStr('email') ) ) {
                        f.fieldError('email', o.lang.email); return;
                    }
                    f.ajax(url, {step:'resend-activation'}, function(data, errors){
                        if(data && data.success) {
                            bff.redirect(data.redirect);
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
            });
        },
        register: function(options)
        {
            o.captcha = false;
            o.agreement = false;
            o.pass_confirm = false;
            o.login_social_url = '/';
            o.login_social_return = '/orders/';
            $('.userType input').on('change', function() {
                var userTypeVal = $('.userType input:radio:checked').val();
                if (userTypeVal==1 || userTypeVal==3) {
                    $('.userPhone').addClass('required');
                    $('.userPhone input').addClass('j-required');
                    $('.userPhone input').attr('id', 'j-u-register-phone');
                }
                else {
                    $('.userPhone').removeClass('required');
                    $('.userPhone input').removeClass('j-required')
                    $('.userPhone input').removeAttr('id', 'j-u-register-phone');

                }
            });

            $( '.s-signBlock-form--register .j-show-pass' ).on( 'click',
                function() {
                    $( this ).toggleClass( 'display-password' );
                    if ( $( this ).hasClass( 'display-password' ) ) {
                        $( this ).siblings( ['input[type="password"]'] ).prop( 'type', 'text' );
                    } else {
                        $( this ).siblings( 'input[type="text"]' ).prop( 'type', 'password' );
                    }
                }
            );

            init(options, function()
            {
                var $phone = $('#j-u-register-phone');

                var f = app.form('#j-u-register-form', function($f){
                    if( ! f.checkRequired() ) return;
                    if( ! bff.isEmail( f.fieldStr('email') ) ) {
                        return f.fieldError('email', o.lang.email);
                    }
                    if( ! f.fieldStr('pass').length ) {
                        return f.fieldError('pass', o.lang.pass);
                    }
                    if( o.pass_confirm ) {
                        var pass2 = f.fieldStr('pass2');
                        if( ! pass2.length || pass2 != f.fieldStr('pass') ) {
                            return f.fieldError('pass2', o.lang.pass2);
                        }
                    }
                    if($phone.length){
                        if(f.fieldStr('phone').length < 6) {
                            return f.fieldError('phone', o.lang.phone);
                        }
                    }
                    if( o.captcha ) {
                        if( ! f.fieldStr('captcha').length ) {
                            return f.fieldError('captcha', o.lang.captcha);
                        }
                    }
                    if( f.$field('type').is('[type="radio"]') && ! f.$field('type').is(':checked')){
                        return f.alertError(o.lang.type, {title:app.lang.form_alert_errors});
                    }
                    if(o.agreement) {
                        if (!f.$field('agreement').is(':checked')) {
                            return f.fieldError('agreement', o.lang.agreement);
                        }
                    }

                    var tFade = setTimeout(function () {
                        bff.fadeAjax(o.lang.fadeAjaxText1);
                        tFade = setTimeout(function () {
                            bff.fadeAjax(o.lang.fadeAjaxText2);
                        }, 10000);
                    }, 500);

                    f.ajax(o.sendUrl, {}, function(data, errors) {
                        clearTimeout(tFade);
                        if (data && data['emailError']) {
                            bff.fadeAjax('<p>'+errors['email']+'</p>');
                            bff.fadeAjaxSetActionClose('Внимание');
                            if (data['fields']) f.fieldsError(data.fields);
                            return;
                        }
                        bff.fadeAjaxHide();
                        if(!data) {
                            app.alert.error(o.lang.errorAjax);
                        }
                        else if(data && data['success']) {
                            //gtag('event', 'authorRegister', {'form': 'submit'});
                            console.log('google event submit');
                            console.log('success', data, errors);
                            bff.redirect(data.redirect);
                        }
                        else {
                            console.log('error', data, errors);
                            if(o.captcha && data.captcha) {
                                $f.find('.j-captcha').triggerHandler('click');
                            }
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
                initLoginSocialButtons();

            });
        },
        registerEmailed: function(options)
        {
            init(options, function()
            {
                var $retry = $('a.retrySedMail'), retry_process = false;
                if( $retry.length ) {
                    $retry.on('click', function(e){
                        nothing(e);
                        if(retry_process) return;
                        $retry.html('письмо отправляется...');
                        bff.ajax(url, {step:'emailed'}, function(data, errors){
                            if(data && data.success) {
                                app.alert.success(o.lang.success);
                                // $retry.fadeOut();
                            } else {
                                app.alert.error(errors);
                            }
                        }, function(p){ retry_process = p; });
                    });
                }
            });
        },
        registerSocial: function(options)
        {
            init(options, function()
            {
                var formStatus = 'register', f, $form;
                function setFormStatus(status)
                {
                    formStatus = status;
                    $('.j-social', $form).toggle();
                    $('.j-shortpage-title').html(o.lang[status].title);
                    $('#j-u-register-social-email', $form).prop({readonly:(status=='login')});
                    app.alert.hide();
                }
                f = app.form('#j-u-register-social-form', function($f)
                {
                    if(formStatus == 'register') {
                        if( ! bff.isEmail( f.fieldStr('email') ) ) {
                            return f.fieldError('email', o.lang.register.email);
                        }
                        if( ! f.$field('type').is(':checked')){
                            return f.alertError(o.lang.register.type, {title:app.lang.form_alert_errors});
                        }
                        if( ! f.$field('agreement').is(':checked') ) {
                            return f.fieldError('agreement', o.lang.register.agreement);
                        }
                        f.ajax(url, {step:'social'}, function(data, errors){
                            if(data && data.success) {
                                if( data.exists ) {
                                    setFormStatus('login');
                                } else {
                                    bff.redirect(data.redirect);
                                }
                            } else {
                                f.fieldsError(data.fields, errors);
                            }
                        });
                    } else {
                        if( ! f.fieldStr('pass').length ) {
                            return f.fieldError('pass', o.lang.login.pass);
                        }
                        f.ajax(o.login_url, {social:true}, function(data, errors){
                            if(data && data.success) {
                                bff.redirect(data.redirect);
                            } else {
                                f.fieldsError(data.fields, errors);
                            }
                        });
                    }
                });
                $form = f.getForm();
                $('#j-u-register-social-email-change', $form).on('click', function(e){ nothing(e);
                    setFormStatus('register');
                });
            });
        },
        forgotStart: function(options)
        {
            init(options, function()
            {
                app.form('#j-u-forgot-start-form', function($f){
                    var f = this;
                    if( ! f.checkRequired() ) return;

                    var tFade = setTimeout(function () {
                        bff.fadeAjax(o.lang.fadeAjaxText1);
                        tFade = setTimeout(function () {
                            bff.fadeAjax(o.lang.fadeAjaxText2);
                        }, 10000);
                    }, 500);

                    f.ajax(url, {}, function(data, errors){
                        clearTimeout(tFade);
                        if (data && data['emailError']) {
                            bff.fadeAjax('<p>'+errors['email']+'</p>');
                            bff.fadeAjaxSetActionClose('Внимание');
                            if (data['fields']) f.fieldsError(data.fields);
                            return;
                        }
                        bff.fadeAjaxHide();
                        if(!data) {
                            app.alert.error(o.lang.errorAjax);
                        }
                        else if(data && data.success) {
                            f.alertSuccess(o.lang.success, {hide:false,reset:true});
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
            });
        },
        forgotFinish: function(options)
        {
            init(options, function()
            {
                app.form('#j-u-forgot-finish-form', function($f){
                    var f = this;
                    if( ! f.fieldStr('pass').length ) {
                        f.fieldError('pass', o.lang.pass); return;
                    }
                    f.ajax(url, {}, function(data, errors){
                        if(data && data.success) {
                            f.alertSuccess(o.lang.success, {hide:false,reset:true});
                            $f.find('button[type="submit"]').prop({disabled:true});
                            if(data.redirect){
                                bff.redirect(data.redirect);
                            }
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });

                });
            });
        }
    };
}());
