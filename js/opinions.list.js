var jOpinionsList = (function(){

    var inited = false, o = {lang:{}, add:0}, listMngr;
    var $block, $filter, $list, $pgn;
    var types = {}, terms = {}, directions = {};

    function init()
    {
        $block = $('#j-opinions-list');
        $filter  = $block.find('.j-filter');
        $list  = $block.find('.j-list');
        $pgn   = $block.find('.j-pagination');

        if(o.add)
        {
            app.form($('#j-opinion-add-form'), function($f){
                var f = this;
                if( ! f.checkRequired({focus:true}) ) return;
                if( ! $f.find('[name="type"]:checked').length){
                    app.alert.error(o.lang.check_type);
                    return;
                }

                f.ajax(bff.ajaxURL('opinions', 'opinion-add'),{},function(resp, errors){
                    if(resp && resp.success) {
                        $('#j-opinion-add-block').hide();
                        listMngr.submit({}, true);
                        if(resp.cache){
                            updateOpinionsCache(resp.cache);
                        }
                    } else {
                        f.alertError(errors);
                    }
                });
            }, {noEnterSubmit: true});

            app.form($('#j-opinion-order-add-form'), function($f){
                var f = this;
                if( ! f.checkRequired({focus:true}) ) return;
                if( ! $f.find('[name="type"]:checked').length){
                    app.alert.error(o.lang.check_type);
                    return;
                }

                f.ajax(bff.ajaxURL('opinions', 'opinion-add'),{},function(resp, errors){
                    if(resp && resp.success) {
                        location.href = o.url;
                    } else {
                        f.alertError(errors);
                    }
                });
            }, {noEnterSubmit: true});
        }

        // if (!$filter.length) return;

        for(var i in o.types){
            if(o.types.hasOwnProperty(i)){
                if(o.types[i].hasOwnProperty('id') && o.types[i].hasOwnProperty('plural')){
                    types[ o.types[i].id ] = o.types[i].plural;
                }
            }
        }

        for(i in o.terms){
            if(o.terms.hasOwnProperty(i)){
                if(o.terms[i].hasOwnProperty('id') && o.terms[i].hasOwnProperty('t')){
                    terms[ o.terms[i].id ] = o.terms[i].t;
                }
            }
        }

        for(i in o.directions){
            if(o.directions.hasOwnProperty(i)){
                if(o.directions[i].hasOwnProperty('id') && o.directions[i].hasOwnProperty('t')){
                    directions[ o.directions[i].id ] = o.directions[i].t;
                }
            }
        }

        $list.on('click', '.j-opinion-edit', function(){
            var $el = $(this);
            var id = $el.data('id');
            var $f = $list.find('.j-my-opinion-edit-'+id);
            var $bl = $el.closest('.j-opinion');
            if($f.length){
                $f.show();
                $bl.hide();
                if( ! $f.hasClass('i')){
                    $f.addClass('i');
                    $f.find('.j-cancel').click(function(){
                        $f.hide();
                        $bl.show();
                        return false;
                    });
                    app.form($f.find('form'), function(){
                        var f = this;
                        if( ! f.checkRequired({focus:true}) ) return;
                        f.ajax(bff.ajaxURL('opinions', 'opinion-edit'),{},function(resp, errors){
                            if(resp && resp.success) {
                                location.reload();
                                listMngr.submit({}, false);
                                if(resp.cache){
                                    updateOpinionsCache(resp.cache);
                                }
                            } else {
                                f.alertError(errors);
                            }
                        });
                    }, {noEnterSubmit: true});
                }
            }
            return false;
        });

        $list.on('click', '.j-opinion-order-edit', function(){
            var $el = $(this);
            var id = $el.data('id');
            var $f = $list.find('.j-my-opinion-edit-'+id);
            var $bl = $el.closest('.j-opinion');
            if($f.length){
                $f.show();
                $bl.hide();
                if( ! $f.hasClass('i')){
                    $f.addClass('i');
                    $f.find('.j-cancel').click(function(){
                        $f.hide();
                        $bl.show();
                        return false;
                    });
                    app.form($f.find('form'), function(){
                        var f = this;
                        if( ! f.checkRequired({focus:true}) ) return;
                        f.ajax(bff.ajaxURL('opinions', 'opinion-edit'),{},function(resp, errors){
                            if(resp && resp.success) {
                                location.reload();
                            } else {
                                f.alertError(errors);
                            }
                        });
                    }, {noEnterSubmit: true});
                }
            }
            return false;
        });

        $list.on('click', '.j-opinion-delete', function(){
            if(confirm(o.lang.opinion_delete)){
                bff.ajax(bff.ajaxURL('opinions', 'opinion-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        location.reload();
                        listMngr.submit({popstate:true}, false);
                        if(resp.cache){
                            updateOpinionsCache(resp.cache);
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $list.on('click', '.j-opinion-order-delete', function(){
            if(confirm(o.lang.opinion_delete)){
                bff.ajax(bff.ajaxURL('opinions', 'opinion-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        location.reload();
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $list.on('click', '.j-answer-add', function(){
            var $bl = $('#j-answer-add-'+$(this).data('id'));
            if($bl.length){
                if( ! $bl.hasClass('i')){
                    $bl.addClass('i');
                    app.form($bl.find('form'), function(){
                        var f = this;
                        if( ! f.checkRequired({focus:true}) ) return;
                        f.ajax(bff.ajaxURL('opinions', 'answer-add'),{},function(resp, errors){
                            if(resp && resp.success) {
                                listMngr.submit({}, false);
                                if(resp.cache){
                                    updateOpinionsCache(resp.cache);
                                }
                            } else {
                                f.alertError(errors);
                            }
                        });
                    }, {noEnterSubmit: true});

                }
            }
        });

        $list.on('click', '.j-answer-edit', function(){
            var $el = $(this);
            var id = $el.data('id');
            var $f = $list.find('.j-answer-edit-'+id);
            var $bl = $el.closest('.j-answer');
            if($f.length){
                $f.show();
                $bl.hide();
                if( ! $f.hasClass('i')){
                    $f.addClass('i');
                    $f.find('.j-cancel').click(function(){
                        $f.hide();
                        $bl.show();
                        return false;
                    });
                    app.form($f.find('form'), function(){
                        var f = this;
                        if( ! f.checkRequired({focus:true}) ) return;
                        f.ajax(bff.ajaxURL('opinions', 'answer-edit'),{},function(resp, errors){
                            if(resp && resp.success) {
                                listMngr.submit({}, false);
                            } else {
                                f.alertError(errors);
                            }
                        });
                    }, {noEnterSubmit: true});
                }
            }
            return false;
        });

        $list.on('click', '.j-answer-delete', function(){
            if(confirm(o.lang.answer_delete)){
                bff.ajax(bff.ajaxURL('opinions', 'answer-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        listMngr.submit({popstate:true}, false);
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $filter.on('click', '.j-f-t', function(){
            var $el = $(this);
            $filter.find('[name="t"]').val($el.data('id'));
            popupClose($el);
            filterRefresh();
            listMngr.submit({}, true);
            return false;
        });

        $filter.on('click', '.j-f-tm', function(){
            var $el = $(this);
            $filter.find('[name="tm"]').val($el.data('id'));
            popupClose($el);
            filterRefresh();
            listMngr.submit({}, true);
            return false;
        });

        $filter.on('click', '.j-f-d', function(){
            var $el = $(this);
            $filter.find('[name="d"]').val($el.data('id'));
            popupClose($el);
            filterRefresh();
            listMngr.submit({}, true);
            return false;
        });

        var $counter = $block.find('.j-counter-all');
        listMngr = app.list($filter, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('counters')){
                    for(var t in resp.counters){
                        if(resp.counters.hasOwnProperty(t)){
                            $filter.find('.j-cnt-'+t).text('(' + resp.counters[t].cnt + ')');
                        }
                    }
                }
                if(resp.hasOwnProperty('counter')){
                    $counter.html(resp.counter);
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onAfterDeserialize: function() {
                filterRefresh();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function() {
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });
    }

    function popupClose($el)
    {
        var $p = $el.closest('li.open');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function filterRefresh()
    {
        // types
        var id = intval($filter.find('[name="t"]').val());
        var $t = $filter.find('.j-f-t');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == id){
                $(this).parent().addClass('active');
            }
        });
        if(types.hasOwnProperty(id)){
            $filter.find('.j-f-title').html(types[id]);
        }

        // terms
        id = intval($filter.find('[name="tm"]').val());
        $t = $filter.find('.j-f-tm');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == id){
                $(this).parent().addClass('active');
            }
        });
        if(terms.hasOwnProperty(id)){
            $filter.find('.j-tm-title').html(terms[id]);
        }

        // directions
        id = intval($filter.find('[name="d"]').val());
        $t = $filter.find('.j-f-d');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == id){
                $(this).parent().addClass('active');
            }
        });
        if(directions.hasOwnProperty(id)){
            $filter.find('.j-d-title').html(directions[id]);
        }
    }

    function updateOpinionsCache(cache)
    {
        $('#j-user-opinions-cache').html(cache);
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());