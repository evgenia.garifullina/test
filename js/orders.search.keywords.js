var jOrdersSearchKeywords = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;
    var $typesSelect, $specsSelect;
    function init()
    {
        $block = $('#j-orders-search-keywords');

        $form = $('#j-orders-search-keywords-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        // select типов заказа
        $typesSelect = $('#j-orders-search-type');
        $specsSelect = $('#j-orders-search-spec');

        // изменили выбранный тип заказа
        $typesSelect.on('change', function (e)
        {
            // <select> специализаций не доступен, пока не подгрузили <option> для выбранного типа
            $specsSelect.prop('disabled', true);
            var selectedTypeID = $(this).val();
            // выбран тип заказа из списка
            if (selectedTypeID != 0)
            {
                // обновим специализации, принадлежащие выбранному типу
                bff.ajax(
                    bff.ajaxURL('specializations', 'form-select'),
                    $form.serialize(),
                    function (resp, errors)
                    {
                        if (resp && resp.success)
                        {
                            $specsSelect.html(resp.options);
                            $specsSelect.prop('disabled', false);
                        }
                    }
                );
            }
            // не выбран тип, значение "ВСЕ"
            else
            {
                // сбросим выбранную специализацию
                $specsSelect.prop('selectedIndex', 0);
            }
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('count')){
                    $block.find('.j-orders-count').text(resp.count);
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function() {
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());

