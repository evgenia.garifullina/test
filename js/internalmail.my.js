var jMyMessages = (function(){
    var inited = false, o = {lang:{},ajax:true},
        $block, listMngr, $form, $list, $pgn,
        $folderVal;

    function init()
    {
        $block = $('#j-my-messages-block');

        $form = $('#j-my-messages-form-list');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pgn');
        $folderVal = $form.find('#j-my-messages-folder-value');

        var $actF = $('#j-my-messages-form-act');
        var f = app.form($actF, false, {noEnterSubmit: true});
        foldersInit($actF, f);

        var $m_actF = $('#j-my-messages-mobile-form-act');
        var mf = app.form($m_actF, false, {noEnterSubmit: true});
        foldersInit($m_actF, mf);

        app.$W.on('app-device-changed', function(e, device){
            location.reload();
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                //if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(typeof(jUsersNote) == 'object'){
                    jUsersNote.$init();
                }
                return false;
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onAfterDeserialize: function() {
                onFolderSelect();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
            $form.on('click', '.j-q-submit', function(e){
                nothing(e);
                listMngr.submit();
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $list.on('click', '.j-f-action', function(e){
            moveToFolder($(this), o.lang.move_folder);
            return false;
        });

        $('#setReadAll').on('click', function(e){
            bff.ajax(
                location.href,
                {act:'setReadAll', hash:app.csrf_token},
                function(data, errors) {
                    location.reload();
                });
            return false;
        });

        if(typeof(jUsersNote) == 'object'){
            jUsersNote.$init();
        }
    }

    function popupClose($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function moveToFolder($el, lang_move_to_folder)
    {
        var userID = intval($el.data('user-id'));
        var folderID = intval($el.data('folder-id'));
        if( ! userID || ! folderID ) return;
        bff.ajax(bff.ajaxURL('internalmail', 'move2folder&ev=my_messages'), {act:'move2folder', user:userID, folder:folderID, hash:app.csrf_token},
        function(data, errors){
            if(data && data.success) {
                if (intval(data.added)) {
                    $el.parent().addClass('active');
                } else {
                    $el.parent().removeClass('active');
                }
                popupClose($el);
                var $title = $el.closest('.dropdown').find('.j-f-title');
                var $folders = $el.closest('ul').find('li.active');
                if ($folders.length) {
                    var t = [];
                    $folders.each(function(){
                        t.push($(this).find('a').text());
                    });
                    $title.text(t.join(', '));
                } else {
                    $title.text(lang_move_to_folder);
                }
            } else {
                app.alert.error(errors);
            }
        });
    }

    function foldersInit($actF, f)
    {
        $actF.on('click', '.j-make-folder', function(){
            var name = f.field('new_folder_name').value;
            if( ! name.length){
                f.fieldError('new_folder_name', o.lang.set_new_folder_name);
                return false;
            }

            bff.ajax(document.location.href, {act:'folder-create', title:name, hash:app.csrf_token}, function(data, errors) {
                if (data && data.success) {
                    location.reload();
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $actF.on('click', '.j-edit-folder', function(){
            var $el = $(this);
            var id = $el.data('id');
            var $li = $el.closest('li');
            $li.addClass('hidden');
            $li.after('<li data-id="'+id+'"'+($li.hasClass('active') ? ' class="active"' : '')+'> <div class="input-group"> '+
                '<input type="text" name="f_name_'+id+'" maxlength="25" class="form-control input-sm" value="'+$li.find('.j-title').text()+'"> '+
                '<span class="input-group-btn"> '+
                    '<button class="btn btn-default btn-sm j-edit-folder-submit" type="button">'+o.lang.ok+'</button> '+
                '</span> '+
                '<span class="input-group-addon"> '+
                    '<a href="#" class="small ajax-link j-edit-folder-cancel"><span>'+o.lang.cancel+'</span></a> '+
                '</span> '+
                '</div>' +
            '</li>');
            return false;
        });

        $actF.on('click', '.j-edit-folder-cancel', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            $li.prev().removeClass('hidden');
            $li.remove();
            return false;
        });

        $actF.on('click', '.j-edit-folder-submit', function(){
            var $li = $(this).closest('li');
            var id = $li.data('id');
            var name = f.field('f_name_'+id).value;
            if( ! name.length){
                f.fieldError('f_name_'+id, o.lang.set_new_folder_name);
                return false;
            }
            bff.ajax(document.location.href, {act:'folder-rename', title:name, id:id, hash:app.csrf_token}, function(data, errors) {
                if (data && data.success) {
                    var $pr = $li.prev();
                    $li.remove();
                    $pr.find('.j-title').text(name);
                    $pr.removeClass('hidden');
                    listMngr.submit();
                } else {
                    app.alert.error(errors);
                }
            });

            return false;
        });

        $actF.on('click', '.j-delete-folder', function(){
            var $el = $(this);
            bff.ajax(document.location.href, {act:'folder-delete', id:$el.data('id'), hash:app.csrf_token}, function(data, errors) {
                if (data && data.success) {
                    var $li = $el.closest('li');
                    if($li.hasClass('active')){
                        $actF.find('.j-folder-select:first').trigger('click');
                    }else{
                        listMngr.submit();
                    }
                    $li.remove();
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $actF.on('click', '.j-folder-select', function(){
            var $el = $(this);
            $folderVal.val($el.data('id'));
            listMngr.submit({scroll:true}, true);
            onFolderSelect();
            return false;
        });
    }

    function onFolderSelect()
    {
        var id = $folderVal.val();
        $block.find('.j-folder-select').each(function(){
            var $el = $(this);
            if($el.data('id') == id){
                $el.parent().addClass('active');
                $el.find('.fa').removeClass('fa-folder').addClass('fa-folder-open');
            }else{
                $el.parent().removeClass('active');
                $el.find('.fa').removeClass('fa-folder-open').addClass('fa-folder');
            }
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        page: function(pageId)
        {
            listMngr.page(pageId);
            return false;
        },
        moveToFolder:moveToFolder
    };
}());

var jMyChat = (function(){
    var inited = [], o = {lang:{}, selectorChatList: '', recipientId: 0},
        listMngr;


    function renderMsg(obj,msg) {
        var _dt = new Date();
        var tpl = "<div class=\"i-message-box\">\n" +
            "        <div class=\"i-message-self\">\n" +
            "            <div class=\"i-message i-message-sending\">\n" +
            "                "+msg+"                            </div>\n" +
            "            <div class=\"i-message-time\">\n" +
            "                                <span>"+_dt.getHours()+":"+_dt.getMinutes()+"</span>            </div>\n" +
            "        </div>\n" +
            "    </div>";
        return $(obj).append(tpl).find('.i-message-box:last-child');
    }

    function smsFieldCount(form_app, countBlock) {
        form_app.$field('send_sms').change(function() {
            if(this.checked) {
                countBlock.removeClass('hidden');
            }else{
                countBlock.addClass('hidden');
            }
        });
    }



    function bindKeyPress($form, form_app, countBlock) {
        form_app.$field('message').on('keyup', function() {
            $('#j-count-message').html(this.value.length);

            var price = Math.ceil((this.value.length / o.wordCount)) * o.wordPrice;

            $('#j-price-message').html(price+' ₽');

        });

        smsFieldCount(form_app, countBlock);

        form_app.$field('message').keypress(function(e) {
            // ctrlKey + 13 - firefox
            // 10 - chrome
            if((e.ctrlKey && e.which == 13) || e.which == 10) {
                $form.submit();
            }
        });
    }

    function init()
    {
        // list
        var $list = $(o.selectorChatList);
        setTimeout(function(){
            $list.scrollTop( $list.get(0).scrollHeight + 100 );
        }, 1);

        // form
        var $form = $('#j-my-chat-form' + o.recipientId);
        console.log('****', '#j-my-chat-form' + o.recipientId, $form);
        var lastMsg;
        var f = app.form($form, false, {noEnterSubmit: true});
        var $f = f.getForm();
        var countBlock = $form.find("#j-count-message-block");
        bindKeyPress($form,f,countBlock);
        bff.iframeSubmit($f, function(resp, errors) {
            console.log('iframeSubmit', resp, errors);
            if(resp && resp.success) {
                if( f.fieldStr('act') === 'update') {
                    $form.find("input[name='act']").val('send');
                } else {
                    f.alertSuccess(o.lang.success, {reset: true});

                    if(!f.$field('send_sms').checked){
                        countBlock.addClass('hidden');
                    }
                }
                $form.find('.j-attachments .j-list').empty();
                if (resp['reload']) {
                    location.reload();
                }
                else if(resp.hasOwnProperty('message')) {
                    $list.append(resp.message);
                    $list.find('.alert').remove(); // ЧТо это?
                    if(lastMsg) lastMsg.remove();// REMOVE pre render
                    $list.scrollTo($list.find('.i-message-box:last'), {duration:300, offset:0});
                }

                if (resp['debugSms']) {
                    f.fieldsError(resp.fields, resp['debugSms']);
                }

            }
            else if(resp['fields']) {
                f.fieldsError(resp.fields, errors);
            }
        }, {
            beforeSubmit: function(ff) {
                var blockProgress = $form.find('.j-progress-loader');
                if (blockProgress.length && !blockProgress.hasClass("hidden")) {
                    $('.modal_load').css('display', 'block');
                    setInterval(function () {
                        if (blockProgress.hasClass("hidden")) {
                            clearInterval();
                            $('.modal_load').css('display', 'none');
                        }
                    }, 500);
                    return false;

                }
                if (f.fieldStr('act') !== 'update' && !$form.find('.j-attachments .j-list').html().length) {
                    if (f.fieldStr('message').length < 2 && !f.fieldStr('attach').length && (!f.field('order_complete') || !f.field('order_complete').checked)) {
                        f.fieldError('message', o.lang.message);
                        return false;
                    }
                }
                return true;
            }
        });

        // $('#j-my-chat-folders').on('click', '.j-f-action', function(e){
        //     jMyMessages.moveToFolder($(this), o.lang.move_folder);
        //     return false;
        // });
    }

    return {
        init: function(options)
        {
            o = $.extend(o, options || {});
            if(inited[o.recipientId]) return;
            inited[o.recipientId] = true;
            $(function(){ init(); });
        },
        page: function(pageId)
        {
            listMngr.page(pageId);
            return false;
        }
    };
}());