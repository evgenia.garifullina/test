var jMySettings = (function(){
    var inited = false, o = {lang:{}, url_settings:'', docsMaxSize:0 , docsAllowExt: []},
        geo = {$block:0,$regionBlocks:0,country:0,cityAC:0,cityID:0,cityPreSuggest:{},
               addr:{$block:0,map:0,mapEditor:0,lastQuery:'',inited:0}, metro:{data:{}}},
        $cont, avatarUploader;

    function init()
    {
        $cont = $('#j-my-settings');

        // avatar uploader
        avatarUploader = new qq.FileUploaderBasic({
            button: $cont.find('#j-my-avatar-upload').get(0),
            action: o.url_settings,
            params: {hash: app.csrf_token, act: 'avatar-upload'},
            uploaded: 0, multiple: false, sizeLimit: o.avatarMaxsize,
            allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                setAvatarImg(false, true);
            },
            onComplete: function(id, fileName, data) {
                if(data && data.success) {
                    setAvatarImg(data[o.avatarSzSmall]);
                } else {
                    if(data.errors) {
                        app.alert.error(data.errors, {title: o.lang.ava_upload});
                    }
                }
                return true;
            },
            messages: o.lang.ava_upload_messages,
            showMessage: function(message, code) {
                app.alert.error(message, {title: o.lang.ava_upload});
            }
        });

        $cont.on('click', '#j-my-avatar-delete', function(e){ nothing(e);
            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'avatar-delete'}, function(data,errors){
                if(data && data.success) {
                    setAvatarImg(data[o.avatarSzSmall]);
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            return false;
        });


        $cont.on('click', '.j-unsubscribe-telegram', function(e){ nothing(e);
            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'unsubscribe-telegram'}, function(data,errors){
                if(data && data.success) {
                    location.reload();
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            location.reload();
            return false;
        });

        $cont.on('click', '.j-unsubscribe-whatsapp', function(e){ nothing(e);

            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'unsubscribe-whatsapp'}, function(data,errors){
                if(data && data.success) {
                    location.reload();
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            location.reload();
            return false;
        });

        $cont.on('click', '.j-unsubscribe-private-whatsapp', function(e){ nothing(e);

            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'unsubscribe-private-whatsapp'}, function(data,errors){
                if(data && data.success) {
                    location.reload();
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            location.reload();
            return false;
        });

        // contacts
        contactsInit(o.contactsLimit, o.contactsData);

        // contacts form
        app.form($cont.find('.j-form-contacts'), function(){return false;}, {
            onInit: function($f){
                var appForm = this;
//                $f.on('click', '.j-submit', function() { // Нужна ли кнопка сохранить?
//                    if( ! appForm.checkRequired({focus:true}) ) {
//                        formSlideError(appForm.fieldFirstError);
//                        return false;
//                    }
//                    appForm.ajax(o.url_settings,{},function(data,errors){
//                        console.log('AJAX callback for j-form-contacts', data, errors);
//                        if(data && data.success) {
//
//                            appForm.alertSuccess(o.lang.saved_success);
//                            appForm.$field('name').val(data.name);
//                            if(data.redirect){
//                                bff.redirect(data.redirect);
//                            }
//                            formSlideSuccess();
//                        }
//                        else {
//                            appForm.fieldsError(data.fields, errors);
//                            formSlideError(appForm.fieldFirstError);
//                        }
//                    });
//                    return false;
//                });
                $('#j-settins-change').on('change', function() {
                    if( ! appForm.checkRequired({focus:true}) ) {
                        formSlideError(appForm.fieldFirstError);
                        return false;
                    }
                    appForm.ajax(o.url_settings,{},function(data,errors){
                        console.log('AJAX callback for j-form-contacts', data, errors);
                        if(data && data.success) {

                            appForm.alertSuccess(o.lang.saved_success); //Оставить или убрать?
                            appForm.$field('name').val(data.name);
                            if(data.redirect){
                                bff.redirect(data.redirect);
                            }
                            formSlideSuccess();
                        }
                        else {
                            appForm.fieldsError(data.fields, errors);
                            formSlideError(appForm.fieldFirstError);
                        }
                    });
                    return false;
                });
                if (o.form_contacts_require_check) {
                    setTimeout(function(){
                        appForm.checkRequired({focus:true, noAlert: true, scroll: false});
                    }, 500);

                }
            }
        });

        // social form
        socialButtonsInit();

        // enotify form
        app.form($cont.find('.j-form-enotify'), function($f){
            var appForm = this;
            appForm.ajax(o.url_settings,{},function(resp,errors){
                if(resp && resp.success) {
                    appForm.alertSuccess(o.lang.saved_success);
                } else {
                    appForm.alertError(errors);
                }
            });
        }, {noEnterSubmit: true});

        // tabs form
        app.form($cont.find('.j-form-profile-tabs'), function($f){
            var f = this;
            f.ajax(o.url_settings,{},function(resp,errors){
                if(resp && resp.success) {
                    f.alertSuccess(o.lang.saved_success);
                } else {
                    f.alertError(errors);
                }
            });
        }, {noEnterSubmit: true});

        // pass form
        app.form($cont.find('.j-form-pass'), function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            if( f.fieldStr('pass1') != f.fieldStr('pass2') ) {
                f.fieldsError(['pass2'], o.lang.pass_confirm);
                return;
            }
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.pass_changed, {reset:true});
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // email form
        app.form($cont.find('.j-form-email'), function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            if( ! bff.isEmail( f.fieldStr('email') ) ) {
                f.fieldsError(['email'], o.lang.email_wrong);
                return;
            }
            if( f.fieldStr('email') === f.fieldStr('email0') ) {
                f.fieldsError(['email'], o.lang.email_diff);
                return;
            }
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.email_changed, {reset:true});
                    f.$field('email0').val( data.email );
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // destroy form
        app.form($cont.find('.j-form-destroy'), function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.account_destoyed, {reset:true});
                    setTimeout(function(){
                        bff.redirect(data.redirect);
                    }, 1500);
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // Multiple Value
        // TODO : сделать универсальным
        checkMultipleField();
        $('.plusBtn').click(function(){
            var copyObj = $(this).parent().find('.multipleField').last();
            console.log('copyObj: ', copyObj);
            copyObj.clone().insertAfter(copyObj);
            copyObj.next().find('input').val('');
            checkMultipleField();
        });
        $('.minusBtn').click(function(){
            $(this).parent().find('.multipleField').last().remove();
            checkMultipleField();
        });
        function checkMultipleField() {
            var len = $('.plusBtn').parent().find('.multipleField').length;
            if (!len) return;
            if ( len >= parseInt($('.plusBtn').attr('data-max'))) {
                $('.plusBtn').hide();
            }
            else {
                $('.plusBtn').show();
            }
            if ( len <=1) {
                $('.minusBtn').hide();
            }
            else {
                $('.minusBtn').show();
            }
        }


        $('#send-test-push').on('click', function() {
            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'push_test'}, function(data,errors) {
                console.log(data,errors);

                if(data && data.success) {
                    app.alert.success(data.alert);
                    if (data.count !== data.countSend) {
                        $('#check-test-push').show();
                    }
                } else if(errors) {
                    app.alert.error(errors);
                    $('#check-test-push').show();
                }
            });
            return false;
        });

        $('#check-test-push').on('click', function() {
            fireEvent('pushRequestSubscribe');
            return false;
        });

        attachDocs();

        initFormSlide();
    }

    function formSlideSuccess() {
        var $slideForm = $('.activeSlideForm .slideForm');
        if ($slideForm.length && $slideForm.find('.slideFormElementFinish').length) {
            var tt = $slideForm.find('.slideFormSection');
            tt.hide();
            tt.filter('.slideFormElementFinish').show();
            $('.slideFormBtnFinish, .slideFormBtnNext, .slideFormBtnPrev').hide();
            //$slideForm.scrollLeft(0);
        }
    }

    function formSlideError(fieldFirstError) {
        var $slide = $('.activeSlideForm');
        if ($slide.length) {
            var $slideForm = $slide.find('.slideForm');
            var $slides = $slideForm.find('.slideFormSection:not(.slideFormElementFinish)');
            var maxSlide = $slides.length;
            //var widthSlide = $slideForm.find('.slideFormSection:first').width();
            $slides.each(function( i ) {
                if ($( this ).find('.has-error').length) {
                    console.log('Err',  i, $( this ).find('.has-error'));
                    $slide.removeClass('lastSlide');
                    $slide.removeClass('firstSlide');
                    if (i==0) {
                        $slide.addClass('firstSlide');
                    }
                    else if (i == maxSlide-1 ){
                        $slide.addClass('lastSlide');
                    }
                    $slideForm.data('slideCnt', i-1 );

                    $slides.hide();
                    $slides.eq(i).show();
                    //$slideForm.scrollLeft(i * widthSlide);
                    return false;
                }
            });

        }
    }

    function initFormSlide() {
        var $slide = $('.activeSlideForm');
        var appForm = $slide.find('form').data('appForm');
        if ($slide.length) {
            var $slideForm = $slide.find('.slideForm');
            $slideForm.data('slideCnt', 1);
            var $slides = $slideForm.find('.slideFormSection:not(.slideFormElementFinish)');
            var maxSlide = $slides.length;
            //console.log($slides, 'maxSlide' , maxSlide);
            //var widthSlide = $slideForm.find('.slideFormSection:first').width();

            $($slide).on('click', '.slideFormBtnNext', function() {
                var currentSlide = $slideForm.data('slideCnt');
                //console.log('currentSlide', currentSlide );
                if (currentSlide >= maxSlide) { //  || $slideForm.data('animateSlide')===true
                    //console.log('click false');
                    return;
                }
                else {
                    // error check
                    var ee = appForm.checkRequired({focus:true, noAlert: true, searchIn: $slides.eq( currentSlide-1 )});
                    if (!ee) {
                        return;
                    }

                    if (currentSlide == 1) {
                        $slide.removeClass('firstSlide');
                    }
                    if (currentSlide == (maxSlide-1)) {
                        $slide.addClass('lastSlide');
                    }
                    //$slideForm.data('animateSlide', true);
                    //$( $slideForm ).animate({
                    //    scrollLeft: currentSlide * widthSlide
                    //}, 500, function() {
                    //    $slideForm.data('animateSlide', false);
                    //});
                    $slides.hide();
                    $slides.eq( currentSlide ).show();
                    $slideForm.data('slideCnt', currentSlide + 1 );
                    //console.log('next slide' , currentSlide + 1, $slides.eq( currentSlide ));
                }
            });

            $($slide).on('click', '.slideFormBtnPrev', function() {

                var currentSlide = $slideForm.data('slideCnt');
                //console.log('currentSlide', currentSlide );
                if (currentSlide <=1) { //  || $slideForm.data('animateSlide')===true
                    //console.log('click false');
                    return;
                }
                else {
                    if (currentSlide == maxSlide) {
                        $slide.removeClass('lastSlide');
                    }
                    if (currentSlide == 2) {
                        $slide.addClass('firstSlide');
                    }
                    //$slideForm.data('animateSlide', true);
                    //$( $slideForm ).animate({
                    //    scrollLeft: (currentSlide-2) * widthSlide
                    //}, 500, function() {
                    //    $slideForm.data('animateSlide', false);
                    //});
                    $slides.hide();
                    $slides.eq((currentSlide-2) ).show();
                    $slideForm.data('slideCnt', currentSlide - 1 );
                    //console.log('prev slide' , currentSlide - 1, $slides.eq( (currentSlide-2) ));
                }
            });

            //$( $slideForm ).scroll(function(){
            //
            //});
        }
    }
    function attachDocs() {
        // attach file
        avatarUploader = new qq.FileUploaderBasic({
            button: $cont.find('#j-my-docs-upload').get(0),
            action: o.url_settings,
            params: {hash: app.csrf_token, act: 'docs-upload'},
            uploaded: 0, multiple: true, sizeLimit: o.docsMaxSize,
            allowedExtensions: o.docsAllowExt,
            onSubmit: function(id, fileName) {
                setFileDocs(false, true);
            },
            onComplete: function(id, fileName, data) {
                console.log(data, o.avatarSzSmall);
                if(data && data.success) {
                    setFileDocs(data['fileHtml'], false);
                } else {
                    setFileDocs(false, false);
                    if(data.errors) {
                        app.alert.error(data.errors, {title: o.lang.ava_upload});
                    }
                }
                return true;
            },
            messages: o.lang.ava_upload_messages,
            showMessage: function(message, code) {
                app.alert.error(message, {title: o.lang.ava_upload});
            }
        });

        $cont.on('click', '#j-my-docs-delete', function(e){ nothing(e);
            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'docs-delete'}, function(data,errors){
                if(data && data.success) {
                    setFileDocs(false, false);
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            return false;
        });
    }

    function setFileDocs(fileHtml, bProgress)
    {
        var $obj = $cont.find('#j-my-docs-file');

        if(bProgress){
            $obj.parent().addClass('hidden').after(o.avatarUploadProgress);
        }else{
            $obj.parent().removeClass('hidden').next('.j-progress').remove();
        }

        if( fileHtml ) {
            $obj.html(fileHtml);
        } else {
            $obj.html('');
        }
    }

    function setAvatarImg(img, bProgress)
    {
        var $img = $cont.find('#j-my-avatar-img');
        if(bProgress){
            $img.parent().addClass('hidden').after(o.avatarUploadProgress);
        }else{
            $img.parent().removeClass('hidden').next('.j-progress').remove();
        }

        if( img ) {
            $img.attr('src', img);
        } else {
            //
        }
    }

    function contactsInit(limit, contacts)
    {
        var index  = 0, total = 0;
        var $block = $cont.find('#j-my-contacts');
        var $add = $block.find('.j-add');

        function add(contact)
        {
            contact = contact || {};
            if(limit>0 && total>=limit) return;
            index++; total++;
            var value = '';
            if(contact.hasOwnProperty('v') && contact.v) value = contact.v;
            $add.before('<div class="row j-contact">'+
                            '<div class="col-sm-3">'+
                                '<div class="form-group"><select type="text" class="form-control j-type" name="contacts['+index+'][t]">'+o.contactsTypesOptions+'</select></div>'+
                            '</div>'+
                            '<div class="col-xs-8"><div class="form-group">'+
                                '<input type="text" class="form-control j-value" maxlength="40" name="contacts['+index+'][v]" value="'+value.replace(/"/g, "&quot;")+'">'+
                            '</div></div>'+
                            '<div class="col-xs-1 p-profileCabinet-contacts-delete">'+
                                '<a href="#" class="p-delete j-remove"><i class="fa fa-trash-o"></i></a>'+
                            '</div></div>');

            if(contact.hasOwnProperty('t')){
                $block.find('.j-contact:last').find('.j-type').val(contact.t);
            }
            if(limit>0 && total>=limit) {
                $add.hide();
            }
        }

        $block.on('click', '.j-remove', function(e){ nothing(e);
            var $contact = $(this).closest('.j-contact');
            if( $contact.find('.j-value').val() != '' ) {
                if(confirm('Удалить контакт?')) {
                    $contact.remove(); total--;
                }
            } else {
                $contact.remove(); total--;
            }
            if(limit>0 && total<limit) {
                $add.show();
            }
        });

        $block.on('click', '.j-add', function(e){ nothing(e);
            add();
            return false;
        });

        contacts = contacts || {};
        for(var i in contacts) {
            if( contacts.hasOwnProperty(i) ) {
                add(contacts[i]);
            }
        }
        if( ! total ) {
            add();
        }

    }

    function socialButtonsInit()
    {
        var popup;
        $cont.on('click', '.j-my-social-btn', function(e){ nothing(e);
            var $btn = $(this);
            var m = $btn.metadata(); if( ! m || ! m.hasOwnProperty('provider') ) return;
            if( ! $btn.hasClass('not-active') ) {
                // Ошибки в логике при отвязке соц аккаунта, пока отключили
                // bff.ajax(o.url_settings, {act:'social-unlink',provider:m.provider,hash:app.csrf_token}, function(resp, errors){
                //     if( resp && resp.success ) {
                //         $btn.addClass('not-active');
                //         $btn.find('i.fa').remove();
                //     } else {
                //         app.alert.error(errors);
                //     }
                // });
                return;
            }
            m = $.extend({w: 450, h: 380}, m || {});
            if(popup !== undefined) popup.close();
            popup = window.open(o.url_social + m.provider+'?ret='+encodeURIComponent(o.url_settings + '?tab=access'), "u_login_social_popup",
                "width=" + m.w + "," +
                "height=" + m.h + "," +
                "left=" + ( (app.$W.width() - m.w) / 2 ) + "," +
                "top=" + ( (app.$W.height() - m.h) / 2 ) + "," +
                "resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes");
            popup.focus();
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());