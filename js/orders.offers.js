


var jOrdersOfferEdit = (function(){
    var inited = false, o = {type:0, fluidCommissionConfig: null, lang:{}};
	


    function init()
    {
        var $chatBlock = $('#j-offers .o-freelancer-info');
        var $block = $('#j-offer-descr');
        var $blf = $('#j-offer-edit-form-block');

        var form = app.form($blf.find('form'), function($f){
            var f = this;
            f.ajax(bff.ajaxURL('orders&ev=offerEditFormAjax', 'descr_price'),{},function(resp, errors){
                if(resp && resp.success) {
                    console.log(resp);
                    if(resp.descr){
                        $block.find('.o-proposal-descr').html(resp.descr);
                    }
                    if(resp.price){
                        $chatBlock.find('.o-freelancer-info__top .offer-price').html(resp.price);
                    }
                    if(resp.priceFull){
                        $chatBlock.find('.o-freelancer-info__bottom .offer-price').html(resp.priceFull);
                    }
                    $blf.hide();
                    $block.show();
                    $('#j-offer-edit').show();
                } else {
                    f.alertError(errors);
                }
            });
        });

        $('#j-offer-edit').on('click', function(){
            $block.hide();
            $blf.show();
            $(this).hide();
            return false;
        });

        $blf.on('click', '.j-cancel', function(){
            $blf.hide();
            $block.show();
            $('#j-offer-edit').show();
            return false;
        });

        if (o.fluidCommissionConfig) {
            $('#j-price').on('keyup change', function () {
                var v = intval($(this).val());
                var mx = intval($(this).attr('max'));
                if (v > mx) {
                    v = mx;
                }
                $(this).val(v);
                var c = 0;
                for (var k in o.fluidCommissionConfig) {
                    if (k > v) {
                        break;
                    }
                    c = intval(o.fluidCommissionConfig[k]);
                }
                v = (v + v * (c / 100));
                if (v > 1000)
                    v = Math.ceil( v / 100 ) * 100;
                else
                    v = Math.ceil( v / 50 ) * 50;
                $('#j-full-price').val(v);
            });
            //$('#j-full-price').on('keyup change', function () {
            //    var v = intval($(this).val());
            //    var c = 0;
            //    for (var k in o.fluidCommissionConfig) {
            //        if (k > v) {
            //            break;
            //        }
            //        c = intval(o.fluidCommissionConfig[k]);
            //    }
            //    console.log(v, c, ( (v * 100) / (c + 100)));
            //    v = Math.round( (v * 100) / (c + 100));
            //    $('#j-price').val(v);
            //});
            $('#j-price').change();
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());


var jOrdersOfferDelete = (function(){
    var inited = false, o = {type:0, fluidCommissionConfig: null, lang:{}};

    function init()
    {
		
        $(document).on("click", ".j-delete-offer-func", function() {
			$('.modal_load').css('display','block');
			var id = $(this).attr('id');
			del(id);
        });
    }
	
	function del(IDoffer)
    {
		var id = $(this).attr('id');
		var f = this;
		bff.ajax(bff.ajaxURL('orders&ev=offerdelete', 'delete_offer'),{id: IDoffer},function(resp, errors){
			if(resp && resp.success) {
				console.log(resp);
				$('.modal_load').css('display','none'); 
				location.reload();
			} else {
				console.log(errors);
			}
		});
	}
	
    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());

var jOrdersOfferChat = (function () {
    var inited = false, o = {type: 0, lang: {}, id: 0};

    function init() {
        $(document).on('click', '.btn-open-chat', function(ee) {
            var obj = $('.o-chosen-worker .j-open-chat, .my-proposal .j-open-chat');
            var chatObj = $(obj.attr('href'));
            if (!chatObj.hasClass('in')) {
                obj.trigger('click');
                scrollToForm(chatObj);
            }
            else {
                scrollToForm(chatObj);
            }

            if ($(this).hasClass('openFiles')) {
                setTimeout(function() {
                    console.log('+4');
                    $('.my-proposal .j-upload-file').click();
                }, 990);
            }

        });

        $(document).on('click', '.j-open-chat', function(){
            openChat($(this));
            return true;
        });
        //var chatWindows = $('.o-proposal-chat-window');
        var chatWindows = '.o-proposal-chat-window';
        $(document).on('show.bs.collapse', chatWindows, function (e) {
            var blockObj = $(this).parent();
            blockObj.find('.j-open-chat-response').addClass('hidden');
            blockObj.find('.j-open-chat-title').text(o.lang.chat_title_hide);
        });
        $(document).on('hide.bs.collapse', chatWindows, function (e) {
            var blockObj = $(this).parent();
            blockObj.find('.j-open-chat-response').removeClass('hidden');
            blockObj.find('.j-open-chat-title').text(o.lang.chat_title_show);
            window.location.hash = '##';
        });
        $(document).on('shown.bs.collapse', chatWindows, function (e) {
            var thisObj = $(this);
            if (thisObj.hasClass('r')) {
                thisObj.removeClass('r');
                thisObj.find('[name="message"]').focus();
            }
        });

        o.id = intval(o.id);
        if (o.id) {
            $('.j-open-chat-response').each(function () {
                var $el = $(this);
                if (intval($el.data('id')) == o.id) {
                    $el.trigger('click');
                }
            });

        }

        // TODO - перенос в ONLOAD, чтоб для всех работало
        if (window.location.hash && strpos(window.location.hash, '#o-proposal-chat-window-')>=0) {
            $(document).find('a[href="'+window.location.hash+'"]:first').trigger('click');
        }

    }

    function scrollToForm(obj) {
        setTimeout(function() {
            var elementClick = obj.attr('href')+' #j-attachments-internalmail';
            if ($(elementClick).length) {
                var destination = $(elementClick).position().top - 400;
                $('html, body').animate({scrollTop: destination}, 900);
            }
        }, 390);
    }

    function openChat(obj) {

        if(history.pushState) {
            history.pushState(null, null, obj.attr('href'));
        }
        else {
            window.location.hash = obj.attr('href');
        }

        var $bl = $(obj.attr('href'));
        if ($bl.length) {
            if (!$bl.hasClass('i') && !$bl.hasClass('in')) {
                //$bl.addClass('i'); // закоментил, чтобы чат подгружался каждый раз при открытии

                bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'chat'), {
                    id: $bl.data('id'),
                    hash: app.csrf_token
                }, function (resp, errors) {
                    if (resp && resp.success) {
                        var listObj = $bl.find('.j-chat-list');
                        listObj.html(resp.list);
                        // listObj.scrollTop(listObj.get(0).scrollHeight + 100);
                        if (!obj.hasClass('collapsed')) {
                            scrollToForm(obj);
                        }
                        if (!$bl.data('init-form')) {
                            var chatFormObj = $bl.find('.j-chat-form');
                            chatFormObj.html(resp.form);
                            $bl.data('init-form', true);
                        }
                    }
                    else {
                        app.alert.error(errors);
                    }

                });
                $bl.parent().find('.o-proposal-descr').hide();
            }
            else {
                $bl.parent().find('.o-proposal-descr').show();
            }
            if (obj.hasClass('j-open-chat-response')) {
                $bl.addClass('r');
            }
        }
    }

    return {
        init: function (options) {
            if (inited) return;
            inited = true;
            o = $.extend(o, options || {});
            $(function () {
                init();
            });
        }
    };
}());


var jOrdersOffersPerformer = (function(){
    var inited = false, o = {type:0, lang:{}};
    var $block;
    var st = {};
    var li_classes = {}, la_classes = {}, la_title = {};

    function init()
    {
        for(var s in o.st){
            if(o.st.hasOwnProperty(s)){
                st[s] = intval(o.st[s]);
            }
        }
        for(var s in o.statuses){
            if(o.statuses.hasOwnProperty(s)){
                if(o.statuses[s].hasOwnProperty('cli')){
                    li_classes[s] = o.statuses[s].cli;
                }
                if(o.statuses[s].hasOwnProperty('cla')){
                    la_classes[s] = o.statuses[s].cla;
                }
                if(o.statuses[s].hasOwnProperty('tl')){
                    la_title[s] = o.statuses[s].tl;
                }
            }
        }

        $block = $('#j-offers');
        $block.on('click', '.j-status', function(){
            var $el = $(this);
            var s = intval($el.data('s'));

            if(!confirm("Вы отказывайте данному эксперту?"))
            {
                return false;
            }

            bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'status'), {id:$el.data('id'), status: s, hash:app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    //$el.addClass('active').siblings().removeClass('active');
                    //var $bl = $el.closest('.j-offer');
                    //setClassLI($bl, s);
                    //setClassLa($bl.find('.j-label'), s);
                    //if(s == st.performer){
                    //    $block.find('.j-status-' + st.performer).prop('disabled', true);
                    //    $bl.find('.j-status-' + st.performer).prop('disabled', false);
                    //}else{
                    //    if( ! $block.find('.active.j-status-' + st.performer).length){
                    //        $block.find('.j-status-' + st.performer).prop('disabled', false);
                    //    }
                    //}
                    app.alert.success('Вы отказали эксперту - совершен возврат средств. Страница будет перезагружена через 3 сек.');
                    setTimeout(function(){ location.reload(); }, 3000);
                } else {
                    app.alert.error(errors);
                }
            });
        });

    }

    function setClassLI($bl, s)
    {
        for(var i in li_classes){
            if(li_classes.hasOwnProperty(i)){
                $bl.removeClass(li_classes[i]);
            }
        }
        if(li_classes.hasOwnProperty(s)){
            $bl.addClass(li_classes[s]);
        }
    }

    function setClassLa($bl, s)
    {
        for(var i in la_classes){
            if(la_classes.hasOwnProperty(i)){
                $bl.removeClass(la_classes[i]);
            }
        }
        if(la_classes.hasOwnProperty(s)){
            $bl.addClass(la_classes[s]);
        }
        if(la_title.hasOwnProperty(s)){
            $bl.text(la_title[s]);
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());