var jArticlesView = (function(){
    var inited = false, o = {lang:{}};
    var $block, $comments;

    function init()
    {
        $block = $('#j-article-view');
        $comments = $('#j-comments');

        o.id = intval(o.id);
        if(o.owner){
            $block.on('click', '.j-hide', function(){
                bff.ajax(bff.ajaxURL('articles', 'article-hide'), {id:o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $block.find('.j-hide').addClass('hidden');
                        $block.find('.j-show').removeClass('hidden');
                        $block.find('.j-fa-hide').removeClass('hidden');
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

            $block.on('click', '.j-show', function(){
                bff.ajax(bff.ajaxURL('articles', 'article-show'), {id:o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $block.find('.j-show').addClass('hidden');
                        $block.find('.j-hide').removeClass('hidden');
                        $block.find('.j-fa-hide').addClass('hidden');
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

            $block.on('click', '.j-delete', function(){
                if(confirm(o.lang.delete)){
                    bff.ajax(bff.ajaxURL('articles', 'article-delete'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            bff.redirect(resp.redirect);
                        } else {
                            app.alert.error(errors);
                        }
                    });
                }
                return false;
            });
        }

        $comments.on('click', '.j-add-comment', function(){
            var $el = $(this);
            var $form = $el.closest('.j-comment').find('.j-add-comment-form');
            $el.closest('.j-add-title').addClass('hidden');

            if( ! $form.hasClass('i')){
                $form.addClass('i');

                app.form($form, function(){
                    var f = this;
                    if( ! f.checkRequired({focus:true}) ) return;

                    f.ajax(bff.ajaxURL('articles', 'comment-add'),{},function(resp, errors){
                        if(resp && resp.success) {
                            f.$field('message').val('');
                            if(resp.html){
                                var $par = $el.closest('.j-comment-block');
                                var scroll = false;
                                if( ! $par.length){
                                    $par = $comments.find('.l-comments-list');
                                    scroll = true;
                                }
                                $par.append(resp.html);
                                if(scroll){
                                    $.scrollTo($par.find('.j-comment-block:last'), {duration:300, offset:0});
                                }
                                $el.closest('.j-comment').find('.j-cancel').trigger('click');
                            }
                        } else {
                            f.alertError(errors);
                        }
                    });
                }, {noEnterSubmit: true});
            }
        });

        $comments.on('click', '.j-cancel', function(){
            var $com = $(this).closest('.j-comment');
            var $collapse = $com.find('.collapse');
            /* $collapse.one('hidden.bs.collapse', function () { }); */
            $com.find('.j-add-title').removeClass('hidden');
            $collapse.collapse('hide');
            return false;
        });

        $comments.on('click', '.j-comment-delete', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('articles', 'comment-delete'),{id:$el.data('id'), article_id: o.id, hash: app.csrf_token},function(resp, errors) {
                if (resp && resp.success) {
                    var $bl = $el.closest('.j-comment-block');
                    $bl.after(resp.html);
                    $bl.remove();
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $comments.on('click', '.j-comment-restore', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('articles', 'comment-restore'),{id:$el.data('id'), article_id: o.id, hash: app.csrf_token},function(resp, errors) {
                if (resp && resp.success) {
                    var $bl = $el.closest('.j-comment-block');
                    $bl.after(resp.html);
                    $bl.remove();
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $block.on('click', '.j-vote', function(e){ nothing(e);
            var $el = $(this), $bl = $el.closest('.j-vote-block');
            if ($bl.hasClass('q-answer-rating_na')) return false;
            bff.ajax(bff.ajaxURL('articles', 'vote'), {id:$el.data('id'), t:$el.data('t'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.disabled){
                        $bl.addClass('q-answer-rating_na');
                    }
                    if(resp.sum){
                        var $v = $bl.find('.j-votes');
                        $v.html(resp.sum);
                        if(intval(resp.sum) < 0){
                            $v.removeClass('q-answer-good').addClass('q-answer-bad');
                        }else{
                            $v.removeClass('q-answer-bad').addClass('q-answer-good');
                        }
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());