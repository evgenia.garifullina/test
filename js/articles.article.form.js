var jArticlesArticleForm = (function(){
    var inited = false, o = {lang:{}, itemID:0};
    var img = {url:'',$block:0,type:{},$togglers:0,uploader:0,active:{}, classes:{progress:'j-progress'}};

    var f, $f;

    function init()
    {
        o.typeID = intval(o.typeID);
        //submit
        $f = $('#j-article-form');
        f = app.form($f, false, {noEnterSubmit: true});
        bff.iframeSubmit($f, function(resp, errors){
            if(resp && resp.success) {
                if(resp.redirect){
                    bff.redirect( $('<div/>').html(resp.redirect).text() );
                }else{
                    f.alertSuccess(o.lang.saved_success);
                }
            } else {
                f.fieldsError(resp.fields, errors);
            }
        },{
            beforeSubmit: function(){
                if( ! f.checkRequired({focus:true}) ) return false;
                return true;
            },
            button: '.j-submit'
        });

        $f.on('click', '.j-submit', function(){
            $f.submit();
            return false;
        });

        // cancel
        $f.on('click', '.j-cancel', function(){
            history.back();
            return false;
        });

        // images
        img.url = bff.ajaxURL('articles&ev=img&hash='+app.csrf_token+'&article_id='+o.itemID, '');
        img.$block = $f.find('.j-images');
        img.type = {
            $ajax: img.$block.find('.j-images-type-ajax'),
            $simple: img.$block.find('.j-images-type-simple')
        };
        img.$togglers = img.$block.find('.j-images-type');
        img.$togglers.on('click', '.j-images-toggler', function(e){ nothing(e);
            var type = $(this).data('type');
            img.$togglers.addClass('hide');
            img.$togglers.filter('.j-images-type-' + type).removeClass('hide');
            img.$block.find('.j-images-type-value').val(type);
        });

        img.uploader = new qq.FileUploaderBasic({
            button: null,
            action: img.url+'upload',
            limit: intval(o.imgLimit), sizeLimit: o.imgMaxSize,
            uploaded: intval(o.imgUploaded),
            multiple: true, allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                return imgProgress(id, 'start', false);
            },
            onComplete: function(id, fileName, resp) {
                if(resp && resp.success) {
                    imgProgress(id, 'preview', resp);
                    imgRotate(true);
                } else {
                    if(resp.errors) {
                        app.alert.error(resp.errors);
                        imgProgress(id, 'remove');
                    }
                }
                return true;
            },
            onCancel: function(id, fileName) {
                imgProgress(id, 'remove');
            },
            showMessage: function(message, code) {
                app.alert.error(message);
            },
            messages: {
                typeError: o.lang.upload_typeError,
                sizeError: o.lang.upload_sizeError,
                minSizeError: o.lang.upload_minSizeError,
                emptyError: o.lang.upload_emptyError,
                limitError: o.lang.upload_limitError,
                onLeave: o.lang.upload_onLeave
            }
        });
        img.type.$ajax.find('.j-img-upload .j-img-link').each(function(){
            img.uploader._createUploadButton(this);
        });

        for(var i in o.imgData) {
            if( o.imgData.hasOwnProperty(i) ) {
                imgProgress('img'+i, 'start', true);
                imgProgress('img'+i, 'preview', o.imgData[i]);
            }
        }
        imgRotate();

        img.type.$ajax.on('click', '.j-img-delete', function(e){ nothing(e);
            imgRemove( $(this).data('id') );
        });

        var unloadProcessed = false;
        app.$W.bind('beforeunload', function(){
            if( ! unloadProcessed && intval(o.itemID) === 0) {
                unloadProcessed = true;
                var fn = [];
                for(var i in img.active) {
                    if( img.active.hasOwnProperty(i) && img.active[i].data!==false ) {
                        var data = img.active[i].data;
                        if( data.tmp ) {
                            fn.push( data.filename );
                        }
                    }
                }
                if( fn.length ) {
                    bff.ajax(img.url+'delete-tmp', {filenames:fn}, false, false, {async:false});
                }
            }
        });

    }

    function imgProgress(id, step, data)
    {
        var $slot, $preview;
        switch(step)
        {
            case 'start': {
                $slot = img.type.$ajax.find('.j-img-slot:not(.j-active) .j-img-upload:visible:first').closest('.j-img-slot');
                if( $slot.length ) {
                    img.active[id] = {slot:$slot,data:false};
                    $slot.addClass('j-active'+(data === false ? ' '+img.classes.progress : ''));
                    $slot.find('.j-img-img').attr('src', app.rootStatic+'/img/loader.gif');
                    $slot.find('.j-img-preview').removeClass('hidden');
                    $slot.find('.j-img-upload').addClass('hidden');
                }else{
                    return false;
                }
            } break;
            case 'preview': {
                if( img.active.hasOwnProperty(id) ) {
                    $slot = img.active[id].slot;
                    img.active[id].data = data;
                    $slot.removeClass(img.classes.progress)
                    $slot.find('.j-img-img').attr('src', data.i);
                    $slot.find('.j-img-fn').val(data.filename).attr('name','images['+data.id+']');
                    $slot.find('.j-img-delete').data('id', id);
                }
            } break;
            case 'remove': {
                imgRemove(id);
            } break;
        }
        return true;
    }

    function imgRotate(update)
    {
        var $slots = img.type.$ajax.find('.j-img-slots');
        if(update === true) {
            $slots.sortable('refresh');
        } else {
            $slots.sortable({
                items: '.j-active'
            });
        }
    }

    function imgRemove(id)
    {
        if( img.active.hasOwnProperty(id) )
        {
            var $slot = img.active[id].slot;
            var data = img.active[id].data;
            var clearSlot = function(id, $slot) {
                img.type.$ajax.find('.j-img-upload:visible:last').closest('.j-img-slot').after($slot);
                $slot.find('.j-img-img').attr('src', '');
                $slot.find('.j-img-fn').val('').attr('name','');
                $slot.find('.j-img-preview').addClass('hidden');
                $slot.find('.j-img-upload').removeClass('hidden');
                $slot.removeClass('j-active');

                img.uploader.decrementUploaded();
                delete img.active[id];
                imgRotate(true);
            };
            if( data !== false ) {
                bff.ajax(img.url+'delete',
                    {image_id:data.id, filename:data.filename}, function(resp){
                        if(resp && resp.success) {
                            clearSlot(id, $slot);
                        } else {
                            app.alert.error(resp.errors);
                        }
                    });
            } else {
                clearSlot(id, $slot);
            }
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());