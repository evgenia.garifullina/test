var jOrdersOfferAddForm = (function(){
    var inited = false, o = {itemID:0, lang:{}, examplesCnt:0, examplesLimit:0, fluidCommissionConfig: null};
    var img = {uploader:0};
    var $form, $examples, $progress, $popup;

    function init()
    {
        $form = $('#j-offer-add-form');
        $examples = $('#j-offer-examples');
        $popup = $('#j-examples-popup');

        $progress = $form.find('.j-progress');

        o.url = bff.ajaxURL('orders&ev=offerAdd&hash='+app.csrf_token, '');

        // submit
        app.form($form, function(){
                var f = this;
                if( ! f.checkRequired({focus:true}) ) return;
                f.ajax(o.url+'add',{},function(data,errors){

                    if(data && data['needPhoneConfirm']) {
                        app.openPopupSettingPhone(function() {
                            $form.submit();
                        });
                    }
                    else if(data && data.success) {
                        f.alertSuccess(o.lang.saved_success);
                        yaSetGoal('offerAdd', function () {
                            if(data.reload){
                                location.reload();
                            }
                        });
                    } else {
                        f.fieldsError(data.fields, errors);
                    }
                });
            },
            {noEnterSubmit: true}
        );

        // cancel
        $form.on('click', '.j-cancel', function(){
            $('#offer-add').slideUp();
            $('#j-collapse-add').show(500);
            return false;
        });
        $('#j-collapse-add').on('click', function(){
            $('#offer-add').slideDown();
            $(this).hide(500);
            return false;
        });

        if (o.fluidCommissionConfig) {
            $('#j-price').on('keyup change', function () {
                var v = intval($(this).val());
                var mx = intval($(this).attr('max'));
                if (v > mx) {
                    v = mx;
                }
                $(this).val(v);
                var c = 0;
                for (var k in o.fluidCommissionConfig) {
                    if (k > v) {
                        break;
                    }
                    c = intval(o.fluidCommissionConfig[k]);
                }
				v = (v + v * (c / 100));
				if (v > 1000)
					v = Math.ceil( v / 100 ) * 100;
				else
					v = Math.ceil( v / 50 ) * 50;
				$('#j-full-price').val(v);
            });
            //$('#j-full-price').on('keyup change', function () {
            //    var v = intval($(this).val());
            //    var c = 0;
            //    for (var k in o.fluidCommissionConfig) {
            //        if (k > v) {
            //            break;
            //        }
            //        c = intval(o.fluidCommissionConfig[k]);
            //    }
            //    console.log(v, c, ( (v * 100) / (c + 100)));
            //    v = Math.round( (v * 100) / (c + 100));
            //    $('#j-price').val(v);
            //});
        }
    }

    var progressCnt = 0;
    function imgProgress(id, step, data)
    {
        switch(step)
        {
            case 'start': {
                if(o.examplesCnt >= o.examplesLimit) return false;
                o.examplesCnt++;
                $progress.removeClass('hidden');
                progressCnt++;
            } break;
            case 'preview': {
                examplesAdd('img', id, data);
                progressCnt--;
                if(progressCnt <= 0){
                    $progress.addClass('hidden');
                    progressCnt = 0;
                }
            } break;
            case 'remove': {
                imgRemove(id);
            } break;
        }
        return true;
    }

    function examplesAdd(type, id, data)
    {
        var html;
        switch(type){
            case  'img':
                html = '<div class="o-project-thumb o-uploaded j-examp j-img j-img-'+id+'" data-id="'+id+'" data-fn="'+data.filename+'"> '+
                        '<input type="hidden" name="images[]" value="'+data.filename+'"/>'+
                        '<input type="hidden" name="sort[]" value="fn='+data.filename+'"/>'+
                        '<div class="o-inner added"> '+
                            '<img src="'+data.i+'" alt="" /> '+
                        '</div> '+
                        '<a href="#" class="o-item-remove link-delete j-examp-del"><i class="fa fa-times-circle-o"></i></a> '+
                    '</div>';
                break;
            case 'portfolio':
                html = '<div class="o-project-thumb o-uploaded j-examp j-portfolio j-portfolio-'+id+'" data-id="'+id+'"> '+
                    '<input type="hidden" name="sort[]" value="portfolio='+id+'"/>'+
                    '<div class="o-inner added"> '+
                        '<img src="'+data.preview+'" alt="" /> '+
                    '</div> '+
                    '<a href="#" class="o-item-remove link-delete j-examp-del"><i class="fa fa-times-circle-o"></i></a> '+
                '</div>';
                break;
            case 'shop':
                html = '<div class="o-project-thumb o-uploaded j-examp j-shop j-shop-'+id+'" data-id="'+id+'"> '+
                    '<input type="hidden" name="sort[]" value="shop='+id+'"/>'+
                    '<div class="o-inner added"> '+
                        '<img src="'+data.preview+'" alt="" /> '+
                    '</div> '+
                    '<a href="#" class="o-item-remove link-delete j-examp-del"><i class="fa fa-times-circle-o"></i></a> '+
                '</div>';
                break;

        }
        if(html) {
            $examples.append(html);
            imgRotate(true);
        }
    }

    function imgRotate(update)
    {
        if(update === true) {
            $examples.sortable('refresh');
        }else{
            $examples.sortable();
        }
    }

    function imgRemove(id, $bl)
    {
        o.examplesCnt--;
        if( ! $bl){
            $bl = $examples.find('.j-img-' + id);
        }
        if($bl.length){
            if($bl.hasClass('j-img')) {
                bff.ajax(img.url + 'delete', {image_id: 0, filename: $bl.data('fn')}, function (resp) {
                    if (resp && resp.success) {
                        img.uploader.decrementUploaded();
                        $bl.remove();
                        imgRotate(true);
                    } else {
                        app.alert.error(resp.errors);
                    }
                });
            }else{
                $bl.remove();
                imgRotate(true);
            }
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());