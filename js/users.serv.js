var jUsersServ = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $list, $form, $pgn;
    function init()
    {
        $block = $('#users-list');
        $form = $('#j-serv-users-form-list');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');


        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){

                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                return false;
            },
            ajax: o.ajax
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());