var jOrdersOrderView = (function(){
    var inited = false, o = {lang:{}};
    var $block;

    function init()
    {
        $block = $('#j-order-view');
        var $carousel = $block.find('#j-order-view-carousel');
        if($carousel.length){
            $carousel.owlCarousel({
                items : 5, // 10 items above 1000px browser width
                itemsDesktop : [1000,4], // 5 items between 1000px and 901px
                itemsDesktopSmall : [900,3], // betweem 900px and 601px
                itemsTablet: [600,1], // 2 items between 600 and 0
                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                navigation : true,
                navigationText : ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
                pagination : true,
                autoPlay : false
            });
        }

        var $loader = $block.find('.order-send-expert-loader');
        if($loader.length){
            var $loader_counter = $loader.find('.order-send-expert-loader__counter');
            var $loader_progress = $loader.find('.order-send-expert-loader__progress');
            var i = o.loader;
            var step = o.step * 1000;
            (function() {
                if (i <= 100 && $loader.length) {
                    $loader_counter.text(i + '%');
                    $loader_progress.css('width', i + '%');
                    i++;
                    setTimeout(arguments.callee, step);
                }
            })();
        }

        $block.on('click', '#j-delete-order', function(){
            if(confirm(o.lang.order_delete)){
                bff.ajax(bff.ajaxURL('orders', 'order-delete'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        bff.redirect('/');
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $block.on('click', '.j-hide', function(){
            bff.ajax(bff.ajaxURL('orders', 'order-hide'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    location.reload();
                    // $block.find('.j-hide').addClass('hidden');
                    // $block.find('.j-show').removeClass('hidden');
                    // var $h = $block.find('.j-title').find('h1');
                    // var $fa = $h.find('.fa-lock');
                    if($fa.length){
                        $fa.removeClass('hidden');
                    }else{
                        $h.prepend('<i class="fa fa-lock"></i> ');
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $block.on('click', '.j-show', function(){
            bff.ajax(bff.ajaxURL('orders', 'order-show'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    location.reload();
                    /*$block.find('.j-show').addClass('hidden');
                    $block.find('.j-hide').removeClass('hidden');
                    $block.find('.j-title').find('h1').find('.fa-lock').addClass('hidden');*/
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $block.on('click', '#btn-edit-desc', function() {
            var val = $('#form-edit-descr textarea').val();
            var tmp = $(this).attr('data-save-txt');
            if (tmp) {
                $('#form-edit-descr textarea').slideDown();
                $(this).text(tmp);
                $(this).removeAttr('data-save-txt');
            }
            else {
                bff.ajax(bff.ajaxURL('orders', 'orderDesc'), {id: o.id, descr: val, hash: app.csrf_token}, function (resp, errors) {
                    console.log(resp);
                    if (resp && resp.success) {
                        $('#form-edit-descr').parent().slideUp(function(){
                            $(this).text(val).slideDown();
                        });
                        // bff.redirect('/');
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $block.on('click', '.j-order-change-theme', function() {
            var val = $('#form-edit-title input').val();
            var tmp = $(this).attr('data-save-txt');
            var default_title = $(this).attr('data-default');
            if(val == ''){
                val = default_title;
            }
            if (tmp) {
                $('#form-edit-title').css('display', '');
                $('#form-edit-title input').slideDown();
                $(this).text(tmp);
                $(this).removeAttr('data-save-txt');
            } else {
                bff.ajax(bff.ajaxURL('orders', 'orderTitle'), {id: o.id, title: val, hash: app.csrf_token}, function (resp, errors) {
                    console.log(resp);
                    if (resp && resp.success) {
                        $('#form-edit-title').slideUp(function(){
                            $('.j-order-change-theme').remove();
                            $('.order__title b').remove();
                            var link = '<b><a data-save-txt="Сохранить тему" data-default="'+default_title+'" class="j-order-change-theme">'+val+'</a></b>';
                            $('#form-edit-title').before(link);
                        });
                        // bff.redirect('/');
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        var url = bff.ajaxURL('orders&ev=attachPro&hash=' + app.csrf_token + '&item_id=' + o.id, '');
        $(document).on('click', '.j-download-all', function(){
            bff.ajax(url + 'downloadAll',{}, function (resp) {
                if (resp && resp.success) {
                    if (resp.url){
                        bff.redirect(resp.url);
                    }
                }else{
                    app.alert.error(resp.errors);
                }
            });
        });

        $block.on('click', '#btn-edit-dueDate', function() {
            var val = $('#form-edit-dueDate input').val();
            var tmp = $(this).attr('data-save-txt');
            if (tmp) {
                $('#form-edit-dueDate input').slideDown();
                $(this).text(tmp);
                $(this).removeAttr('data-save-txt');
            }
            else {
                bff.ajax(bff.ajaxURL('orders', 'orderDueDate'), {id: o.id, due_date: val, hash: app.csrf_token}, function (resp, errors) {
                    console.log(resp);
                    if (resp && resp.success) {
                        $('#form-edit-dueDate').slideUp(function(){
                            $(this).parent().find('.dueDateInfo').text(val);
                        });
                        // bff.redirect('/');
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        var $map = $('#map-desktop');
        if($map.length){
            app.map($map.get(0), [o.lat, o.lng], false, {zoom: 12, marker:true});
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());
