var jDevRate = (function() {
	var inited = false, o = {lang: {}};

	function init() {
		console.log('Dev inited');
		app.devInfo(o.rateDetail);
	}
	return {
		init: function(options)
		{
			if(inited) return; inited = true;
			o = $.extend(o, options || {});
			$(function(){ init(); });
		}
	};
}());

var jOrderDecline = (function() {
	var inited = false, o = {lang: {}};
	var $block;

	function init() {
		console.log('inited');
		//todo change block
		$block = $('#profile-collapse');
		var $declinePopup = false;

		$(document).on('click', '.j-order-decline', function(){
			if ($(this).data('order-id') != 'undefined'
				&& $(this).data('order-id') > 0) {
				order_id = $(this).data('order-id');
			}
			else {
				order_id = o.id;
			}
			bff.ajax(bff.ajaxURL('orders', 'decline'), {id: order_id}, function (data, errors) {
				if (data && data.success) {
					$block.after(data.popup);
					$declinePopup = $('#j-decline-modal');
					$declinePopup.modal('show');
				} else {
					app.alert.error(errors);
				}
			});

			return false;
		});

		$('#j-user-invite').click(function(){
			//location.href = '/orders/add?invite[]=' + o.id;
			redirectPost('/orders/add', {'invite[]': o.id});
		});
	}
	function sendDecline() {
		$('#j-order-decline-send').click(function () {
			console.log('clicked j-order-decline-send');
			$data = $('#j-order-decline-form').serialize();
			url = o.sUrl;

			$('#j-decline-modal .modal-body .row').fadeOut(function () {
				$('#j-decline-modal .modal-body').append('<h4>Ожидайте...</h4>');
			});


			bff.ajax(bff.ajaxURL('orders', 'decline_send'), $data, function (resp) {
				if (resp.success) {
					setTimeout(function () {
						$('#j-decline-modal .modal-body h4').remove();
						if ($('#j-orders-respondent-list-form') != 'undefined') {
							$('#j-orders-respondent-list-form').remove();
						}
						$('#j-decline-modal .modal-body').append('<h4>Спасибо, ваш отказ отправлен заказчику и администрации. Перезагружаем страницу.</h4>');
					}, 500);
				}
				else {
					setTimeout(function () {
						$('#j-decline-modal .modal-body .row').remove();
						$('#j-decline-modal .modal-body').append('<h4>Что-то пошло не так. Перезагружаем страницу.</h4>');
					}, 500);
				}
				setTimeout(function () {
					location.reload();

					//old code
					$('#j-decline-modal').modal('hide');
					setTimeout(function () {
						$('#j-decline-modal .modal-body .row').show();
						$('#j-decline-modal .modal-body h4').remove();
						$('#order_decline_message').val('');
					}, 500);
				}, 2000);
			});
		});
	}
	return {
		init: function(options)
		{
			if(inited) return; inited = true;
			o = $.extend(o, options || {});
			$(function(){ init(); });
		},
		send: function(options)
		{
			o = $.extend(o, options || {});
			$(function () { sendDecline(); })
		}
	};
}());