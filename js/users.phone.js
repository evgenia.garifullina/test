
var jUserPhoneCode = (function(){
    var inited = false, phoneCodeInit = false, phoneForm = null, countDownTimer = null,
        dataConfirmPhone = null,
        url = document.location.pathname,
        o = {
            form:'#j-form-phone',

            fieldPhone: '#j-field-phone input[name=phone]',
            fieldPhoneInfo: '#j-field-phone i',
            btnSendCode: '#j-field-phone button',

            rowCode: '#j-field-phone-code',
            fieldCode: '#j-field-phone-code input[name=phonecode]',
            occupiedPhone: '#j-field-phone-code .occupiedPhone',
            helpWait: '#j-field-phone-code .help-wait',
            helpWaitSec: '#j-field-phone-code .help-wait i',
            btnRepeatCode: '#j-field-phone-code .help-repeat',
            successCall: null
    };

    function init(options, callback)
    {
        if(inited) return; inited = true;
        o = $.extend(o, options || {});
        $(function(){
            callback();
        });
    }

    function initPhoneForm() {
        console.log('.initPhoneForm');
        app.form(o.form, function(){

            phoneForm = this;
            if( ! phoneForm.checkRequired({focus:true}) ) {
                console.error('Error form');
                return;
            }

            phoneForm.ajax('/popupUserSettingPhone',{},function(data,errors){
                console.log('!!initPhoneForm!!', data,errors);

                if (typeof errors.phonecode !== 'undefined') {
                    // Ошибка ввода кода
                    console.log('#0');
                    phoneForm.fieldsError(['phonecode'], errors.phonecode);
                }
                else if(jUserPhoneCode.isPhoneCodeResponse(data)) {
                    // Отправили код, просим ввести код
                    console.log('#1');
                    showPhoneCode();
                    if (!jUserPhoneCode.applyPhoneCodeResponse(data)) {
                        phoneForm.fieldsError(data.fields, errors);
                    }
                }
                else if(data && data.success) {
                    // Успешно
                    console.log('#2');
                    window.location.href = data.whatsAppUrl;
                    hidePhoneCode();
                    phoneForm.alertSuccess("Номер телефона подтвержден");//, {reset:true}
                    jUserPhoneCode.successCall();
                }
                else {
                    // Ошибка с номером тел
                    console.log('#3');
                    hidePhoneCode();
                    phoneForm.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // $('#phoneConfirmCancel').click(function(){
        //     bff.cookie('hidePhoneConfirm', true, {expires:99999, domain:'.'+app.host} );
        //     if(confirm('reload')) {
        //         location.reload();
        //     }
        // });
        //
        dataConfirmPhone = $(o.fieldPhone).data('confirmPhone');
        var btnSendCode = $(o.btnSendCode);
        $(o.fieldPhone).on('keyup', function() {
            $(o.fieldPhoneInfo).hide();
            var str = $(this).val();
            if (str.length < 10 || dataConfirmPhone === str) {
                btnSendCode.attr('disabled', 'disabled');
            }
            else {
                btnSendCode.removeAttr('disabled');
            }
        });
        btnSendCode.click(function() {
            $(o.fieldCode).val('');
            $(o.form).submit();
        });

    }


    function initPhoneCodeForm() {

        if (phoneCodeInit) return;
        phoneCodeInit = true;

        // $(o.helpWait).show();
        $(o.btnRepeatCode).attr('disabled', 'disabled')
            .on('click', function() {
                $(o.fieldCode).val('');
                $(o.form).submit();
            });

        $('#phoneCodeSubmit').attr('disabled', 'disabled')
            .on('click', function() {
                $(o.form).submit();
            });

        $(o.fieldCode).on('keyup', function() {
            var str = $(this).val();
            if (str.length !== 4) {
                $('#phoneCodeSubmit').attr('disabled', 'disabled');
            }
            else {
                $('#phoneCodeSubmit').removeAttr('disabled');
            }
        });

        // initPhoneCodeFormOpen();
    }

    function showPhoneCode() {
        $(o.btnSendCode).attr('disabled', 'disabled');
        $(o.btnSendCode).attr('disabled', 'disabled');
        $(o.rowCode).show();
        startCounter();
    }

    function hidePhoneCode() {
        $(o.btnSendCode).attr('disabled', 'disabled');
        $(o.rowCode).hide();
        $(o.rowCode).find('.errorPhoneCode').html('');
        clearInterval(countDownTimer);
    }

    function startCounter() {
        $(o.helpWait).show();
        $(o.btnRepeatCode).attr('disabled', 'disabled');
        var helpWaitSec = $(o.helpWaitSec);
        var sec = intval(helpWaitSec.text());
        if (!sec) {
            sec = intval(helpWaitSec.attr('data-time'));
            helpWaitSec.text(sec);
        }
        clearInterval(countDownTimer);
        countDownTimer = setInterval(function(){
            var sec = intval(helpWaitSec.text()) - 1;
            helpWaitSec.text(sec);
            if (sec<1) {
                clearInterval(countDownTimer);
                $(o.helpWait).hide();
                $(o.btnRepeatCode).removeAttr('disabled');
            }
        }, 1000);
    }


    return {
        init: function(options)
        {
            init(options, function()
            {
                initPhoneForm();
                initPhoneCodeForm();
            });
        },
        isPhoneCodeResponse: function(data) {
            return data && data['needPhoneCode'];
        },
        applyPhoneCodeResponse: function(data) {
            if (data.occupiedPhone)
                $(o.occupiedPhone).show();
            else
                $(o.occupiedPhone).hide();
            if (typeof data.waitTime !== 'boolean') {
                $(o.helpWaitSec).text(data.waitTime);
            }
            if (data['hideError'] && !data.success) {
                return true;
            }
            return false;
        },
        cancelPhoneCodeResponse: function() {
            // DEPRECATED
            hidePhoneCode()
        },
        successCall: function() {
            if (o.successCall)
                o.successCall();
        },
        successSet: function(f) {
            o.successCall = f;
        }
    }
}());
