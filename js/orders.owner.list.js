var jOrdersOwnerList = (function(){

    var inited = false, o = {lang:{}, sideStatus: []}, listMngr;
    var $block, $form, $list, $pgn;

    function init()
    {
        $block = $('#j-orders-owner-list');

        $form = $('#j-orders-owner-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        // $form.on('click', '.j-f-type', function(){
        //     var $el = $(this);
        //     $form.find('[name="t"]').val($el.data('id'));
        //     closePopup($el);
        //     massActions();
        //     listMngr.submit({}, true);
        //     return false;
        // });

        $('body').on('click', '.j-f-status', function(){
            var $el = $(this);
            $form.find('[name="st"]').val($el.data('id'));
            closePopup($el);
            massActions();
            listMngr.submit({}, true);
            return false;
        });

        $list.on('click', '.j-delete', function(){
            if(confirm(o.lang.order_delete)){
                bff.ajax(bff.ajaxURL('orders', 'order-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        listMngr.submit({popstate:true}, false);
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $list.on('click', '.j-hide', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('orders', 'order-hide'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    var $bl = $el.closest('.j-order');
                    $bl.find('.j-hide').addClass('hidden');
                    $bl.find('.j-show').removeClass('hidden');
                    var $h = $bl.find('.l-project-title');
                    var $fa = $h.find('.fa-lock');
                    if($fa.length){
                        $fa.removeClass('hidden');
                    }else{
                        $h.prepend('<i class="fa fa-lock"></i>');
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $list.on('click', '.j-show', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('orders', 'order-show'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    var $bl = $el.closest('.j-order');
                    $bl.find('.j-show').addClass('hidden');
                    $bl.find('.j-hide').removeClass('hidden');
                    $bl.find('.l-project-title').find('.fa-lock').addClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });


        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('counts')){
                    for(var t in resp.counts){
                        if(resp.counts.hasOwnProperty(t)){
                            for(var i in resp.counts[t]){
                                if(resp.counts[t].hasOwnProperty(i)){
                                    var v = intval(resp.counts[t][i]);
                                    var $el = $form.find('.j-cnt-' + t + '-' + i);
                                    if(v){
                                        $el.closest('li').removeClass('hidden');
                                    }else{
                                        //$el.closest('li').addClass('hidden');
                                    }
                                    $el.text('(' + v + ')');
                                }
                            }
                        }
                    }
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function() {
                massActions();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });
    }

    function closePopup($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function massActions()
    {
        // var t = intval($form.find('[name="t"]').val());
        // if(o.types.hasOwnProperty(t)){
        //     $form.find('.j-f-type-title').text(o.types[t].t);
        // }
        // var $t = $form.find('.j-f-type');
        // $t.parent().removeClass('active');
        // $t.each(function(){
        //     if($(this).data('id') == t){
        //         $(this).parent().addClass('active');
        //     }
        // });

        t = intval($form.find('[name="st"]').val());
        if(o.status.hasOwnProperty(t)){
            $form.find('.j-f-status-title').text(o.status[t].t);
        }
        $t = $('body').find('.j-f-status');
        $t.parent().removeClass('active');
        $t.each(function(){
            var i = intval($(this).data('id'));
            if( i == t || (i==4 && !o.sideStatus[t])){
                $(this).parent().addClass('active');
            }
        });
        if (t == 0 || t == 4 || !o.sideStatus[t]) {
            $('#order-filter-sub').removeClass('hidden');
        } else {
            $('#order-filter-sub').addClass('hidden');
        }
    }


    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
