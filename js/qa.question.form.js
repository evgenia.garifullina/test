var jQaQuestionForm = (function(){

    var inited = false, o = {lang:{}, itemID:0};
    var f, $f;

    function init()
    {
        //submit
        $f = $('#j-question-form');
        f = app.form($f, function(){
            if( ! f.checkRequired({focus:true}) ) return;
            var $bl = $f.find('.j-specs:visible');
            if($bl.length){
                if( ! intval($bl.find('.j-spec-value').val())){
                    app.alert.error(o.lang.spec_wrong);
                    return;
                }
            }

            bff.ajax(o.url, $f.serialize(), function(resp, errors){
                if(resp && resp.success) {
                    if(resp.redirect){
                        bff.redirect( $('<div/>').html(resp.redirect).text() );
                    }else{
                        f.alertSuccess(o.lang.saved_success);
                    }
                } else {
                    f.fieldsError(resp.fields, errors);
                }
            }, function(p){ f.processed(p); });
        }, {noEnterSubmit: true});

        $f.on('click', '.j-submit', function(){
            $f.submit();
            return false;
        });

        // cancel
        $f.on('click', '.j-cancel', function(){
            history.back();
            return false;
        });

        if(typeof(jSpecsSelect) == 'object'){
            jSpecsSelect.init({block:$f.find('.j-specs'), cnt:1, limit:1});
        }

        var $term = $f.find('#j-term-title');
        f.$field('term').change(function(){
            var id = $(this).val();
            if(o.terms.hasOwnProperty(id)){
                $term.html(o.terms[id]);
            }else{
                $term.html('');
            }
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());