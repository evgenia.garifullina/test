window.onerror = function (msg, url, line, col, err) {
    errorCatcher(msg, url, line, col, err);
    return false;
};

//Exceptions
function errorCatcher(msg, url, line, col, err) {
    if (!navigator.userAgent) return;

    if (msg instanceof Error) {
        err = msg.stack;
        msg = msg.message;
    }
    else if (err && err instanceof Error) {
        err = err.message + ";\n% " + err.stack;
    }
    else {
        err = '';
        if (arguments['callee'] && arguments.callee['caller']) {
            err += "\n# " + (arguments.callee.caller['name'] ? arguments.callee.caller['name'] : '');
            err += "\n# " + arguments.callee.caller.toString();
        }
        else {
            try {
                throw new Error();
            }
            catch (e) {
                err += "\n* " + e.stack;
            }
        }
    }

    if (!url) {
        url = window.location.toString();
    }

    if (msg) {
        if (typeof msg !== 'string') {
            msg = JSON.stringifyOnce(msg);
        }
        msg = msg.replace(/\n/g, "||");
        msg = msg.substr(0, 1600);
    }

    console.log('ErrException', msg, err, url);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", '/index.php?errorsJs=' + (new Date().getTime() / 1000), true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    var query = 'm=' + encodeURIComponent(msg) +
        '&v=' + (typeof jsVer === "undefined" ? 0 : jsVer) +
        '&r=' + encodeURIComponent(document.referrer) +
        '&u=' + encodeURIComponent(url);
    if (line) query += '&l=' + encodeURIComponent(line + (col ? ':' + col : ''));
    if (err) {
        err = err.replace(/\n/g, "||");
        err = err.substr(0, 3200);
        query += '&s=' + encodeURIComponent(err);
    }
    xmlhttp.send(query);
}

//Exceptions
function bff_report_exception(msg, url, line) {
    errorCatcher(msg, url, line);
}

function setDnt (v) {
    setCookie( (v ? v : 'dnt'), '1', 365, '/');
}

function setCookie(name, value, expires, path, domain, secure) {
    if (expires && ('number' == typeof expires || typeof expires.toUTCString != 'undefined')) {
        var date;
        if ('number' == typeof expires) {
            date = new Date();
            date.setTime(date.getTime() + (expires * 86400000/*24 * 60 * 60 * 1000*/));
        } else {
            date = expires;
        }
        expires = date.toUTCString(); // use expires attribute, max-age is not supported by IE
    }
    document.cookie = name + "=" + encodeURIComponent(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
// Get cookie.
function getCookie(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = decodeURIComponent(cookie.substring(offset, end));
        }
    }
    return(setStr);
}
function delCookie(name) {
    document.cookie = name + "=" + "; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT";
}

function fireEvent(name, params) {
    if ( typeof window.CustomEvent !== "function" ) {
        var fixCustomEvent = function(event, params) {
            params = params || {bubbles: false, cancelable: false, detail: undefined};
            var evt = document.createEvent('CustomEvent');
            evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            return evt;
        };
        fixCustomEvent.prototype = window.Event.prototype;
        window.CustomEvent = fixCustomEvent;
    }
    window.dispatchEvent(new window.CustomEvent(name, params));
}

JSON.stringifyOnce = function(obj, replacer, indent){
    var printedObjects = [];
    var printedObjectKeys = [];

    function printOnceReplacer(key, value){
        if ( printedObjects.length > 2000){ // browsers will not print more than 20K, I don't see the point to allow 2K.. algorithm will not be fast anyway if we have too many objects
            return 'object too long';
        }
        var printedObjIndex = false;
        printedObjects.forEach(function(obj, index){
            if(obj===value){
                printedObjIndex = index;
            }
        });

        if ( key == ''){ //root element
            printedObjects.push(obj);
            printedObjectKeys.push("root");
            return value;
        }

        else if(printedObjIndex+"" != "false" && typeof(value)=="object"){
            if ( printedObjectKeys[printedObjIndex] == "root"){
                return "(pointer to root)";
            }else{
                return "(see " + ((!!value && !!value.constructor) ? value.constructor.name.toLowerCase()  : typeof(value)) + " with key " + printedObjectKeys[printedObjIndex] + ")";
            }
        }else{

            var qualifiedKey = key || "(empty key)";
            printedObjects.push(value);
            printedObjectKeys.push(qualifiedKey);
            if(replacer){
                return replacer(key, value);
            }else{
                return value;
            }
        }
    }
    return JSON.stringify(obj, printOnceReplacer, indent);
};