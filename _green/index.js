const preformButtonListener = () => {
    window.scrollTo(0, 0);
};

const preformButtonScroll = () => {
    const preform = $('#preformIndex');
    if (preform.length){
        const preformButton = $('#preformIndex button');

        if($(window).width() < 480){
            preformButton.on('click', preformButtonListener);
        } else {
            preformButton.off('click', preformButtonListener);
        }
    }
};

const scaleMobile = () => {
    const body = $('body');
    if($(window).width() < 480){
        body.css('transform', 'scale(' + $(window).width() / 480 + ')');
        body.css('margin-bottom', body.height() * ($(window).width() / 480) - body.height() + 'px');
    } else {
        body.css('transform', '');
        body.css('margin-bottom', '');
    }
};

const ordersCarouselSetup = () => {
    const ordersCarousel = $('.index-work-type ul')
    if (ordersCarousel.length && !ordersCarousel.parents().hasClass('index-work-type--price')) {
        if($(window).width() < 992){
            ordersCarousel.addClass('owl-carousel');
            ordersCarousel.owlCarousel({
                items: 2,
                loop: true,
                nav: true,
                dots: false,
                navContainer: $('.index-work-type .slider-nav'),
                margin: 30,
                autoplayTimeout: 7500,
                autoplay: true,
                responsive : {
                    480: {
                        items: 2,
                        center: false
                    }
                    /*
                    0: {
                        items: 1,
                        center: true,
                        autoplay: false
                    },*/
                }
            });
        }

        else {
            ordersCarousel.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
            ordersCarousel.find('.owl-stage-outer').children().unwrap();
        }
    }
};

const priceItemsPagination = () => {
    const priceItems = $('.index-work-type--price ul');
    if (priceItems.length) {
        if($(window).width() < 767){
            if (!priceItems.data('paginate')){
                priceItems.paginate({
                    perPage: 8,
                    useHashLocation: false,
                    autoScroll: false
                });
            }
        } else if (priceItems.data('paginate')) {
            priceItems.data('paginate').kill();
        }
    }
};

$(document).ready(function() {


    var list_title = $('.subjects .list-title');
    if (list_title.length){
        if($(window).width() < 992){
            list_title.click(function() {
                $('.subjects').toggleClass('subjects-opened');
            });
        }
    }

    $('.banner-activate span').click(function () {

        $('.banner-activate').css('display', 'none');
        bff.cookie('banner-hide', 1, {expires: 180, path: '/', domain: '.' + app.host});
    });

    $('.navbar-toggle').off();
    $('.navbar-toggle').click(function () {
        $('.navmenu').toggleClass('visible');
    });

    const newsSlider = $('#slidewrapper');
    if (newsSlider.length) {
        newsSlider.owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            dots: false,
            navContainer: $('.news .slider-nav'),
            margin: 30,
            autoplay: true,
            autoplayTimeout: 7500,

            responsive : {
                480: {
                    items: 1
                },
                992: {
                    items: 2
                }
            }
        });
    }

  const howItworksSlider = $('.about-us__how-it-works-list');
  if (howItworksSlider.length) {
    howItworksSlider.owlCarousel({
      items: 1,
      loop: true,
      nav: true,
      dots: false,
      navContainer: $('.about-us__how-it-works-slider-nav'),
      margin: 200,
      autoplay: true,
      autoplayTimeout: 10000,
      center: true,
      autoWidth:true,

    });
  }

    preformButtonScroll();
    ordersCarouselSetup();
    priceItemsPagination();
    scaleMobile();
    // subjectsList();
});

$(window).resize(function(){
    preformButtonScroll();
    ordersCarouselSetup();
    priceItemsPagination();
    scaleMobile();
});



